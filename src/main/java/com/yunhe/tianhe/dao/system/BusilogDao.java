package com.yunhe.tianhe.dao.system;

import com.yunhe.tianhe.entity.system.Busilog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 10:43
 * @Description: 系统日志操作
 */
public interface BusilogDao {

    /**
     * 添加
     * @param busilog
     */
    void save(Busilog busilog);

    /**
     * 单个查找
     * @param logid
     * @return
     */
    Busilog find(long logid);

    /**
     * 查找所有
     * @return
     */
    List<Busilog> findlist();

    /**
     * 查找所有
     * @return
     */
    List<Busilog> findpage(@Param ( "busilog" ) Busilog busilog,@Param ( "offset" ) int offset,@Param ( "limit" ) int limit);
    /**
     * 分页
     * @return
     */
    Integer count();
}

