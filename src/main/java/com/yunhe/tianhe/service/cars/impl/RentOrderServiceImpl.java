package com.yunhe.tianhe.service.cars.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.util.StringUtils;
import com.yunhe.tianhe.dao.cars.RentOrderDao;
import com.yunhe.tianhe.entity.cars.RentOrder;
import com.yunhe.tianhe.service.cars.RentOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/29 0029 上午 9:28
 * @Description TODO
 */
@Service
public class RentOrderServiceImpl implements RentOrderService {

    @Autowired
    private RentOrderDao rentOrderDao;

    @Override
    public Pagedao<RentOrder> list(Integer offset, Integer limit,String searchCode) {
        Pagedao <RentOrder> pagedao = new Pagedao <>();
        List<RentOrder> list = null;
        Integer count = rentOrderDao.findCount(searchCode);
        if (count > 0){
            pagedao.setPage(offset);
            pagedao.setSize(limit);
            list = rentOrderDao.findAll(offset,limit, searchCode);
        }
        pagedao.setTotal(count);
        pagedao.setRows(list);
        return pagedao;
    }

    @Override
    public void delete(String ids) {
        if (StringUtils.notEmpty(ids)){
            String[] split = ids.split(",");
            for (String p:split){
                rentOrderDao.delete(Long.valueOf(p));
            }
        }
    }

    @Override
    public void save(RentOrder rentOrder) {
        rentOrderDao.save(rentOrder);
    }
}
