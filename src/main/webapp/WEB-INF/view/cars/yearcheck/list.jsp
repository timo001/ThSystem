<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/25 0025
  Time: 下午 1:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>年检</title>
    <link rel="stylesheet" href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="/static/js/plugins/bootstrap-table/bootstrap-table.css">
    <link rel="stylesheet" href="/static/css/common.css">
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
</head>
<body>
<div class="layui-box box-padding-15">
    <span class="layui-breadcrumb">
                <a href="/" target="_parent">首页</a>
                <a href="/yearcheck/yearchecklist">车辆年检</a>
                <a><cite>列表</cite></a>
    </span>
    <hr>
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md7">
            <div class="grid-demo grid-demo-bg1">
                <button id="btn_add" type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>添加
                </button>
                <button id="btn_delete" type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>删除
                </button>
                <button id="btn_edit" type="button" class="btn btn-default">
                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>修改
                </button>
            </div>
        </div>
        <div class="layui-col-md5">
            <div class="grid-demo">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">车牌号：</label>
                        <div class="layui-input-inline">
                            <input type="text" name="carcode" placeholder="请输入车牌号" id="carcode" lay-verify="required|phone" autocomplete="off" class="input-carcode  layui-input">
                        </div>
                        <button class="button-carcode layui-btn" id="btn-carcode">搜索</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table id="tb_departments"></table>
</div>










<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script>
    $(function () {
        layui.use(['form','element','layer'], function(){
            var element = layui.element;
            var layer = layui.layer;

            $("#tb_departments").bootstrapTable({
                url: '/yearcheck/page',         //请求后台的URL（*）
                method: 'get',                      //请求方式（*）
                //toolbar: '#toolbar',                //工具按钮用哪个容器
                striped: true,                      //是否显示行间隔色
                cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                pagination: true,                   //是否显示分页（*）
                //sortable: false,                     //是否启用排序
                // sortOrder: "asc",                   //排序方式
                //             queryParams: oTableInit.queryParams,//传递参数（*）
                sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
                pageNumber:1,                       //初始化加载第一页，默认第一页
                pageSize: 10,                           //每页的记录行数（*）
                pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
                //search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
                strictSearch: true,                 //  ? ? ?
                // showColumns: true,                  //是否显示所有的列
                //showRefresh: true,                  //是否显示刷新按钮
                //           minimumCountColumns: 2,             //最少允许的列数
                clickToSelect: true,                //是否启用点击选中行
                height: 600,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
                uniqueId: "id",                     //每一行的唯一标识，一般为主键列
                //showToggle:true,                    //是否显示详细视图和列表视图的切换按钮
                cardView: false,                    //是否显示详细视图
                detailView: false,                   //是否显示父子表
                columns: [{
                    checkbox: true
                }, {
                    field: 'carcode',
                    title: '年检车辆'
                }, {
                    field: 'address',
                    title: '地点'
                }, {
                    field: 'usecartime',
                    title: '年检时间',
                    formatter:function (value, row, index) {
                        if(isNaN(value)){
                            return "--";
                        } else {
                            var time = new Date(value);
                            return time.toLocaleDateString().replace("/","-").replace("/","-");
                        }
                    }
                },{
                    field: 'costmoney',
                    title: '年检费用'
                }, {
                        field: 'headman',
                        title: '经办人'
                    },]
            })



            //添加保养记录
            $("#btn_add").on("click",function () {
                location.href="/yearcheck/add"
            })

            //修改保险记录
            $("#btn_edit").on("click",function () {
                var $table = $("table[id = tb_departments]");  //获取树
                var boot = $table.bootstrapTable("getSelections");//获取选中的树节点
                var recordid = null;
                $(boot).each(function () {
                    recordid = this.recordid;
                })
                if (boot.length != 1){
                    layer.msg("请选择一条数据!")
                }else {
                    location.href="/yearcheck/edit?recordid="+recordid;
                }
            })






            //删除保养记录
            $("#btn_delete").on("click",function () {
                var $table = $("table[id = tb_departments]");  //获取树
                var boot = $table.bootstrapTable("getSelections");//获取选中的树节点
                var arr = [];
                $(boot).each(function () {
                    arr.push(this.recordid);
                })
                if (arr.length < 1){
                    layer.msg("请选择一条数据！")
                }else {
                    layer.confirm("确定要删除选中的数据吗？",function () {
                        $.ajax({
                            url:"/yearcheck/delete",
                            type:"post",
                            dataType:"json",
                            data:{ids:arr.join(",")},
                            success:function (msg) {
                                if (msg.code == 1){
                                    layer.msg(msg.message)
                                    $("#tb_departments").bootstrapTable("refresh")  //刷新表格数据
                                }
                            },
                            error:function () {
                                layer.msg("服务器繁忙，请稍后再试!")
                            }
                        })
                    })
                }
            })




            //搜索
            $("#btn-carcode").on("click",function () {
                var carcode = $("#carcode").val();
                $("#tb_departments").bootstrapTable("refresh",{
                    url:"/yearcheck/page",
                    query:{
                        carcode:carcode
                    }
                })
                $("#carcode").val("")
            })


        })
    })


</script>
</body>
</html>
