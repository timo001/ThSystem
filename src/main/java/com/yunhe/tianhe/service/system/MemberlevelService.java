package com.yunhe.tianhe.service.system;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.system.Memberlevel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 13:54
 * @Description:  系统会员级别
 */
public interface MemberlevelService {

    /**
     * 添加
     * @param memberlevel
     */
    void save(Memberlevel memberlevel);

    /**
     * 删除
     * @param ids
     */
    void deleted(String ids);

    /**
     * 修改
     * @param memberlevel
     */
    void update(Memberlevel memberlevel);

    /**
     * 单个查找
     * @param levelid
     * @return
     */
    Memberlevel find(Long levelid);

    /**
     * 查找
     * @return
     */
    List<Memberlevel> findlist();


    /**
     * 分页
     * @param offset
     * @param limit
     * @return
     */
    Pagedao<Memberlevel> findpage(int offset, int limit);

}
