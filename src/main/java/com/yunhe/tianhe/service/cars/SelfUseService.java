package com.yunhe.tianhe.service.cars;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.SelfUse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/24 0024 下午 7:26
 * @Description: TODO
 */
public interface SelfUseService {

    /*车辆自用记录保存*/
    Long save(SelfUse selfUse);

    /*车辆自用记录修改*/
    void update(SelfUse selfUse);

    /*车辆自用记录删除*/
    void delete(Long recordId);

    /*车辆自用记录批量删除*/
    void deleteBatch(String ids);

    /*车辆自用记录查询*/
    SelfUse find(Long recordId);

    /*分页*/
    Pagedao<SelfUse> findPage(@Param("offset") Integer offset, @Param("limit") Integer limit,@Param("searchCode")String searchCode);

    /*根据车牌号查询*/
    List<SelfUse> findByCarCode(String carCode);
}
