/**
 * Created by Administrator on 2017/11/23 0023.
 */
//JavaScript代码区域
layui.use(['element','layer','form'], function(){
    var layer = layui.layer;
    var element = layui.element;
    var form = layui.form;

    var setting = {
        data: {
            simpleData: {
                enable: true
            }
        },
        callback: {
            onClick: zTreeOnClick
        }
    };
    $.ajax({
        url:"/system/sysdict/tree",
        type:"get",
        dataType:"json",
        success:function (msg) {
            var zNodes =msg.data;//获取数据
            $.fn.zTree.init($("#treeRole"), setting, zNodes); //初始化节点树
            console.log("节点数加载成功！")
        },
        error:function () {
            console.log("节点数加载失败！")
        }
    })

    //表单校验
    form.verify({
        parentname:function(value, item){ //value：表单的值、item：表单的DOM对象
            var vname=false;
                $.ajax({
                    url:"/system/sysdict/parent",
                    dataType:"json",
                    data:{name:value},
                    typt:"get",
                    async:false,//默认是异步，设为同步，表明反回的数据在ajax之后执行
                    success:function (msg) {
                        var data = msg.data;
                        if (msg.code==1){
                            $("input[name=fid]").val(data[0].id)
                            vname=true;
                        }
                    },
                    error:function () {
                        console.log("服务器忙！")
                    }
                })
            if (!vname){
                return "该父级名称不存在或错误";
            }
        }
    })


    //监听保存提交
    form.on('submit(formDemo)', function(data){
        $.ajax({
            url:"/system/sysdict/save",
            type:"post",
            dataType:"json",
            data:data.field,
            success:function (msg) {
                layer.msg("添加成功！",{time:1000},function () {
                    location.href="/system/sysdict/list";
                })
            },
            error:function () {
                layer.msg("服务器忙！请稍后再试！")
            }
        })
        return false;
    });

//    修改
    var id;
    function zTreeOnClick(event, treeId, treeNode) {
        id=treeNode.id;
        $("#parent").val(treeNode.name)//添加的父类
        $.ajax({
            url:"/system/sysdict/edit",
            type:"get",
            dataType:"json",
            data:{id:id},
            success:function (msg) {
                var data = msg.data;
                var data1 = msg.data1;
                if (data1==null){
                    layer.msg("该项不能修改！")
                }else{
                    $("#parentname").val(data1.name)
                    $("#name").val(data.name)
                    $("#code").val(data.code)
                }
            },
            error:function () {
                layer.msg("服务器忙！请稍后再试！")
            }
        })
    };
    $("#btn_edit").click(function () {
        if (null == id){
            layer.msg("请选择一条数据")
        }
    })

    //监听保修改提交
    form.on('submit(formedit)', function(data){
        $.ajax({
            url:"/system/sysdict/update",
            type:"post",
            dataType:"json",
            data:data.field,
            success:function (msg) {
                layer.msg("修改成功！",{time:1000},function () {
                    location.href="/system/sysdict/list";
                })
            },
            error:function () {
                layer.msg("服务器忙！请稍后再试！")
            }

        })
        return false;
    });

    $('#findlist').hide()
    $('#tb_departments').bootstrapTable({
        url: '/system/sysdict/page',         //请求后台的URL（*）
        method: 'get',                      //请求方式（*）
        toolbar: '#toolbar',                //工具按钮用哪个容器
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: true,                     //是否启用排序
        sortOrder: "asc",                   //排序方式
        // queryParams: {type:1},             //传递参数（*）
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        pageNumber:1,                       //初始化加载第一页，默认第一页
        pageSize: 10,                       //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        // search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
        // strictSearch: true,
        // showColumns: true,                  //是否显示所有的列
        showRefresh: false,                  //是否显示刷新按钮
        // minimumCountColumns: 2,             //最少允许的列数
        clickToSelect: true,                //是否启用点击选中行
        height: 1000,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        uniqueId: "id",                     //每一行的唯一标识，一般为主键列
        // showToggle:true,                    //是否显示详细视图和列表视图的切换按钮
        // cardView: false,                    //是否显示详细视图
        detailView: false,                   //是否显示父子表
       /* queryParams: function (data) {      //传递参数（*）
            var params={
                offset:data.offset,
                limit:data.limit,
                name:name
            }
            return params;
        },*/
        columns: [{
            field: 'id',
            title: '编号',
            width:'33%',
            align: 'center'
        },{
            field: 'name',
            title: '字典名称',
            width:'33%',
            align: 'center'
        },{
            field: 'code',
            title: '字典代码',
            width:'33%',
            align: 'center'
        }]
    });

//    查找
    $("#btn_find").on("click",function () {
        var name = $("#find").val();
        $('#tb_departments').bootstrapTable('refresh',{
            query:{
                name:name
            }
        })
        if ("" != name){
            layer.open({
                type: 1 ,
                title: '字典信息',
                area: ['800px', '800px'],
                content: $("#findlist")
            })
        }else{
            layer.msg("请输入字典名称！")
        }


    })



});