package com.yunhe.tianhe.service.cars.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.util.StringUtils;
import com.yunhe.tianhe.dao.cars.SelfUseDao;
import com.yunhe.tianhe.entity.cars.SelfUse;
import com.yunhe.tianhe.service.cars.SelfUseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/24 0024 下午 7:32
 * @Description: TODO
 */
@Service
public class SelfUseServiceImpl implements SelfUseService{

    @Autowired
    private SelfUseDao selfUseDao;
    @Override
    public Long save(SelfUse selfUse) {
        selfUseDao.save(selfUse);
        Long recordId = selfUse.getRecordId();
        return recordId;
    }

    @Override
    public void update(SelfUse selfUse) {
        selfUseDao.update(selfUse);
    }

    @Override
    public void delete(Long recordId) {
        selfUseDao.delete(recordId);
    }

    @Override
    public void deleteBatch(String ids) {
        if(StringUtils.notEmpty(ids)){
            String[] idArr = ids.split(",");
            for(String id : idArr){
                this.delete(Long.valueOf(id));
            }
        }
    }

    @Override
    public SelfUse find(Long recordId) {
        return selfUseDao.find(recordId);
    }

    @Override
    public Pagedao<SelfUse> findPage(Integer offset, Integer limit,String searchCode) {
        Pagedao<SelfUse> pagedao = new Pagedao<>();
        Integer count = selfUseDao.findCount(searchCode);
        List<SelfUse> data = null;
        if(count > 0){
            pagedao.setPage(offset);
            pagedao.setSize(limit);
            data = selfUseDao.findAll(offset,limit,searchCode);
        }
        pagedao.setTotal(count);
        pagedao.setRows(data);
        return pagedao;
    }

    @Override
    public List<SelfUse> findByCarCode(String carCode) {
        return selfUseDao.findByCarCode(carCode);
    }
}
