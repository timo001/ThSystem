/**
 * Created by Administrator on 2017/10/23 0023.
 */

layui.use('form', function() {
    var form = layui.form;
    form.on('select(type)',function (date) {
        //URI 和父级元素的显示和隐藏
        var val = date.value;
        if (val>1){
            $("#utyle").removeClass("layui-hide").find("input").attr("lay-verify","required");
            if(val != 9){
                $("#mstyle").removeClass("layui-hide").find("input").attr("lay-verify","required");
            }else{
                $("#mstyle").addClass("layui-hide").find("input").removeAttr("lay-verify");
            }
        }else{
            $("#mstyle").addClass("layui-hide").removeAttr("lay-verify");
            $("#utyle").addClass("layui-hide").removeAttr("lay-verify");
        }
        if (val==3){
            $("#btnId").removeClass("layui-hide");
        }else{
            $("#btnId").addClass("layui-hide");
        }

    })

    //监听提交
    form.on('submit(formSubmit)', function(data){
        $.ajax({
            url:"/permi/resource/update",
            type:"post",
            dataType:"json",
            data:data.field,
            success:function (msg) {
                layer.msg("修改成功！",{time:1000},function () {
                    location.href="/permi/resource/list";
                })
            },
            error:function () {
                layer.msg("服务器忙！请稍后再试！")
            }
        })
        return false;
    });
})