package com.yunhe.commons.util;


/**
 * 操作String的工具类
 * @author tianxuan
 * @Time 2016/4/19
 */
public class StringUtils {

     public static String trim(String str){
         return null == str ? str : str.trim();
     }

    public static String toUpperCase(String str){
        return null == str ? str : str.trim().toUpperCase();
    }

    /**
     * String 是否非空
     * @param str
     * @return
     */
    public static boolean notEmpty(String str){
        return (null != str) && !str.isEmpty();
    }

    /**
     * null 值 转为 空串
     * @param str
     * @return
     */
    public static String nullToEmpty(String str){
        return null == str ? "" : str;
    }

    public static boolean hasText(CharSequence charseq) {
        int len;
        if(charseq != null && (len = charseq.length()) != 0) {
            for(int i = 0; i < len; ++i) {
                if(!Character.isWhitespace(charseq.charAt(i))) {
                    return true;
                }
            }

            return false;
        } else {
            return false;
        }
    }
}
