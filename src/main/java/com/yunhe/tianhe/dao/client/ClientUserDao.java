package com.yunhe.tianhe.dao.client;

import com.yunhe.tianhe.entity.client.ClientUser;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/23 0023 上午 11:02
 * @Description: TODO
 */

public interface ClientUserDao {
    /**
     * 根据手机号码查询用户信息
     * @param mobile 手机号码
     * @return
     */
    ClientUser findByMobile(String mobile);

    /**
     * 首次用车保存新客户信息
     * @param clientUser
     */
    void save(ClientUser clientUser);

    /**
     * 根据ID查询一个客户
     * @param id
     * @return
     */
    ClientUser findById(Long id);


    /**
     * 查询所有客户详细信息与信息数,service层整合成pageable数据
     * @return
     */
    List<ClientUser> findList(@Param("offset") Integer offset,@Param("limit") Integer limit, @Param("memLevel")Integer memLevel,@Param("addTime") Date addTime);

    Integer findCount(@Param("memLevel")Integer memLevel,@Param("addTime")Date addTime);




    /**
     * 删除客户
     * @param id
     */
    void delete( Long id);

    /**
     * 修改客户信息
     */
    void update(ClientUser clientUser);

}
