package com.yunhe.tianhe.dao.client;

import com.yunhe.tianhe.entity.client.Credit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/23 0023 上午 11:02
 * @Description: TODO
 */
public interface CreditDao {


    /**
     * 添加信用记录
     */
    void save(Credit credit);

    /**
     * 查询信用记录
     * @return
     */
    List<Credit> findList(@Param("offset") Integer offset, @Param("limit") Integer limit, @Param("idcard") String idcard, @Param("mobile") String mobile);

    Integer findCount( @Param("idcard") String idcard, @Param("mobile") String mobile);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);




}
