package com.yunhe.tianhe.service.system;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.system.Dept;

import java.awt.print.Pageable;
import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/23 0023 12:05
 * @Description: ${todo}(用一句话描述该文件做什么)
 */
public interface DeptServive {


    /**
     * 保存
     * @param dept
     */
    void save(Dept dept);

    /**
     * 删除
     * @param ids
     */
    void deleted(String ids);

    /**
     * 编辑
     * @param dept
     */
    void update(Dept dept);

    /**
     * 根据id查
     * @param deptid
     * @return
     */
    Dept find(Long deptid);

    /**
     * 查找所有
     * @return
     */
    List<Dept> findlist();

    /**
     * 分页查找
     * @param offset
     * @param limit
     * @return
     */
    Pagedao findpage(int offset, int limit);
}
