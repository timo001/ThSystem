package com.yunhe.tianhe.service.client.impl;

import com.yunhe.tianhe.dao.client.ClientCompanyDao;
import com.yunhe.tianhe.entity.client.ClientCompany;
import com.yunhe.tianhe.service.client.ClientCompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/12/1 0001 上午 10:30
 * @Description: TODO
 */
@Service
public class ClientCompanyServiceImpl implements ClientCompanyService {

    @Autowired
    private ClientCompanyDao clientCompanyDao;

    @Override
    public Long save(ClientCompany clientCompany) {
        return clientCompanyDao.save(clientCompany);
    }

    @Override
    public ClientCompany findById(Long cid) {
        return clientCompanyDao.findById(cid);
    }
}
