package com.yunhe.tianhe.controller.client;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.client.Credit;
import com.yunhe.tianhe.service.client.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/23 0023 下午 4:36
 * @Description: TODO
 */
@Controller
@RequestMapping("/credit")
public class CreditController {

    @Autowired
    private CreditService creditService;

    @RequestMapping()
    public String list(){
        return "client/credit/credit";
    }


    @RequestMapping("/add")
    public  String add(HttpServletRequest request){
        return "/client/credit/add";
    }

    @RequestMapping("/save")
    @ResponseBody
    public JSONResponse save(Credit credit){
        creditService.save(credit);
        return JSONResponse.success("保存成功");
    }


    /**
     * 信用记录页面
     * 信用记录列表,分页数据
     * @return
     */
    @RequestMapping("/listData")
    @ResponseBody
    public Pagedao<Credit> findPage(Integer offset, Integer limit, String idcard, String mobile){
        return  creditService.findPage(offset,limit,idcard,mobile);
    }

    /**
     * 删除信用记录
     * @return  跳转到列表页
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JSONResponse delete(String ids){
        creditService.deleteBatch(ids);
        return JSONResponse.success("删除成功");
    }

}
