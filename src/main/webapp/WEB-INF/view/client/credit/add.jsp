<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>

    <div class="box-padding-15">
        <form class="layui-form" action="/credit/save" method="post">

            <div class="layui-form-item">
                <label class="layui-form-label">姓名</label>
                <div class="layui-input-block">
                    <input type="text" name="name"   lay-verify="required" placeholder="请输入姓名" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">手机号码</label>
                <div class="layui-input-block">
                    <input type="text" name="mobile"    lay-verify="required|number" placeholder="请输入手机号码" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">身份证号码</label>
                <div class="layui-input-block">
                    <input type="text" name="idcard"   lay-verify="required" placeholder="请输入身份证号码" autocomplete="off" class="layui-input">
                </div>
            </div>




            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="formSubmit">保存</button>
                    <a href="/vote"><button type="button" class="layui-btn layui-btn-primary">取消</button></a>
                </div>
            </div>
        </form>
    </div>

    <script src="/static/js/plugins/jquery.js"></script>
    <script src="/static/js/plugins/layui/layui.js"></script>
    <script src="/static/js/common.js"></script>
<script>

    layui.use(['element','layer','form','upload'], function(){
        var form = layui.form
        ,upload = layui.upload
        ,element = layui.element
        ,layer = layui.layer;


            //监听提交
          form.on('submit(formSubmit)', function(data){
            //进行异步保存
            $.ajax({
                  url: "/baclist/save",
                  type:"post",
                  dataType:"json",
//                  async:'false',
                  data:data.field,
                  success:function(rs){
                      layer.msg(rs.message,{time:1000},function () {
                          if(rs.code == 1) {
                              location.href="/baclist"
                              parent.$("#tb_page").bootstrapTable("refresh");  //刷新表格数据
                              var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                              parent.layer.close(index); //再执行关闭
                          }
                      });
                  },
                  error:function(){
                      layer.msg("服务器忙，请稍后再试！");
                  }
              });
            return false;
          });
    });


</script>
</body>
</html>

