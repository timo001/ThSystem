package com.yunhe.tianhe.controller.cars;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.Car;
import com.yunhe.tianhe.service.cars.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/23 0023 下午 12:07
 * @Description: TODO
 */
@RequestMapping("/cars")
@Controller
public class CarController {
    @Autowired
    private CarService carService;

    /*列表页*/
    @RequestMapping
    public String list(){
        return "cars/list";
    }
    /**
     * 添加页面
     * @return
     */
    @RequestMapping("/add")
    public  String add(){
        return "cars/add";
    }

    /**
     * 编辑页面
     * @return
     */
    @RequestMapping("/edit")
    public String edit(Long carId, HttpServletRequest request){
        Car car = carService.find(carId);
        request.setAttribute("car",car);
        return "cars/edit";
    }

    /**
     * 保存
     * @return  跳转到列表页
     */
    @RequestMapping("/save")
    public String save(Car car){
        carService.save(car);
        return "redirect:/cars";
    }

    /**
     * 列表页分页数据接口
     * @return
     */
    @RequestMapping("/listData")
    @ResponseBody
    public Pagedao<Car> listData(Integer offset, Integer limit,String searchCode,String btrentdept){
        return  carService.findPage(offset ,limit,searchCode,btrentdept);
    }


    /**
     * 修改
     * @return  跳转到列表页
     */
    @RequestMapping("/update")
    public String update(Car car){
        carService.update(car);
        return "redirect:/cars";
    }


    /**
     * 删除
     * @return  跳转到列表页
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JSONResponse delete(String ids){
        carService.deleteBatch(ids);
        return JSONResponse.success("删除成功");
    }

    /**
     * 验证车牌号
     */
    @RequestMapping("/verifyCarCode")
    @ResponseBody
    public JSONResponse findcarcode(String carcode){
        int a = carService.carcode(carcode);
        int code = 0;
        boolean data = false;
        if (a == 1){
            code = 1;
            data = true;
        }
        return new  JSONResponse(code,"",data);
    }

}
