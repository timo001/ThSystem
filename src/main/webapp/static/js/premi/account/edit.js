/**
 * Created by Administrator on 2017/11/23 0023.
 */
layui.use(['element','layer','form'], function() {
    var layer = layui.layer;
    var form = layui.form;
    var element = layui.element;

    //监听radio单选
    form.on('radio(filter)', function(data){
        var value = data.value; //被点击的radio的value值
        if (value==0) {
            $("#remark").closest(".layui-form-item").removeClass("layui-hide");
            $("#remark").attr("lay-verify","required");
        }else{
            $("#remark").closest(".layui-form-item").addClass("layui-hide");
            $("#remark").removeAttr("lay-verify");
        }
    });


    //监听提交
    form.on('submit(formDemo)', function(data){
        $.ajax({
            url:"/permi/account/update",
            type:"post",
            dataType:"json",
            data:data.field,
            success:function (msg) {
                layer.msg("修改成功！",{time:1000},function () {
                    location.href="/permi/account/list";
                })
            }

        })
        return false;
    })
})