package com.yunhe.tianhe.controller.system;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/29 0029 15:28
 * @Description: 个人信息设置
 */
@Controller
@RequestMapping("/system/personal")
public class PersonalController {

    @RequestMapping("/list")
    public String list(){
        return "system/personal/list";
    }

}
