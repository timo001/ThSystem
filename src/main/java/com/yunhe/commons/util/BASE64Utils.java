package com.yunhe.commons.util;


import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class BASE64Utils
{
   //BASE64加密
    public static String encrypt(String s){
        if(s==null)return null;
        return (new BASE64Encoder()).encode(s.getBytes());
    }
    
    
    //BASE64解密
    public static String decrypt(String s) throws Exception
        {
        if(s==null)return null;
        String decodeStr=null;
        
        byte[] strByte=new BASE64Decoder().decodeBuffer(s);
        decodeStr=new String(strByte);
        
        return decodeStr;
    }
}
