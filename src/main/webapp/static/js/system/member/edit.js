/**
 * Created by Administrator on 2017/11/23 0023.
 */
layui.use(['element','layer','form'], function() {
    var layer = layui.layer;
    var form = layui.form;
    var element = layui.element;

    //监听提交
    form.on('submit(formDemo)', function(data){
        $.ajax({
            url:"/system/level/update",
            type:"post",
            dataType:"json",
            data:data.field,
            success:function (msg) {
                layer.msg("修改成功！",{time:1000},function () {
                    location.href="/system/level/list";
                })
            }
        })
        return false;
    });
})