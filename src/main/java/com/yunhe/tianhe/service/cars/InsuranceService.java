package com.yunhe.tianhe.service.cars;


import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.Insurance;

import java.util.List;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/23 0023 上午 9:03
 * @Description 车辆查询接口
 */
public interface InsuranceService<T> {

    Pagedao<Insurance> findlist(int limit, int offset);   //分页

    void save(Insurance insurance);   //新增

    void delete(String ids);    //删除

    Insurance toupdate(Long recordid);  //根据recordid查询数据，返回数据，为修改做准备

    void update(Insurance insurance);   //修改

    Pagedao<Insurance> findcarcode(String carcode, int limit, int offset);   //根据车牌搜索

}
