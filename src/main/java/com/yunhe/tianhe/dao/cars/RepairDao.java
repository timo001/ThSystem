package com.yunhe.tianhe.dao.cars;

import com.yunhe.tianhe.entity.cars.Repair;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/27 0027 下午 3:28
 * @Description: 车辆维修dao接口
 */
public interface RepairDao {
    Long save(Repair repair);

    void update(Repair repair);

    void delete(Long orderId);

    Repair find(Long orderId);

    Integer findCount(@Param("searchCode")String searchCode);

    List findAll(@Param("offset")Integer offset, @Param("limit") Integer limit, @Param("searchCode")String searchCode);

    List<Repair> findByCarCode(String carCode);
}
