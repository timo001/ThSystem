/*
package com.yunhe.commens.filterservlet;

import com.yunhe.commens.util.BASE64Utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

*/
/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/14 0014 10:22
 * @Description: 拦截器
 *//*

@Component
public class MyInterceptor implements HandlerInterceptor {
    @Autowired
    private PermissionService permissionService ;
    @Autowired
    private Loginservice loginservice;;
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        HttpSession session = httpServletRequest.getSession ();//获取session对象
        Object useName = session.getAttribute ( "userName" );//获取用户
        Map allpremi ;
        allpremi= (Map)session.getAttribute ( "allpremi" );//获取所有的用户权限
        Cookie[] cookies = httpServletRequest.getCookies ();//拿到Cookies中的用户名
        String value="";
        String value1="";
        if (null != cookies) {
            for (Cookie cookie : cookies) {
                if (cookie.getName ().equals ( "userName" )) {
                    value = BASE64Utils.decrypt ( cookie.getValue () );
                }
                if (cookie.getName ().equals ( "password" )) {
                    value1 = BASE64Utils.decrypt ( cookie.getValue () );
                }

            }
        }
        User user = new User ( value, value1 );
        User login = loginservice.login ( user );



        String requestURI = httpServletRequest.getRequestURI ();//得到拦截的路径，如果拦截路径是以下的文件，则跳过拦截，继续执行，不过滤主界面
        if (requestURI.endsWith ( "/login" ) || requestURI.endsWith ( "/" )||requestURI.startsWith ( "/static/" )
                ||requestURI.endsWith ( "/regist" )||requestURI.startsWith("/kaptcha.jpg")||requestURI.startsWith("/voteItem/vote")
                ||requestURI.startsWith("/downimg") ||requestURI.endsWith("/count") ){
           return true;
        }else {
            if (useName==null && value == null || value.isEmpty ()){//如果session和cookie 为空，则返回登录界面
                httpServletResponse.sendRedirect ( httpServletRequest.getContextPath ()+"/" );
                return false;
            }else {
                allpremi = loginservice.findAllpremiss ( login.getRoleId () );//获取用户的所有权限，放入map直接map中
                List<Menus> menusList = loginservice.premirole ( login.getRoleId () );
                session.setAttribute ( "menulist", menusList);//左侧菜单树存入request中时必须转发才能拿到值
                if (null != allpremi && allpremi.containsKey ( requestURI )){
                    Permission permi = (Permission)allpermi.get ( requestURI );
                    if ( premi.getType ()==2){
                        List<Permission> button = permissionService.findButton ( permi.getId () );//获取该页面下的所有权限 ，放入Request中
                        httpServletRequest.setAttribute ( "buttons",button );
                    }
                  return true;
                }else{
                   httpServletResponse.sendError ( 401,"权限不足" );
                    return false;
                }
            }
        }
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
*/
