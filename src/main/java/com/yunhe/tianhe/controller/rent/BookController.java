package com.yunhe.tianhe.controller.rent;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.RentOrder;
import com.yunhe.tianhe.service.rent.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/12/1 0001 上午 11:19
 * @Description: TODO
 */
@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    /*列表页*/
    @RequestMapping
    public String list(){
        return "rent/book/list";
    }

    /**
     * 列表页分页数据接口
     * @return
     */
    @RequestMapping("/listData")
    @ResponseBody
    public Pagedao<RentOrder> listData(Integer offset, Integer limit, String searchCode){
        return  bookService.findPage(offset ,limit,searchCode);
    }
}
