package com.yunhe.commons.util;

/**
 * @author tianxuan
 * @version V1.0
 * @Date 2017/10/12 0012 下午 4:51
 * @Description: 读取config.properties的工具类
 */
public class ConfigUtils {
    private static PropertiesHandler propertiesHandler = new PropertiesHandler("config");

    private ConfigUtils(){}

    /**
     * 读取key值
     * @param key
     * @return
     */
    public static String readValue(String key){
        return propertiesHandler.readValue(key);
    }
}
