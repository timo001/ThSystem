<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/22 0022
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>会员级别列表</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet"/>
    <link href="/static/js/plugins/bootstrap-table/bootstrap-table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/static/js/plugins/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="layui-row box-border box-padding-15">
    <div class="layui-col-md2">
        <div class="grid-demo grid-demo-bg1">
            <div class="layui-form-item">
                <label class="layui-form-label box-width">字典名称：</label>
                <div class="layui-input-block">
                    <input type="text" name="find" id="find"  autocomplete="off" placeholder="请输入字典名称" class="layui-input">
                </div>
            </div>
        </div>
    </div>
    <button class="layui-btn" id="btn_find">查找</button>
</div>
<div class="layui-row ">
    <div class="layui-col-md3 box-right">
        <ul id="treeRole" class="ztree"></ul>
    </div>
    <div class="layui-col-md9">
        <div class="layui-tab layui-tab-card">
            <input type="hidden" name="tabId" id="tabId" value="${param.tabId}">
            <ul class="layui-tab-title">
                <li class="layui-this">增加</li>
                <li id="btn_edit">编辑</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <form class="layui-form" method="post">
                        <div class="layui-form-item">
                            <label class="layui-form-label box-width">父类名称：</label>
                            <div class="layui-input-block">
                                <input type="hidden" name="fid">
                                <input type="text" name="parentname" id="parent" lay-verify="parentname"
                                       value="${sysdict.name}" placeholder="请输入父类名称" autocomplete="off"
                                       class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label box-width">类别名称：</label>
                            <div class="layui-input-block">
                                <input type="text" name="name" lay-verify="required" placeholder="请输入类别名称"
                                       autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label box-width">类别代码：</label>
                            <div class="layui-input-block">
                                <input type="text" name="code" lay-verify="required" placeholder="请输入类别代码"
                                       autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="layui-tab-item">
                    <form class="layui-form" method="post">
                        <div class="layui-form-item">
                            <label class="layui-form-label box-width">父类名称：</label>
                            <div class="layui-input-block">
                                <input type="hidden" name="fid" value="${sysdict1.id}">
                                <input type="text" name="parentname" id="parentname" lay-verify="parentname"
                                       value="${sysdict1.name}" placeholder="请输入父类名称" autocomplete="off"
                                       class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label box-width">类别名称：</label>
                            <div class="layui-input-block">
                                <input type="hidden" name="id" value="${sysdict.name}">
                                <input type="text" name="name" value="${sysdict.name}" id="name" lay-verify="required"
                                       placeholder="请输入类别名称" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label box-width">类别代码：</label>
                            <div class="layui-input-block">
                                <input type="text" name="code" value="${sysdict.code}" id="code" lay-verify="required"
                                       placeholder="请输入类别代码" autocomplete="off" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="formedit">保存</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="findlist">
        <table id="tb_departments" class="box-table"></table>
    </div>
</div>

<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script type="text/javascript" src="/static/js/plugins/zTree_v3/js/jquery.ztree.core.js"></script>
<script type="text/javascript" src="/static/js/plugins/zTree_v3/js/jquery.ztree.excheck.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="/static/js/system/sysdict/list.js"></script>
</body>
</html>
