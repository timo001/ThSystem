package com.yunhe.tianhe.service.cars.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.util.StringUtils;
import com.yunhe.tianhe.dao.cars.CarDao;
import com.yunhe.tianhe.entity.cars.Car;
import com.yunhe.tianhe.service.cars.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/23 0023 上午 11:16
 * @Description: 车辆基本信息业务处理接口实现类
 */

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarDao carDao;

    @Override
    public Long save(Car car) {
        carDao.save(car);
        Long carId = car.getCarId();
        return carId;
    }

    @Override
    public void update(Car car) {
        carDao.update(car);
    }

    @Override
    public void delete(Long carId) {
        carDao.delete(carId);
    }

    @Override
    public void deleteBatch(String ids) {
        if(StringUtils.notEmpty(ids)){
            String[] idArr = ids.split(",");
            for(String id : idArr){
                this.delete(Long.valueOf(id));
            }
        }
    }

    @Override
    public Car find(Long carId) {
        Car car = carDao.find(carId);
        return car;
    }

    @Override
    public Pagedao<Car> findPage(Integer offset, Integer limit,String searchCode,String btrentdept) {
        Pagedao<Car> pagedao = new Pagedao<>();
        Integer count = carDao.findCount(searchCode,btrentdept);
        List<Car> data = null;
        if(count > 0){
            pagedao.setPage(offset);
            pagedao.setSize(limit);
            data = carDao.findAll(offset,limit,searchCode,btrentdept);
        }
        pagedao.setTotal(count);
        pagedao.setRows(data);
        return pagedao;
    }

    @Override
    public int carcode(String carcode) {
        Car car = carDao.carcode(carcode);
        int a = 0;
        if (car != null){
            a = 1;       //表示查到信息，有此车
        }
        return a;
    }
}
