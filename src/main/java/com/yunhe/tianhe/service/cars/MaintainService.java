package com.yunhe.tianhe.service.cars;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.Maintain;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/25 0025 下午 1:38
 * @Description TODO
 */
public interface MaintainService<E> {
    //分页(搜索)
    Pagedao<Maintain> list(@Param("limit")Integer limit,@Param("offset") Integer offset,@Param("carcode")String carcode);

    //删除
    void delete(String ids);
    //添加
    void save(Maintain maintain);
    //查询为修改做准备
    Maintain edit(Long recordid);
    //修改
    void update(Maintain maintain);

}
