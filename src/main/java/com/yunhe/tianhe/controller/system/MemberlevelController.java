package com.yunhe.tianhe.controller.system;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.system.Memberlevel;
import com.yunhe.tianhe.service.system.MemberlevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 14:04
 * @Description: 系统会员级别操作
 */
@Controller
@RequestMapping("/system/level")
public class MemberlevelController {
    @Autowired
    private MemberlevelService memberlevelService;

    @RequestMapping("/list")
    public String list(){
        return "system/member/list";
    }
    @RequestMapping("/add")
    public String add(){
        return "system/member/add";
    }

    @RequestMapping("/edit")
    public ModelAndView edit(Long levelid){
        Memberlevel memberlevel = memberlevelService.find ( levelid );
        return new ModelAndView ( "system/member/edit","member",memberlevel );
    }

    @RequestMapping("/save")
    @ResponseBody
    public JSONResponse save(Memberlevel memberlevel){
        memberlevelService.save ( memberlevel );
        return new JSONResponse (1,"保存成功！");
    }

    @RequestMapping("/update")
    @ResponseBody
    public JSONResponse update(Memberlevel memberlevel){
        memberlevelService.update ( memberlevel );
        return new JSONResponse (1,"保存成功！");
    }

    @RequestMapping("/deleted")
    @ResponseBody
    public JSONResponse deleted(String ids){
        memberlevelService.deleted ( ids );
        return new JSONResponse(1,"删除成功！");
    }


    @RequestMapping("/page")
    @ResponseBody
    public Pagedao page(int offset, int limit){
        Pagedao findpage = memberlevelService.findpage ( offset, limit );
        return findpage;
    }
}
