package com.yunhe.commons.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 列表
 */
public class Pagedao<E> {
    private List<E> rows;
    private int page=1;//当前页
    private int size=10;//每页 的数据个数
    private Integer total;//数据总数
    private int pageCount;//总页数
    private boolean first;
    private boolean last;

    public boolean isFirst() {
        return first;
    }

    public boolean isLast() {
        return last;
    }

    public List<E> getRows() {
        return rows;
    }

    public void setRows(List<E> rows) {
        if (null == rows ){  //一次全删时，rows 为null，存在缓存，不能及时更新
            this.rows = new ArrayList ();
        }else{
            this.rows = rows;
        }
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
        this.pageCount= (total%size==0)? total/size:total/size+1;
        this.first= (page==1)? true:false;
        this.last= (page==pageCount)?true:false;
        this.total = total;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
    public void setPage(String page) {
        if (page!=null){
            this.page = Integer.valueOf ( page );
        }
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    public void setSize(String size) {
        if (size!=null){
            this.size = Integer.valueOf ( size );
        }
    }


    public int getPageCount() {
        return pageCount;
    }

}
