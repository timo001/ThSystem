package com.yunhe.tianhe.dao.cars;

import com.yunhe.tianhe.entity.cars.Car;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/23 0023 上午 9:18
 * @Description: 车辆基本信息管理接口
 */
public interface CarDao {

    Long save(Car car);

    void update(Car car);

    void delete(Long carId);

    Car find(Long carId);

    Integer findCount(@Param("searchCode")String searchCode,@Param("btrentdept") String btrentdept);

    List findAll(@Param("offset")Integer offset,@Param("limit") Integer limit,@Param("searchCode")String searchCode,@Param("btrentdept") String btrentdept);

    List<Car> findByCarCode(String carCode);

    Car carcode(String carcode);    //查看是否有此车
}
