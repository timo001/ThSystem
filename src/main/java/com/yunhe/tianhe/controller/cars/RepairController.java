package com.yunhe.tianhe.controller.cars;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.Repair;
import com.yunhe.tianhe.service.cars.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/27 0027 下午 5:10
 * @Description: TODO
 */
@Controller
@RequestMapping("/repair")
public class RepairController {
    @Autowired
    private RepairService repairService;

    /*列表页*/
    @RequestMapping
    public String list(){
        return "cars/repair/list";
    }

    /**
     * 添加页面
     * @return
     */
    @RequestMapping("/add")
    public  String add(){
        return "cars/repair/add";
    }

    /**
     * 编辑页面
     * @return
     */
    @RequestMapping("/edit")
    public String edit(Long recordId, HttpServletRequest request){
        Repair repair = repairService.find(recordId);
        request.setAttribute("repair",repair);
        return "cars/repair/edit";
    }

    /**
     * 保存
     * @return  跳转到列表页
     */
    @RequestMapping("/save")
    public String save(Repair repair){
        repairService.save(repair);
        return "redirect:/repair";
    }

    /**
     * 列表页分页数据接口
     * @return
     */
    @RequestMapping("/listData")
    @ResponseBody
    public Pagedao<Repair> listData(Integer offset, Integer limit, String searchCode){
        return  repairService.findPage(offset ,limit,searchCode);
    }


    /**
     * 修改
     * @return  跳转到列表页
     */
    @RequestMapping("/update")
    public String update(Repair repair){
        repairService.update(repair);
        return "redirect:/repair";
    }


    /**
     * 删除
     * @return  跳转到列表页
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JSONResponse delete(String ids){
        repairService.deleteBatch(ids);
        return JSONResponse.success("删除成功");
    }
}
