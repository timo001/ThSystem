package com.yunhe.tianhe.service.system;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.dto.TreeNode;
import com.yunhe.tianhe.entity.system.Sysdict;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 16:01
 * @Description:
 */
public interface SysdictService {

    /**
     * 添加
     * @param sysdict
     */
    void save(Sysdict sysdict);

    /**
     * 删除
     * @param ids
     */
    void deleted(String ids);

    /**
     * 修改
     * @param sysdict
     */
    void update(Sysdict sysdict);

    /**
     * 单个查找
     * @param id
     * @return
     */
    Sysdict find(Long id);

    /**
     * 查找
     * @return
     */
    List<Sysdict> findlist(Sysdict sysdict);

    /**
     * 查找
     * @return
     */
    List<TreeNode> findtree(Sysdict sysdict);



    /**
     * 分页
     * @param offset
     * @param limit
     * @return
     */
    Pagedao<Sysdict> findpage(Sysdict sysdict,int offset,  int limit);
}
