package com.yunhe.tianhe.dao.cars;


import com.yunhe.tianhe.entity.cars.Insurance;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/23 0023 上午 9:08
 * @Description 车辆查询Dao层接口
 */
public interface InsuranceDao<T> {

    int pagedao(String carcode);  //分页（统计总数）

    List<Insurance> queryPage(@Param("page") int page, @Param("size") int size, @Param("carcode") String carcode);//分页

    void save(Insurance insurance);   //新增

    void delete(Integer id);             //删除

    Insurance toupdate(Long recordid);  //根据recordid查询数据，返回数据，为修改做准备

    void update(Insurance insurance);      //修改

    List<Insurance> findcarcode(String carcode);   //根据车牌搜索

}
