package com.yunhe.tianhe.entity.system;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 15:11
 * @Description: 系统字典管理
 */
public class Sysdict {
    private Long id; //字典编号
    private Long fid=0l; //父类编号
    private String name; //字典名称
    private String code; //字典代码
    private Integer ordernum; //字典排序
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addtime; //添加时间(系统时间)

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFid() {
        return fid;
    }

    public void setFid(Long fid) {
        this.fid = fid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getOrdernum() {
        return ordernum;
    }

    public void setOrdernum(Integer ordernum) {
        this.ordernum = ordernum;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}
