package com.yunhe.tianhe.service.premission;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.premission.Resource;
import com.yunhe.tianhe.entity.premission.Role;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/29 0029 17:32
 * @Description: 权限操作
 */
public interface ResourceService {

    void save(Resource resource);

    void update(Resource resource);

    void deleted(String ids);

    Resource find(Long permisid);

    List<Resource> findlist(Resource resource);

    Pagedao<Resource> findpage(Resource resource,int offset, int limit);
}

