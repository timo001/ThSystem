<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/10/17 0017
  Time: 上午 10:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="box-padding-15">
    <span class="layui-breadcrumb" lay-separator=">">
        <a href="/" target="_top">首页</a>
        <a href="/premi/resource/list">权限管理</a>
        <a><cite>编辑</cite></a>
    </span>
    <form  method="post" class="layui-form">
        <div class="layui-form-item">
            <label class="layui-form-label">权限名称</label>
            <div class="layui-input-block">
                <input type="text" name="permisname" value="${resource.permisname}"  lay-verify="required" placeholder="请输入权限名称" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">权限类型</label>
            <div class="layui-input-block">
                <select name="permistype"  lay-verify="required" lay-filter="type">
                    <option value="1" <c:if test="${resource.permistype eq 1}">selected</c:if>>模块</option>
                    <option value="2" <c:if test="${resource.permistype eq 2}">selected</c:if>>功能</option>
                    <option value="3" <c:if test="${resource.permistype eq 3}">selected</c:if>>按钮</option>
                    <option value="4" <c:if test="${resource.permistype eq 4}">selected</c:if>>附属操作</option>
                    <option value="9" <c:if test="${resource.permistype eq 9}">selected</c:if>>默认权限</option>
                </select>
            </div>
        </div>
        <div id="mstyle" class="layui-form-item <c:if test="${resource.permistype eq 1 ||resource.permistype eq 9}">layui-hide</c:if>" >
            <label class="layui-form-label">父级权限</label>
            <div class="layui-input-block">
                <select name="parentid"  id="module" >
                    <c:forEach items="${resourcelist}" var="resource">
                        <option value="${resource.permisid}" <c:if test="${resource.permisid} eq ${resource.parentid}">selected</c:if> >${resource.permisname} </option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div id="utyle" class="layui-form-item <c:if test="${resource.permistype eq 1 }">layui-hide</c:if>">
            <label class="layui-form-label">URI</label>
            <div class="layui-input-block">
                <input type="text" name="relateurl" value="${resource.relateurl}"  placeholder="请输入URl" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div id="btnId" class="layui-form-item <c:if test="${resource.permistype != 3 }">layui-hide</c:if>">
            <label class="layui-form-label">按钮Id</label>
            <div class="layui-input-block">
                <input type="text" name="buttonId"   placeholder="请输入按钮ID" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-block">
                <input type="checkbox" name="isabled" value="1" <c:if test="${resource.isabled eq 1}">checked</c:if> title="启用">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="formSubmit">保存</button>
                <a href="/permi/resource/list"> <button type="button" class="layui-btn layui-btn-primary">取消</button></a>
            </div>
        </div>
    </form>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/premi/resource/edit.js"></script>
</body>
</html>
