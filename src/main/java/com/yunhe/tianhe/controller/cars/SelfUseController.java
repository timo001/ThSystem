package com.yunhe.tianhe.controller.cars;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.SelfUse;
import com.yunhe.tianhe.service.cars.SelfUseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/24 0024 下午 8:01
 * @Description: TODO
 */
@RequestMapping("/selfuse")
@Controller
public class SelfUseController {

    @Autowired
    private SelfUseService selfUseService;
    /*列表页*/
    @RequestMapping
    public String list(){
        return "cars/selfuse/list";
    }

    /**
     * 添加页面
     * @return
     */
    @RequestMapping("/add")
    public  String add(){
        return "cars/selfuse/add";
    }

    /**
     * 编辑页面
     * @return
     */
    @RequestMapping("/edit")
    public String edit(Long recordId, HttpServletRequest request){
        SelfUse selfUse = selfUseService.find(recordId);
        request.setAttribute("selfuse",selfUse);
        return "cars/selfuse/edit";
    }

    /**
     * 保存
     * @return  跳转到列表页
     */
    @RequestMapping("/save")
    public String save(SelfUse selfUse){
        selfUseService.save(selfUse);
        return "redirect:/selfuse";
    }

    /**
     * 列表页分页数据接口
     * @return
     */
    @RequestMapping("/listData")
    @ResponseBody
    public Pagedao<SelfUse> listData(Integer offset, Integer limit,String searchCode){
        return  selfUseService.findPage(offset ,limit,searchCode);
    }


    /**
     * 修改
     * @return  跳转到列表页
     */
    @RequestMapping("/update")
    public String update(SelfUse selfUse){
        selfUseService.update(selfUse);
        return "redirect:/selfuse";
    }


    /**
     * 删除
     * @return  跳转到列表页
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JSONResponse delete(String ids){
        selfUseService.deleteBatch(ids);
        return JSONResponse.success("删除成功");
    }

}
