package com.yunhe.tianhe.service.system.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.dao.system.BusilogDao;
import com.yunhe.tianhe.entity.system.Busilog;
import com.yunhe.tianhe.service.system.BusilogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 10:55
 * @Description: 实现BusilogService
 */
@Service
public class BusilogServiceImpl implements BusilogService{

    @Autowired
    private BusilogDao busilogDao;

    @Override
    public void save(Busilog busilog) {
        busilogDao.save ( busilog );
    }

    @Override
    public Busilog find(Long logid) {
        return busilogDao.find ( logid );
    }

    @Override
    public List<Busilog> findlist() {
        return busilogDao.findlist ();
    }

    @Override
    public Pagedao<Busilog> findpage(Busilog busilog,int offset, int limit) {
        Pagedao<Busilog> pagedao = new Pagedao<> ();
        int count = busilogDao.count ();
        List<Busilog> findpage=null;
        if (count>0){
             findpage = busilogDao.findpage (busilog, offset, limit );
             pagedao.setPage ( offset/limit+1 );
             pagedao.setSize ( limit );
        }
        pagedao.setRows ( findpage );
        pagedao.setTotal ( count );
        return pagedao;
    }
}
