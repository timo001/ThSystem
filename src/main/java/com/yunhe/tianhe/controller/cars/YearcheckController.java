package com.yunhe.tianhe.controller.cars;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.Yearcheck;
import com.yunhe.tianhe.service.cars.YearcheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/27 0027 下午 4:54
 * @Description TODO
 */
@Controller
@RequestMapping("/yearcheck")
public class YearcheckController {
    @Autowired
    private YearcheckService yearcheckService;


    @RequestMapping("/yearchecklist")        //到年检列表
    public String maintainlist(){
        return "cars/yearcheck/list";
    }

    @RequestMapping("/page")       //分页
    @ResponseBody
    public Pagedao list(Integer limit, Integer offset,String carcode){
        Pagedao list = yearcheckService.list(limit, offset,carcode);
        return list;
    }

    @RequestMapping("/add")
    public String add(){
        return "cars/yearcheck/add";
    }

    @RequestMapping("/save")
    public String save(Yearcheck yearcheck){
        yearcheckService.save(yearcheck);
        return "redirect:/yearcheck/yearchecklist";
    }
    @RequestMapping("/delete")
    @ResponseBody
    public JSONResponse delete(String ids){
        yearcheckService.delete(ids);
        return JSONResponse.success("删除成功");
    }

    @RequestMapping("/edit")
    public ModelAndView edit(Long recordid){
        Yearcheck edit = yearcheckService.edit(recordid);
        return new ModelAndView("cars/yearcheck/edit","year",edit);
    }

    @RequestMapping("/update")
    public String update(Yearcheck yearcheck){
        yearcheckService.update(yearcheck);
        return "redirect:/yearcheck/yearchecklist";
    }

}
