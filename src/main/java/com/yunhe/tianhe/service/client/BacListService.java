package com.yunhe.tianhe.service.client;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.client.BacList;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/23 0023 下午 4:37
 * @Description: TODO
 */
public interface BacListService {

    /**
     * 加入黑名单
     */
    void save(BacList bacList);

    /**
     * 黑名单列表
     * @return
     */
    Pagedao<BacList> findPage(Integer offset, Integer limit,String name,String idcard,String mobile);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**
     * 批量删除
     * @param ids
     */
    void deleteBatch(String ids);

    /**
     * 根据ID查询详情
     * @param id
     * @return
     */
    BacList findById(Long id);

    /**
     * 修改黑名单信息
     * @param bacList
     */
    void update(BacList bacList);
}
