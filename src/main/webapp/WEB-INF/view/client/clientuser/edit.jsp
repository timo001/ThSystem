<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>

    <div class="box-padding-15">
        <form class="layui-form" action="/clientuser/update" method="post">
            <div class="layui-form-item">
                <label class="layui-form-label">手机号码</label>
                <div class="layui-input-block">
                    <input type="hidden" name="clientPwd" id="1" value="${clientUser.clientPwd}">
                    <input type="hidden" name="addtime" id="2" value="${clientUser.addTime}">
                    <input type="text" name="mobile" disabled value="${clientUser.mobile}"  lay-verify="required|number" placeholder="${clientUser.mobile}" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">姓名</label>
                <div class="layui-input-block">
                    <input type="text" name="name"  value="${clientUser.name}"  lay-verify="required" placeholder="请输入姓名" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">身份证号码</label>
                <div class="layui-input-block">
                    <input type="text" name="idcard"  value="${clientUser.idcard}"  lay-verify="required" placeholder="请输入身份证号码" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">驾驶证号码</label>
                <div class="layui-input-block">
                    <input type="text" name="driverNum"  value="${clientUser.driverNum}"  lay-verify="required" placeholder="请输入驾驶证号码" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">企业代码</label>
                <div class="layui-input-block">
                    <input type="text" name="companyId"  value="${clientUser.companyId}"  lay-verify="required" placeholder="请输入企业代码" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">信用分值</label>
                <div class="layui-input-block">
                    <input type="text" name="creditNum"  value="${clientUser.creditNum}"  lay-verify="required" placeholder="请输入信用分值" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">会员级别</label>
                <div class="layui-input-block">
                    <input type="text" name="memLevel"  value="${clientUser.memLevel}"  lay-verify="required" placeholder="请输入会员级别" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">客户状态</label>
                <div class="layui-input-block">
                    <input type="text" name="clientState"  value="${clientUser.clientState}"  lay-verify="required" placeholder="请输入客户状态" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-input-block">
                    <button class="layui-btn" lay-submit lay-filter="formSubmit">保存</button>
                    <a href="/vote"><button type="button" class="layui-btn layui-btn-primary">取消</button></a>
                </div>
            </div>
        </form>
    </div>

    <script src="/static/js/plugins/jquery.js"></script>
    <script src="/static/js/plugins/layui/layui.js"></script>
    <script src="/static/js/common.js"></script>
<script>

    layui.use(['element','layer','form','upload'], function(){
        var form = layui.form
        ,upload = layui.upload
        ,element = layui.element
        ,layer = layui.layer;


        //普通图片上传
        var uploadInst = upload.render({
            elem: '#upload_btn'
            ,url: '/upload'
            ,before: function(obj){
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result){
                    $('#img1').attr('src', result); //图片链接（base64）
                });
            }
            ,done: function(res,index,result){
                //上传成功
                if(res.code == 1) {
                    $("#img").val(res.data[0]);
                    $('#uploadMsg').empty();
                }else {
                    var demoText = $('#demoText');
                    demoText.html('<span style="color: #FF5722;">'+res.message+'</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                    demoText.find('.demo-reload').on('click', function(){
                        uploadInst.upload();
                    });
                }

            }
            ,error: function(){
                //演示失败状态，并实现重传
                var demoText = $('#demoText');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function(){
                    uploadInst.upload();
                });
            }
        });

            //监听提交
          form.on('submit(formSubmit)', function(data){
            //进行异步保存
            $.ajax({
                  url: "/clientuser/update",
                  type:"post",
                  dataType:"json",
                  data:data.field,
                  success:function(rs){
                      layer.msg(rs.message,{time:1000},function () {
                          if(rs.code == 1) {
                              location.href="/clientuser"
                              parent.$("#tb_page").bootstrapTable("refresh");  //刷新表格数据
                              var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                              parent.layer.close(index); //再执行关闭
                          }
                      });
                  },
                  error:function(){
                      layer.msg("服务器忙，请稍后再试！");
                  }
              });
            return false;
          });
    });


</script>
</body>
</html>

