package com.yunhe.tianhe.controller.rent;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.Car;
import com.yunhe.tianhe.entity.cars.RentOrder;
import com.yunhe.tianhe.entity.client.ClientCompany;
import com.yunhe.tianhe.entity.client.ClientUser;
import com.yunhe.tianhe.entity.system.Dept;
import com.yunhe.tianhe.service.cars.CarService;
import com.yunhe.tianhe.service.client.ClientUserService;
import com.yunhe.tianhe.service.system.DeptServive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/30 0030 上午 10:51
 * @Description 租车登记
 */
@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private ClientUserService clientUserService;
    @Autowired
    private DeptServive deptServive;
    @Autowired
    private CarService carService;

    @RequestMapping("/list")
    public String list(){
        return "rent/register/p1";
    }

    @RequestMapping("/add")
    public ModelAndView add(ClientUser clientUser, ClientCompany clientCompany){
        clientUserService.add(clientUser,clientCompany);
        return new ModelAndView("rent/register/p2","user",clientUser);
    }

    @RequestMapping("/save")
    public ModelAndView save(RentOrder rentOrder, HttpServletRequest req){
        //todo  保存数据到数据库
        HttpSession session = req.getSession();
        session.setAttribute("rent",rentOrder);
        return new  ModelAndView("rent/register/p3","rent",rentOrder);
    }

    @RequestMapping("/dept")
    @ResponseBody
    public JSONResponse dept(){
        List <Dept> findlist = deptServive.findlist();
        return JSONResponse.success("",findlist);
    }

    @RequestMapping("/page")
    @ResponseBody
    public Pagedao list(Integer offset, Integer limit,HttpServletRequest req){
        HttpSession session = req.getSession();
        RentOrder rentOrder  = (RentOrder)session.getAttribute("rent");
        String btRentDept = rentOrder.getBtRentDept();
        Pagedao <Car> page = carService.findPage(offset,limit,null,btRentDept);
        return page;
    }

    @RequestMapping("/phone")
    @ResponseBody
    public JSONResponse phone(String phone){
        ClientUser byMobile = clientUserService.findByMobile(phone);

        return null;
    }


}
