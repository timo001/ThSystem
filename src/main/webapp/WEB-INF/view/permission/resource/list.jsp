<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/22 0022
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>角色管理</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet"/>
    <link href="/static/js/plugins/bootstrap-table/bootstrap-table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="box-padding-15">
    <span class="layui-breadcrumb" lay-separator=">">
        <a href="/" target="_top">首页</a>
        <a href="/premi/resource/list">权限管理</a>
        <a><cite>列表</cite></a>
    </span>
    <div id="toolbar"  class="btn-group ">
        <button class="layui-btn" id="btn_add">增加</button>
        <button class="layui-btn" id="btn_edit" >编辑</button>
        <button class="layui-btn" id="btn_deleted" >删除</button>
    </div>
    <table id="tb_departments"></table>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/premi/resource/list.js"></script>
</body>
</html>
