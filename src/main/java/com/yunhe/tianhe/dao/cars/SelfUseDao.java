package com.yunhe.tianhe.dao.cars;

import com.yunhe.tianhe.entity.cars.SelfUse;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/24 0024 下午 4:19
 * @Description: 车辆自用dao接口
 */
public interface SelfUseDao {
    Long save(SelfUse selfUse);

    void update(SelfUse selfUse);

    void delete(Long recordId);

    SelfUse find(Long recordId);

    Integer findCount(@Param("searchCode")String searchCode);

    List findAll(@Param("offset")Integer offset, @Param("limit") Integer limit,@Param("searchCode")String searchCode);

    List<SelfUse> findByCarCode(String carCode);
}
