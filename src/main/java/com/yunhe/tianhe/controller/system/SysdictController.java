package com.yunhe.tianhe.controller.system;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.dto.TreeNode;
import com.yunhe.tianhe.entity.system.Memberlevel;
import com.yunhe.tianhe.entity.system.Sysdict;
import com.yunhe.tianhe.service.system.MemberlevelService;
import com.yunhe.tianhe.service.system.SysdictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 14:04
 * @Description: 系统会员级别操作
 */
@Controller
@RequestMapping("/system/sysdict")
public class SysdictController {
    @Autowired
    private SysdictService sysdictService;

    @RequestMapping("/list")
    public String list(){
        return "system/sysdict/list";
    }

    @RequestMapping("/edit")
    @ResponseBody
    public JSONResponse edit(Long id){
        Sysdict sysdict = sysdictService.find ( id );
        Sysdict sysdict1 = sysdictService.find ( sysdict.getFid () );
        return  new JSONResponse (sysdict,sysdict1);
    }

    @RequestMapping("/save")
    @ResponseBody
    public JSONResponse save(Sysdict sysdict){
        sysdictService.save ( sysdict );
        return new JSONResponse (1,"保存成功！");
    }

    @RequestMapping("/update")
    @ResponseBody
    public JSONResponse update(Sysdict sysdict){
        sysdictService.update ( sysdict );
        return new JSONResponse (1,"保存成功！");
    }

    @RequestMapping("/deleted")
    @ResponseBody
    public JSONResponse deleted(String ids){
        sysdictService.deleted ( ids );
        return new JSONResponse(1,"删除成功！");
    }


    @RequestMapping("/page")
    @ResponseBody
    public Pagedao page(Sysdict sysdict,int offset, int limit){
        Pagedao findpage = sysdictService.findpage ( sysdict,offset, limit );
        return findpage;
    }

    @RequestMapping("/parent")
    @ResponseBody
    public JSONResponse page( Sysdict sysdict){
        int code=0;
        String msg="该父级名称不存在";
        List<Sysdict> findlist = sysdictService.findlist ( sysdict );
        if (null != findlist && findlist.size ()==1 ){
            code=1;
        }
        return new JSONResponse ( code,msg,findlist );
    }

    @RequestMapping("/tree")
    @ResponseBody
    public JSONResponse tree(Sysdict sysdict){
        List<TreeNode> findtree = sysdictService.findtree ( sysdict );
        return new JSONResponse ( 1,"加载节点数",findtree );
    }
}
