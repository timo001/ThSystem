package com.yunhe.tianhe.entity.cars;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/22 0022 下午 5:33
 * @Description: 车辆基本信息记录表
 */
public class Car {

    /*车辆编号*/
    private Long carId;
    /*车牌号码*/
    private String carCode;
    /*车牌颜色*/
    private String carColor;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    /*购买日期*/
    private Date buyDate;
    /*发动机号*/
    private String engineNum;
    /*车架号码*/
    private String frameNum;
    /*车辆类型*/
    private Integer carType;
    /*车辆品牌*/
    private String carBrand;
    /*品牌型号*/
    private String carBrandType;
    /*车辆图片*/
    private String pic;
    /*自动手动*/
    private Integer isAuto = 0;
    /*排量大小*/
    private String engineSize;
    /*座位数量*/
    private Integer seatNum;
    /*外观类型*/
    private String faceType;
    /*车辆里程*/
    private Double totalMile;
    /*现有违章*/
    private Integer hadIllegal;

    /*投保单位*/
    private String insurer;
    /*保险时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date insuranceTime;
    /*年检时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date yearCheck;
    /*保养时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date maintainTime;
    /*保养里程*/
    private Double maintainMile;
    /*事故车辆*/
    private Integer hadAccident;
    /*是否大修*/
    private Integer isBadRepair;
    /*伪造里程*/
    private Integer isFakemile;
    /*安全系数*/
    private Integer safeLevel;
    /*车辆每日租金价格*/
    private Double carPrice;
    /*评估人员*/
    private String evaluater;

    /*车辆状态*/
    private Integer carState;
    /*添加时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date addTime;
    /*所在区域城市*/
    private String carCity;
    /*所属租赁分店*/
    private String deptName;
    /*提醒手机号码*/
    private String phone;

    /*合同日期*/
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date contractDate;
    /*车辆信息*/
    private String carRemark;


    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getCarCode() {
        return carCode;
    }

    public void setCarCode(String carCode) {
        this.carCode = carCode;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    public String getEngineNum() {
        return engineNum;
    }

    public void setEngineNum(String engineNum) {
        this.engineNum = engineNum;
    }

    public String getFrameNum() {
        return frameNum;
    }

    public void setFrameNum(String frameNum) {
        this.frameNum = frameNum;
    }

    public Integer getCarType() {
        return carType;
    }

    public void setCarType(Integer carType) {
        this.carType = carType;
    }

    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(String carBrand) {
        this.carBrand = carBrand;
    }

    public String getCarBrandType() {
        return carBrandType;
    }

    public void setCarBrandType(String carBrandType) {
        this.carBrandType = carBrandType;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public Integer getIsAuto() {
        return isAuto;
    }

    public void setIsAuto(Integer isAuto) {
        this.isAuto = isAuto;
    }

    public String getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(String engineSize) {
        this.engineSize = engineSize;
    }

    public Integer getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(Integer seatNum) {
        this.seatNum = seatNum;
    }

    public String getFaceType() {
        return faceType;
    }

    public void setFaceType(String faceType) {
        this.faceType = faceType;
    }

    public Double getTotalMile() {
        return totalMile;
    }

    public void setTotalMile(Double totalMile) {
        this.totalMile = totalMile;
    }

    public Integer getHadIllegal() {
        return hadIllegal;
    }

    public void setHadIllegal(Integer hadIllegal) {
        this.hadIllegal = hadIllegal;
    }

    public String getInsurer() {
        return insurer;
    }

    public void setInsurer(String insurer) {
        this.insurer = insurer;
    }

    public Date getInsuranceTime() {
        return insuranceTime;
    }

    public void setInsuranceTime(Date insuranceTime) {
        this.insuranceTime = insuranceTime;
    }

    public Date getYearCheck() {
        return yearCheck;
    }

    public void setYearCheck(Date yearCheck) {
        this.yearCheck = yearCheck;
    }

    public Date getMaintainTime() {
        return maintainTime;
    }

    public void setMaintainTime(Date maintainTime) {
        this.maintainTime = maintainTime;
    }

    public Double getMaintainMile() {
        return maintainMile;
    }

    public void setMaintainMile(Double maintainMile) {
        this.maintainMile = maintainMile;
    }

    public Integer getHadAccident() {
        return hadAccident;
    }

    public void setHadAccident(Integer hadAccident) {
        this.hadAccident = hadAccident;
    }

    public Integer getIsBadRepair() {
        return isBadRepair;
    }

    public void setIsBadRepair(Integer isBadRepair) {
        this.isBadRepair = isBadRepair;
    }

    public Integer getIsFakemile() {
        return isFakemile;
    }

    public void setIsFakemile(Integer isFakemile) {
        this.isFakemile = isFakemile;
    }

    public Integer getSafeLevel() {
        return safeLevel;
    }

    public void setSafeLevel(Integer safeLevel) {
        this.safeLevel = safeLevel;
    }

    public Double getCarPrice() {
        return carPrice;
    }

    public void setCarPrice(Double carPrice) {
        this.carPrice = carPrice;
    }

    public String getEvaluater() {
        return evaluater;
    }

    public void setEvaluater(String evaluater) {
        this.evaluater = evaluater;
    }

    public Integer getCarState() {
        return carState;
    }

    public void setCarState(Integer carState) {
        this.carState = carState;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Date getContractDate() {
        return contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    public String getCarRemark() {
        return carRemark;
    }

    public void setCarRemark(String carRemark) {
        this.carRemark = carRemark;
    }

    public String getCarCity() {
        return carCity;
    }

    public void setCarCity(String carCity) {
        this.carCity = carCity;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

