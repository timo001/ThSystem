<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/12/1 0001
  Time: 下午 4:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加</title>
    <link rel="stylesheet" href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="/static/js/plugins/bootstrap-table/bootstrap-table.css">
    <link rel="stylesheet" href="/static/css/common.css">
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
</head>
<body>
<div class="layui-box box-padding-15">
<span class="layui-breadcrumb">
    <a href="/" target="_parent">首页</a>
                <a href="/insurance/insurancelist">车辆保险</a>
                <a><cite>列表</cite></a>
</span>
    <hr>
    <div class="layui-form-item">
        <label class="layui-form-label">客户手机</label>
        <div class="layui-input-block">
            <input name="mobile" lay-verify="required" value="${rent.mobile}"  autocomplete="off" class="layui-input">
        </div>
    </div>
</div>
<table id="tb_departments"></table>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script>
    $(function () {
        layui.use(['form','element','layer'], function(){
            var element = layui.element;
            var layer = layui.layer;

            $("#tb_departments").bootstrapTable({
                url: '/register/page',         //请求后台的URL（*）
                method: 'get',                      //请求方式（*）
                //toolbar: '#toolbar',                //工具按钮用哪个容器
                striped: true,                      //是否显示行间隔色
                cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
                pagination: true,                   //是否显示分页（*）
                //sortable: false,                     //是否启用排序
                // sortOrder: "asc",                   //排序方式
                //             queryParams: oTableInit.queryParams,//传递参数（*）
                sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
                pageNumber:1,                       //初始化加载第一页，默认第一页
                pageSize: 10,                           //每页的记录行数（*）
                pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
                //search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
                strictSearch: true,                 //  ? ? ?
                // showColumns: true,                  //是否显示所有的列
                //showRefresh: true,                  //是否显示刷新按钮
                //           minimumCountColumns: 2,             //最少允许的列数
                clickToSelect: true,                //是否启用点击选中行
                height: 600,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
                uniqueId: "id",                     //每一行的唯一标识，一般为主键列
                //showToggle:true,                    //是否显示详细视图和列表视图的切换按钮
                cardView: false,                    //是否显示详细视图
                detailView: false,                   //是否显示父子表
                columns: [{
                    checkbox: true
                }, {
                    field: 'carBrand',
                    title: '车辆品牌型号',
                    formatter:function (value, row, index) {
                        return row.carBrand+row.carBrandType;
                    }
                },{
                    field: 'carCode',
                    title: '车牌号码',
                }, {
                    field: 'buyDate',
                    title: '购买时间',
                    formatter:function (value, row, index) {
                        if(isNaN(value)){
                            return "--";
                        } else {
                            var time = new Date(value);
                            return time.toLocaleDateString().replace("/","-").replace("/","-");
                        }
                    }

                }, {
                    field:'pic',
                    title:'车辆图片',
                    formatter: function (value, row, index) {
                        var img =  '<img style="width: 150px;height: 80px"  src="/download?FileName='+value+'"/>';
                        return img;
                    }
                },{
                    field: 'carPrice',
                    title: '日租金',
                },{
                    field: 'deptName',
                    title: '租赁门店',
                    formatter: function (value, row, index) {
                        var rtVal = "";
                        switch (value) {
                            case 1:
                                rtVal = "高新路店";
                                break;
                            case 2:
                                rtVal = "钟楼店";
                                break;
                        }
                        return rtVal;
                    }
                },{
                    field: 'carCity',
                    title: '所属区域城市',
                },{
                    field: 'carState',
                    title: '状态',
                    formatter: function (value, row, index) {
                        var rtVal = "";
                        switch (value) {
                            case 0:
                                rtVal = "待审核";
                                break;
                            case 1:
                                rtVal = "待租赁";
                                break;
                            case 2:
                                rtVal = "使用中";
                                break;
                            case 3:
                                rtVal = "待结算";
                                break;
                        }
                        return rtVal;
                    }
                }]

            });


        })
    })
</script>
</body>
</html>
