package com.yunhe.commons.dto;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/10/24 0024 16:07
 * @Description: 权限树形实体
 */
public class TreeNode {
    private Long id;
    private Long pId; //父级id
    private String name;  //权限名称
    private Boolean open=Boolean.FALSE;  //是否展开
    private  Boolean checked=Boolean.FALSE; //是否选中

    public TreeNode() {}

    public TreeNode(Long id, Long pId, String name) {
        this.id = id;
        this.pId = pId;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getpId() {
        return pId;
    }

    public void setpId(Long pId) {
        this.pId = pId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }
}
