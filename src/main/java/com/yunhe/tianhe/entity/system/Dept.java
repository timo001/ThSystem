package com.yunhe.tianhe.entity.system;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/23 0023 11:06
 * @Description: 门店实体
 */
public class Dept {
    private long deptid; //部门编号
    private String deptname; //部门名称
    private String deptphone; //部门固话
    private String ownername; //负责人姓名
    private String ownermobile; //负责人手机
    private String incity; //所属城市
    private String address; //详细地址
    private Double gpslon;//位置经度
    private Double gpslat;//位置纬度
    private String remark;//备注描述
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addtime;//添加时间

    public long getDeptid() {
        return deptid;
    }

    public void setDeptid(long deptid) {
        this.deptid = deptid;
    }

    public String getDeptname() {
        return deptname;
    }

    public void setDeptname(String deptname) {
        this.deptname = deptname;
    }

    public String getDeptphone() {
        return deptphone;
    }

    public void setDeptphone(String deptphone) {
        this.deptphone = deptphone;
    }

    public String getOwnername() {
        return ownername;
    }

    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

    public String getOwnermobile() {
        return ownermobile;
    }

    public void setOwnermobile(String ownermobile) {
        this.ownermobile = ownermobile;
    }

    public String getIncity() {
        return incity;
    }

    public void setIncity(String incity) {
        this.incity = incity;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getGpslon() {
        return gpslon;
    }

    public void setGpslon(Double gpslon) {
        this.gpslon = gpslon;
    }

    public Double getGpslat() {
        return gpslat;
    }

    public void setGpslat(Double gpslat) {
        this.gpslat = gpslat;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}
