<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/30 0030
  Time: 上午 11:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>预定租车</title>
    <meta name="viewport" content="width=device-width" />
    <link href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/static/js/plugins/bootstrap-table/bootstrap-table.css" rel="stylesheet" />
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="layui-box box-padding-15">
    <span class="layui-breadcrumb">
                <a href="/" target="_parent">首页</a>
                <a href="/book/listData">我要租车</a>
                <a><cite>列表</cite></a>
    </span>
    <hr>
    <div class="layui-row layui-col-space30">
        <div class="layui-col-md3">
        </div>
        <div class="layui-col-md4">
            <input type="text" id="searchCode"  placeholder="请输入要查询的车牌号" autocomplete="off"  class="layui-input">
        </div>
        <div class="layui-col-md2">
            <button class="layui-btn" id="queryBtn">查询</button>
        </div>
        <div class="layui-col-md3">

        </div>
    </div>

    <div class=" box-table-h7">
        <table id="tb_page"></table>
    </div>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>

<script>
    layui.use(['element','layer'], function(){
        var layer = layui.layer;
        /*var btnEvent = layui.btnEvent;*/
        //定义表格
        var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
        $('#tb_page').bootstrapTable({
            url: '/book/listData',         //请求后台的URL（*）
            method: 'get',                      //请求方式（*）
            toolbar: '#toolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: true,                   //是否显示分页（*）
            sortable: false,                     //是否启用排序
            sortOrder: "asc",                   //排序方式
            sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber:1,                       //初始化加载第一页，默认第一页
            pageSize: 10,                       //每页的记录行数（*）
            pageList: [15, 30, 50],        //可供选择的每页的行数（*）
            search: false,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            strictSearch: true,
            showColumns: false,                  //是否显示所有的列
            showRefresh: false,                  //是否显示刷新按钮
            minimumCountColumns: 2,             //最少允许的列数
            clickToSelect: true,                //是否启用点击选中行
            height: '700',                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: "orderId",                     //每一行的唯一标识，一般为主键列
            showToggle:false,                    //是否显示详细视图和列表视图的切换按钮
            cardView: false,                    //是否显示详细视图
            detailView: false,                   //是否显示父子表
            columns: [{
                checkbox: true
            }, {
                field: 'orderId',
                title: '订单号',
            }, {
                field: 'carCode',
                title: '车牌号码',
            },{
                field: 'client.name',
                title: '客户姓名',
            },{
                field: 'client.mobile',
                title: '客户电话',
            },{
                field: 'realBtTime',
                title: '租车开始时间',
                formatter:function (value, row, index) {
                    if(isNaN(value)){
                        return "--";
                    } else {
                        var time = new Date(value);
                        return time.toLocaleDateString().replace("/","-").replace("/","-");
                    }
                }

            }, {
                field: 'realEtTime',
                title: '租车截止时间',
                formatter:function (value, row, index) {
                    if(isNaN(value)){
                        return "--";
                    } else {
                        var time = new Date(value);
                        return time.toLocaleDateString().replace("/","-").replace("/","-");
                    }
                }

            },{
                field: 'realEtTime',
                title: '租车截止时间',
                formatter:function (value, row, index) {
                    if(isNaN(value)){
                        return "--";
                    } else {
                        var time = new Date(value);
                        return time.toLocaleDateString().replace("/","-").replace("/","-");
                    }
                }

            },  ]

        });

        $("#queryBtn").on('click',function () {
            var searchCode = $("#searchCode").val();
            $('#tb_page').bootstrapTable("refresh",{
                url: '/book/listData',
                query:{
                    searchCode:searchCode
                }
            });
            $("#searchCode").val("");
        })
    })
</script>
</body>
</html>
