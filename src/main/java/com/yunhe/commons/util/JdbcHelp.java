package com.yunhe.commons.util;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;


public class JdbcHelp {
    //通过标识名来创建相应连接池
    private static DataSource dataSource;
    static {
//       使用c3p0数据源
        /*dataSource = new ComboPooledDataSource ( "mysql" );//创建c3p0数据连接池*/

//        使用Druid数据源
        Properties druidConfig = Propertiesinit.read ( "druid.properties" );
        try {
            dataSource = DruidDataSourceFactory.createDataSource ( druidConfig );
        } catch (Exception e) {
            e.printStackTrace ();
        }
        /*JIDN*/
        //此类是执行命名操作的初始上下文。解析该URL
       /* try {
            InitialContext ctx = new InitialContext ();  //创建上下文对象
             dataSource = (DataSource)ctx.lookup ( "java:comp/env/jdbc/mysql" ); //通过上下文对象来获取对应的数据源
        } catch (NamingException e) {
            e.printStackTrace ();
        }*/

    }


    /**
     * 获取连接对象
      * @return
     */
    public static Connection getConn(){
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace ();
            return null;
        }
    }

    public static DataSource getDataSource() {
        return dataSource;
    }









/*//增删改
    public static void dell(PreparedStatement statement){
        try {
             statement.executeUpdate ();
        } catch (SQLException e) {
            e.printStackTrace ();
        }
    }
//            查找
    public static ResultSet chatAll(PreparedStatement statement){
        try {
           return statement.executeQuery ();
        } catch (SQLException e) {
            e.printStackTrace ();
            return null;
        }
    }
            //            拼装参数
        public static PreparedStatement stim(Connection conn,String sql,Object ...params){
                PreparedStatement statement;
            try {
                statement = conn.prepareStatement ( sql );
                if (params!=null){
                    for (int i=0;i<params.length;i++){
                        statement.setObject ( i+1,params[i] );
                    }
                }
                return statement;
            } catch (Exception e) {
                e.printStackTrace ();
                return null;
            }
        }
//        关闭资源
    public static void close(ResultSet resultSet,PreparedStatement statement,Connection conn){
            if (resultSet!=null){
                try {
                    resultSet.close ();
                } catch (Exception e) {
                    e.printStackTrace ();
                }
            }
            if (statement!=null){
                try {
                    statement.close ();
                } catch (SQLException e) {
                    e.printStackTrace ();
                }
            }
            if (conn!=null){
                try {
                    conn.close ();
                } catch (SQLException e) {
                    e.printStackTrace ();
                }
            }
    }*/
}