package com.yunhe.tianhe.controller.system;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.system.Busilog;
import com.yunhe.tianhe.entity.system.Sysdict;
import com.yunhe.tianhe.service.system.BusilogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 11:03
 * @Description: 系统日志控制也
 */
@Controller
@RequestMapping("/system/busi")
public class busilogController {
    @Autowired
    private BusilogService busilogService;

    @RequestMapping("/list")
    public String list(){
        return "system/busilog/list";
    }

    @RequestMapping("/save")
    public void save(Busilog busilog){
        busilogService.save ( busilog );
    }

    @RequestMapping("/page")
    @ResponseBody
    public Pagedao<Busilog> findpage(Busilog busilog, int offset, int limit){
        Pagedao<Busilog> findpage = busilogService.findpage ( busilog,offset, limit );
        return findpage;
    }

    @RequestMapping("/work")
    public ModelAndView work(Long logid){
        Busilog busilog = busilogService.find ( logid );
        return new ModelAndView ( "system/busilog/edit","busilog",busilog );
    }
}
