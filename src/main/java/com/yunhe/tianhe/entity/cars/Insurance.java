package com.yunhe.tianhe.entity.cars;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/23 0023 上午 10:06
 * @Description 车辆保险
 */
public class Insurance {
    private Long recordId;  //记录编号
    private String carCode;   //车
    private String company; //保险公司
    private String companyPhone; //出险电话
    private String code;   //保单编号
    private String content; //保单内容
    private Double money;   //保单费用
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date validdate;  //生效时间
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date invaliddate; //失效时间
    private String contactman;   //经办人员
    private String contactmobile; //经办手机
    private String remark;       //备注信息
    private Date addTime;         //添加时间

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getCarCode() {
        return carCode;
    }

    public void setCarCode(String carCode) {
        this.carCode = carCode;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyPhone() {
        return companyPhone;
    }

    public void setCompanyPhone(String companyPhone) {
        this.companyPhone = companyPhone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Date getValiddate() {
        return validdate;
    }

    public void setValiddate(Date validdate) {
        this.validdate = validdate;
    }

    public Date getInvaliddate() {
        return invaliddate;
    }

    public void setInvaliddate(Date invaliddate) {
        this.invaliddate = invaliddate;
    }

    public String getContactman() {
        return contactman;
    }

    public void setContactman(String contactman) {
        this.contactman = contactman;
    }

    public String getContactmobile() {
        return contactmobile;
    }

    public void setContactmobile(String contactmobile) {
        this.contactmobile = contactmobile;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
