package com.yunhe.tianhe.service.cars.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.util.StringUtils;
import com.yunhe.tianhe.dao.cars.YearcheckDao;
import com.yunhe.tianhe.entity.cars.Yearcheck;
import com.yunhe.tianhe.service.cars.YearcheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/27 0027 下午 5:12
 * @Description TODO
 */
@Service
public class YearcheckServiceImpl implements YearcheckService {
    @Autowired
    private YearcheckDao yearcheckDao;
    @Override
    public Pagedao <Yearcheck> list(Integer limit, Integer offset, String carcode) {
        Pagedao <Yearcheck> pagedao = new Pagedao <>();
        List<Yearcheck> yearchecks = null;
        int count = yearcheckDao.pagedao(carcode);
        if (count > 0){
            pagedao.setPage(offset);
            pagedao.setSize(limit);
            yearchecks = yearcheckDao.queryPage(offset, limit,carcode);
        }
        pagedao.setTotal(count);
        pagedao.setRows(yearchecks);
        return pagedao;
    }

    @Override
    public void save(Yearcheck yearcheck) {
        yearcheckDao.save(yearcheck);
    }

    @Override
    public void delete(String ids) {
        if (StringUtils.notEmpty(ids)){
            String[] split = ids.split(",");
            for (String p : split){
                yearcheckDao.delete(Integer.valueOf(p));
            }
        }
    }

    @Override
    public Yearcheck edit(Long recordid) {
        Yearcheck edit = yearcheckDao.edit(recordid);
        return edit;
    }

    @Override
    public void update(Yearcheck yearcheck) {
        yearcheckDao.update(yearcheck);
    }
}
