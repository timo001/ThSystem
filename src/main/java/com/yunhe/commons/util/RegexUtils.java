package com.yunhe.commons.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则工具类
 *
 * @author tianxuan
 * @Time 2016/4/26
 */
public class RegexUtils {

    /**
     * 是否是手机号
     * @param str
     * @return
     */
    public static boolean isMobile(String str) {
        Pattern p = Pattern.compile("^1[0-9]{10}$");
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 是否是邮箱
     * @param str
     * @return
     */
    public static boolean isMail(String str) {
        Pattern p = Pattern.compile("^([a-z0-9A-Z]+[-|\\\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\\\.)+[a-zA-Z]{2,}$");
        Matcher m = p.matcher(str);
        return m.matches();
    }
}
