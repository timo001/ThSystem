package com.yunhe.tianhe.dao.system;

import com.yunhe.tianhe.entity.system.Dept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/23 0023 11:19
 * @Description: 店铺操作
 */
public interface DeptDao {

    /**
     * 保存
     * @param dept
     */
    void save(Dept dept);

    /**
     * 删除
     * @param deptid
     */
    void deleted(Long deptid);

    /**
     * 编辑
     * @param dept
     */
    void update(Dept dept);

    /**
     * 根据id查
     * @param deptid
     * @return
     */
    Dept find(Long deptid);

    /**
     * 查找所有
     * @return
     */
    List<Dept> findlist();

    /**
     * 分页总数
     * @return
     */
    int count();

    /**
     * 分页查找
     * @return
     */
    List<Dept> findpage(@Param ( "offset" ) int offset,@Param ( "limit" ) int limit);


}
