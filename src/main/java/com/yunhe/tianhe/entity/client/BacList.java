package com.yunhe.tianhe.entity.client;





import java.util.Date;

/* 黑名单*/
/*注：黑名单可以通过系统手动添加，也可以自动通过其他第三方接口定期同步更新获取黑名单数据到本数据表。*/

public class BacList  {
    private Long bId;//id
    private String name;//黑户姓名
    private String mobile;//黑户手机
    private String idcard;//身份证号
    private Integer reasonType;//拉黑原因：1.偷盗惯犯;2.租车老赖;3.酒后驾驶
    private String remark;//备注信息
    private Integer sourceType;//信息来源：1.人工识别;2.公安局接口;3.政信通;4.芝麻信用
    private Date addTime;//系统时间

    public Long getBid() {
        return bId;
    }

    public void setBid(Long bid) {
        this.bId = bid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public Integer getReasonType() {
        return reasonType;
    }

    public void setReasonType(Integer reasonType) {
        this.reasonType = reasonType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getSourceType() {
        return sourceType;
    }

    public void setSourceType(Integer sourceType) {
        this.sourceType = sourceType;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
