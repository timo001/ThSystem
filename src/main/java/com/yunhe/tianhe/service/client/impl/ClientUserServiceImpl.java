package com.yunhe.tianhe.service.client.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.util.StringUtils;
import com.yunhe.tianhe.dao.client.ClientUserDao;
import com.yunhe.tianhe.entity.client.ClientCompany;
import com.yunhe.tianhe.entity.client.ClientUser;
import com.yunhe.tianhe.service.client.ClientCompanyService;
import com.yunhe.tianhe.service.client.ClientUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/22 0022 下午 8:03
 * @Description: TODO
 */
@Service
public class ClientUserServiceImpl implements ClientUserService {

    @Autowired
    private ClientUserDao clientUserDao;
    @Autowired
    private ClientCompanyService clientCompanyService;


    @Override
    public ClientUser findByMobile(String mobile) {
        clientUserDao.findByMobile(mobile);
        return null;
    }

    @Override
    public void save(ClientUser clientUser) {
        clientUserDao.save(clientUser);
    }

    @Override
    public void add(ClientUser clientUser, ClientCompany clientCompany) {
        clientCompanyService.save(clientCompany);
        if (clientCompany != null){
            Long aLong = clientCompany.getcId();
            clientUser.setCompanyId(aLong);
            clientUserDao.save(clientUser);
        }
    }

    @Override
    public ClientUser findById(Long id) {
        return clientUserDao.findById(id);
    }

    @Override
    public Pagedao<ClientUser> findPage(Integer offset, Integer limit,Integer memLevel,Date endTime) {
        Pagedao<ClientUser> pagedao = new Pagedao<>();
        List<ClientUser> data = new ArrayList<ClientUser>();
        Integer count = clientUserDao.findCount(memLevel,endTime);
        data = clientUserDao.findList(offset,limit,memLevel,endTime);
        for(ClientUser cu : data){
            // TODO: 2017/11/25 0025 在车辆完成订单列表中查询客户用车次数，加入clientUser
        }

        pagedao.setTotal(count );
        pagedao.setRows(data);
        return pagedao;
    }


    @Override
    public void delete(Long id) {
        clientUserDao.delete(id);
    }

    @Override
    public void deleteBatch(String ids) {
        if(StringUtils.notEmpty(ids)){
            String[] idArr = ids.split(",");
            for(String id : idArr) {
                this.delete(Long.valueOf(id));
            }
        }
    }

    @Override
    public void update(ClientUser clientUser) {
        clientUserDao.update(clientUser);
    }
}
