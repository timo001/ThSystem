package com.yunhe.tianhe.service.system.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.dao.system.MemberlevelDao;
import com.yunhe.tianhe.entity.system.Memberlevel;
import com.yunhe.tianhe.service.system.MemberlevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 13:57
 * @Description: 实现MemberlevelService接口
 */
@Service
public class MemberlevelServiceImpl implements MemberlevelService{
    @Autowired
    private MemberlevelDao memberlevelDao;
    @Override
    public void save(Memberlevel memberlevel) {
        memberlevelDao.save ( memberlevel );
    }

    @Override
    public void deleted(String ids) {
        String[] split = ids.split ( "," );
        for (String levelid :split){
            memberlevelDao.deleted ( new Long(levelid) );
        }
    }

    @Override
    public void update(Memberlevel memberlevel) {
        memberlevelDao.update ( memberlevel );
    }

    @Override
    public Memberlevel find(Long levelid) {
        return memberlevelDao.find ( levelid );
    }

    @Override
    public List<Memberlevel> findlist() {
        return memberlevelDao.findlist ();
    }

    @Override
    public Pagedao<Memberlevel> findpage(int offset, int limit) {
        Pagedao<Memberlevel> pagedao = new Pagedao<> ();
        Integer count = memberlevelDao.count ();
        List<Memberlevel> findpage=null;
        if (count>0){
             findpage = memberlevelDao.findpage ( offset, limit );
             pagedao.setPage ( offset/limit+1 );
             pagedao.setSize ( limit );
        }
        pagedao.setTotal ( count );
        pagedao.setRows ( findpage );
        return pagedao;
    }
}
