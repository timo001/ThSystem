package com.yunhe.commons.dto;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/10/23 0023 13:45
 * @Description: 返回json数据时的对象
 */
public class JSONResponse {
    public static final int CODE_FAIL = 0;    //失败（未知原因）
    public static final int CODE_SUCCESS = 1; //成功
    public static final int CODE_404 = 404;   //资源未找到
    public static final int CODE_500 = 500;   //服务器错误



    private int code;  //是否成功
    private String message;  //返回消息
    private Object data;
    private Object data1;


    public JSONResponse(){}


    public JSONResponse(int code, String message) {
        this(code,message,null);
    }

    public JSONResponse(int code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;


    }
    public JSONResponse(Object data,Object data1){
        this.data = data;
        this.data1=data1;
    }


    public static JSONResponse success(String msg){
        return success(msg,null);
    }

    public static JSONResponse success(String msg, Object data){
        return new JSONResponse(CODE_SUCCESS,msg,data);
    }

    public static JSONResponse fail(String msg){
        return fail(msg,null);
    }

    public static JSONResponse fail(String msg, Object data){
        return new JSONResponse(CODE_FAIL,msg,data);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Object getData1() {
        return data1;
    }

    public void setData1(Object data1) {
        this.data1 = data1;
    }
}
