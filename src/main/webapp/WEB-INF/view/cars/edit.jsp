<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/24 0024
  Time: 上午 10:55
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>车辆基本信息修改</title>
    <meta name="viewport" content="width=device-width" />
    <link href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/static/js/plugins/bootstrap-table/bootstrap-table.css" rel="stylesheet" />
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="box-padding-15">
    <div class="layui-container box-form">
        <form class="layui-form " action="/cars/update" method="post">
            <input type="hidden" name="carId" value="${car.carId}">
            <div class="layui-form-item">
                <label class="layui-form-label ">车牌号码</label>
                <div class="layui-input-block">
                    <input type="text" name="carCode" value="${car.carCode}" required  lay-verify="required" placeholder="请输入车牌号" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">车辆颜色</label>
                <div class="layui-input-block">
                    <input type="text" name="carColor" value="${car.carColor}" required  lay-verify="required" placeholder="请输入颜色" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">车辆类型</label>
                <div class="layui-input-block">
                    <select name="carType" value="${car.carType}">
                        <option>请选择车辆类型</option>
                        <option value="1" <c:if test="${car.carType eq 1}">selected</c:if>>私家车</option>
                        <option value="2" <c:if test="${car.carType eq 2}">selected</c:if>>商务车</option>
                        <option value="3" <c:if test="${car.carType eq 3}">selected</c:if>>跑车</option>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">车辆品牌</label>
                <div class="layui-input-block">
                    <input type="text" name="carBrand"  value="${car.carBrand}" lay-verify="required" placeholder="请输入车辆品牌" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">车辆型号</label>
                <div class="layui-input-block">
                    <input type="text" name="carBrandType"  value="${car.carBrandType}"  lay-verify="required" placeholder="请输入车辆型号" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-upload" id="div">
                <label class="layui-form-label">车辆图片</label>
                <button type="button" class="layui-btn" id="test1">点击修改</button>
                <div class="layui-upload-list">
                    <img class="layui-upload-img" id="demo1" style="width: 300px" src="/download?FileName=${car.pic}">
                    <input type="hidden" name="pic" id="pic" value="${car.pic}" >
                    <p id="demoText"></p>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">购买日期</label>
                <div class="layui-input-block">
                    <input type="text" name="buyDate"  value="<fmt:formatDate value="${car.buyDate}" pattern="yyyy-MM-dd"></fmt:formatDate>" id="buyTime" lay-verify="required" placeholder="请选择购买日期" autocomplete="off" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">发动机号</label>
                <div class="layui-input-block">
                    <input type="text" name="engineNum" value="${car.engineNum}" lay-verify="required" placeholder="请输入发动机号" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">车架号码</label>
                <div class="layui-input-block">
                    <input type="text" name="frameNum" value="${car.frameNum}"  lay-verify="required" placeholder="请输入车架号码 " autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">是否自动</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="isAuto" value="1" title="自动" <c:if test="${car.isAuto eq 1} ">checked</c:if> >
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">排量大小</label>
                <div class="layui-input-block">
                    <input type="text" name="engineSize" value="${car.engineSize}"  lay-verify="required" placeholder="请输入排量大小" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">座位数量</label>
                <div class="layui-input-block">
                    <select name="seatNum" value="${car.seatNum}" >
                        <option >请选择座位数量</option>
                        <option value="4" <c:if test="${car.seatNum eq 4}">selected</c:if>>四座</option>
                        <option value="6" <c:if test="${car.seatNum eq 6}">selected</c:if>>六座</option>
                        <option value="8" <c:if test="${car.seatNum eq 8}">selected</c:if>>八座</option>
                        <option value="0" <c:if test="${car.seatNum eq 0}">selected</c:if>>其它</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">外观类型</label>
                <div class="layui-input-block">
                    <input type="text" name="faceType" value="${car.faceType}"  lay-verify="required" placeholder="请输入外观类型" autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label ">填写更多</label>
                <div class="layui-input-block">
                    <input type="checkbox" name="like" lay-filter="check"  title="更多" id="checkbox">基本信息填写完毕，继续填写更多
                </div>
            </div>
            <div class="layui-hide" id="hide">
                <div class="layui-form-item">
                    <label class="layui-form-label">车辆里程</label>
                    <div class="layui-input-block">
                        <input type="text" name="totalMile" value="${car.totalMile}"  lay-verify="required" placeholder="请输入车辆里程" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">保养时间</label>
                    <div class="layui-input-block">
                        <input type="text" name="maintainTime" value="<fmt:formatDate value="${car.maintainTime}" pattern="yyyy-MM-dd" />"  id="maintainTime" lay-verify="required" placeholder="请选择保养时间" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">保养里程</label>
                    <div class="layui-input-block">
                        <input type="text" name="maintainMile" value="${car.maintainMile}"  lay-verify="required" placeholder="请输入保养里程" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">年检时间</label>
                    <div class="layui-input-block">
                        <input type="text" name="yearCheck" value="<fmt:formatDate value="${car.yearCheck}" pattern="yyyy-MM-dd" />"  id="yearCheckTime"  lay-verify="required" placeholder="请选择年检时间" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">保险时间</label>
                    <div class="layui-input-block">
                        <input type="text" name="insuranceTime" value="<fmt:formatDate value="${car.insuranceTime}" pattern="yyyy-MM-dd" />"  id="insuranceTime"  lay-verify="required" placeholder="请选择保险时间" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">车辆负责人手机号</label>
                    <div class="layui-input-block">
                        <input type="text" name="phone" value="${car.phone}"  lay-verify="required|phone" placeholder="请输入车辆负责人手机号" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">车辆日租金</label>
                    <div class="layui-input-block">
                        <input type="text" name="carPrice" value="${car.carPrice}" lay-verify="required" placeholder="请输入车辆日租金" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">车辆所在区域城市</label>
                    <div class="layui-input-block">
                        <input type="text" name="carCity" value="${car.carCity}"  lay-verify="required" placeholder="请输入车辆所在区域城市" autocomplete="off" class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">车辆所属租赁分店</label>
                    <div class="layui-input-block">
                        <select name="deptName" value="${car.deptName}">
                            <option >请选择车辆所属租赁分店</option>
                            <option value="1" <c:if test="${car.deptName eq 1}">selected</c:if>>高新路店</option>
                            <option value="2" <c:if test="${car.deptName eq 2}">selected</c:if>>钟楼店</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">车辆状态</label>
                    <div class="layui-input-block">
                        <select name="carState" value="${car.carState}">
                            <option >请选择车辆状态</option>
                            <option value="0" <c:if test="${car.carState eq 0}">selected</c:if>>待审核</option>
                            <option value="1" <c:if test="${car.carState eq 1}">selected</c:if>>待租赁</option>
                            <option value="2" <c:if test="${car.carState eq 2}">selected</c:if>>使用中</option>
                            <option value="3" <c:if test="${car.carState eq 3}">selected</c:if>>待结算</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">车辆备注信息</label>
                    <div class="layui-input-block">
                        <input type="text" name="carRemark" value="${car.carRemark}"  lay-verify="required" placeholder="请输入车辆备注信息" autocomplete="off" class="layui-input">
                    </div>
                </div>
            </div>

            <div class="layui-form-item">
                <button class="layui-btn" lay-submit lay-filter="formSubmit">保存</button>
                <a href="/cars"><button type="button" class="layui-btn layui-btn-primary">取消</button></a>
            </div>
        </form>
    </div>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script>

    layui.use(['element','layer','form','laydate','upload'], function() {
        var form = layui.form,
            laydate = layui.laydate
            , upload = layui.upload
            , element = layui.element,
            layer = layui.layer;

        form.on("checkbox(check)",function(){
            $("#hide").toggleClass("layui-hide")
        })

        //日期时间范围
        laydate.render({
            elem: '#buyTime',
        });

        laydate.render({
            elem: '#maintainTime',
        });

        laydate.render({
            elem: '#yearCheckTime',
        });

        laydate.render({
            elem: '#insuranceTime',
        });



        var uploadInst = upload.render({
            elem: '#test1'
            ,url: '/upload'
            ,before: function(obj){
                //预读本地文件示例，不支持ie8
                obj.preview(function(index, file, result){
                    $('#demo1').attr('src', result); //图片链接（base64）
                });
            }
            ,done: function(res){
                $("#pic").val(res.data)    //将返回回来的时间戳组成的名字存入隐藏域中
                //如果上传失败
                if(res.code != 1){
                    return layer.msg('上传失败');
                }
                //上传成功
            }
            ,error: function(){
                //演示失败状态，并实现重传
                var demoText = $('#demoText');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function(){
                    uploadInst.upload();
                });
            }
        });

    });
</script>
</body>
</html>
