package com.yunhe.tianhe.dao.rent;

import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/12/1 0001 上午 10:35
 * @Description: 我要租车
 */
public interface BookDao {
    Integer findCount(@Param("searchCode")String searchCode);       //统计，（分页）

    List findAll(@Param("offset")Integer offset, @Param("limit") Integer limit, @Param("searchCode")String searchCode);
}
