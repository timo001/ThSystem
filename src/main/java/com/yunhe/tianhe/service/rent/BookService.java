package com.yunhe.tianhe.service.rent;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.RentOrder;
import org.apache.ibatis.annotations.Param;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/12/1 0001 上午 11:13
 * @Description: TODO
 */
public interface BookService {
    /*分页*/
    Pagedao<RentOrder> findPage(@Param("offset") Integer offset, @Param("limit") Integer limit, @Param("searchCode")String searchCode);
}
