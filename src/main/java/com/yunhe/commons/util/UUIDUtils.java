package com.yunhe.commons.util;

import java.util.UUID;

/**
 * Created by Tianxuan
 * Date :2016/6/26 17:46
 * description:
 */
public class UUIDUtils {

    // 生成小写UUID
    public static String getLowerUUID(){
        return generateUUID();
    }

    // 生成大写UUID
    public static String getUpperUUID(){
        return generateUUID().toUpperCase();
    }

    // 生成UUID,且去除"-"符号
    private static String generateUUID() {
        String uuid = UUID.randomUUID().toString();
        return uuid.replace("-","");
    }

}
