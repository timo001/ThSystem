package com.yunhe.tianhe.service.cars.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.util.StringUtils;
import com.yunhe.tianhe.dao.cars.InsuranceDao;
import com.yunhe.tianhe.entity.cars.Car;
import com.yunhe.tianhe.entity.cars.Insurance;
import com.yunhe.tianhe.service.cars.InsuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/23 0023 上午 9:39
 * @Description TODO
 */
@Service
public class InsuranceServiceImpl implements InsuranceService {
    @Autowired
    private InsuranceDao insuranceDao;


    @Override
    public Pagedao findlist(int limit, int offset) {
        Pagedao <Insurance> pagedao = new Pagedao <>();
        List<Insurance> insurances = null;
        int count = insuranceDao.pagedao(null);
        if (count > 0){
            pagedao.setPage(offset);
            pagedao.setSize(limit);
            insurances = insuranceDao.queryPage(offset, limit,null);
        }
        pagedao.setTotal(count);
        pagedao.setRows(insurances);
        return pagedao;
    }

    @Override
    public void save(Insurance insurance) {
        insuranceDao.save(insurance);
    }

    @Override
    public void delete(String ids) {
        if (StringUtils.notEmpty(ids)){
            String[] split = ids.split(",");
            for (String p:split){
                insuranceDao.delete(Integer.valueOf(p));
            }
        }
    }

    @Override
    public Insurance toupdate(Long recordid) {
        Insurance toupdate = insuranceDao.toupdate(recordid);
        return toupdate;
    }

    @Override
    public void update(Insurance insurance) {
        insuranceDao.update(insurance);
    }

    @Override
    public Pagedao <Insurance> findcarcode(String carcode,int limit,int offset) {
        Pagedao <Insurance> pagedao = new Pagedao <>();
        List<Insurance> insurances = null;
        int count = insuranceDao.pagedao(carcode);
        if (count > 0){
            pagedao.setPage(offset);
            pagedao.setSize(limit);
            insurances = insuranceDao.queryPage(offset, limit,carcode);
        }
        pagedao.setTotal(count);
        pagedao.setRows(insurances);
        return pagedao;
    }

}
