package com.yunhe.tianhe.service.client;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.client.ClientCompany;
import com.yunhe.tianhe.entity.client.Credit;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/28 0023
 * @Description: TODO
 */
public interface ClientCompanyService {

    /**
     * 增加客户企业信息记录
     */
    Long save(ClientCompany clientCompany);

    /**
     * 查询一个企业
     * @param cid
     * @return
     */
    ClientCompany findById(Long cid);



}
