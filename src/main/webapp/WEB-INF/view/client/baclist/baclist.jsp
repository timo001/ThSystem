<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/23 0023
  Time: 下午 3:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>列表</title>
    <meta name="viewport" content="width=device-width" />
    <link href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/static/js/plugins/bootstrap-table/bootstrap-table.css" rel="stylesheet" />
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">

</head>
<body>
<div class="layui-box box-padding-15">
        <span class="layui-breadcrumb">
          <a href="#">首页</a>
          <a href="javascript:;">客户管理</a>
          <a><cite>黑名单</cite></a>
        </span>

    <div class="layui-input-block">
        <div class="layui-inline">
            <label class="layui-form-label">客户姓名</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" id="name" placeholder="请输入姓名">
            </div>
        </div>

        <div class="layui-inline">
            <label class="layui-form-label">手机号码：</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" id="mobile" placeholder="请输入手机号码">
            </div>
        </div>

        <div class="layui-inline">
            <label class="layui-form-label">身份证号码：</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" id="idcard" placeholder="请输入身份证号码">
            </div>
        </div>


        <button id="selBtn" data-url="/clientuser/select" class="layui-btn layui-btn-primary btn-sel" >查询</button>
    </div>

    <div id="toolbar" class="layui-btn-group">
        <%--    <c:forEach items="${buttons}" var="btn">
                <button id="${btn.btnId}" data-url="${btn.uri}" class="layui-btn layui-btn-primary btn-add">${btn.name}</button>
            </c:forEach>--%>

        <button id="addBtn" data-url="/baclist/add" class="layui-btn layui-btn-primary btn-edit" >添加</button>
        <button id="editBtn" data-url="/baclist/edit" class="layui-btn layui-btn-primary btn-edit" >编辑</button>
        <button id="delBtn" data-url="/baclist/delete" class="layui-btn layui-btn-primary btn-del">删除</button>
    </div>
    <%--<div class=" box-table-h7">--%>
    <table id="tb_page"></table>
    <%--</div>--%>
</div>

<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/common.js"></script>
<script>

    layui.use(['element','layer','btnEvent'], function(){
        var layer = layui.layer;
        var btnEvent = layui.btnEvent;

        var $table = $('#tb_page')
        //定义表格
        var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
        $('#tb_page').bootstrapTable({
            url: '/baclist/listData',         //请求后台的URL（*）
            method: 'get',                      //请求方式（*）
            toolbar: '#toolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: true,                   //是否显示分页（*）
            sortable: false,                     //是否启用排序
            sortOrder: "asc",                   //排序方式
            sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber:1,                       //初始化加载第一页，默认第一页
            pageSize: 15,                       //每页的记录行数（*）
            pageList: [15, 30, 50],        //可供选择的每页的行数（*）
            search: false,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            strictSearch: true,
            showColumns: false,                  //是否显示所有的列
            showRefresh: true,                  //是否显示刷新按钮
            minimumCountColumns: 2,             //最少允许的列数
            clickToSelect: true,                //是否启用点击选中行
            height: '700',                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: "bId",                     //每一行的唯一标识，一般为主键列
            showToggle:false,                    //是否显示详细视图和列表视图的切换按钮
            cardView: false,                    //是否显示详细视图
            detailView: false,                   //是否显示父子表
            columns: [{
                checkbox: true
            }, {
                field: 'name',
                title: '客户名称',
                width: '20%'
            },  {
                field: 'mobile',
                title: '手机',
                width: '20%'
            },{
                field: 'idcard',
                title: '身份证',
                width: '20%'
            },{
                field: 'reasonType',
                title: '原因类型',
                width: '20%'
            },{
                field: 'remark',
                title: '备注',
                width: '15%',
            }]

        });

        btnEvent.bound($('#tb_page'),"addBtn");   //绑定通用事件



        //查询数据事件
        $("#selBtn").click(function (){
            var name = $('#name').val();
            var idcard = $('#idcard').val();
            var mobile = $('#mobile').val();
            var opt = {
                url: "/baclist/listData",
                silent: true,
                query:{
                    name:name,
                    idcard:idcard,
                    mobile:mobile
                }
            };
            $("#tb_page").bootstrapTable('refresh', opt);
        });

        //删除事件
        var $delBtn = $("#delBtn");
        $delBtn.on('click',function(){
            var url = $(this).data("url");
            debugger
            var rows = $table.bootstrapTable('getSelections');  //获取选中行
            if(rows.length < 1) {
                layer.msg("请勾选一条数据");
            }else {
                layer.confirm("您确定要删除勾选的所有数据吗？",function(index){
                    //异步删除 所选记录
                    var bids = [];
                    $.each(rows,function(){
                        bids.push(this.bid);
                    });
                    console.log(bids)
                    $.ajax({
                        url: url,
                        type:"post",
                        data:{ids:bids.join(",")},
                        dataType:'json',
                        success:function(msg) {
                            layer.msg(msg.message);
                            if(msg.code == 1) {
                                $table.bootstrapTable("refresh");  //刷新表格数据
                            }
                        },
                        error:function(e){
                            layer.msg("服务器忙，请稍后再试");
                        }
                    });
                })
            }
        });

            //编辑事件
            var $editBtn = $("#editBtn" );
            $editBtn.on('click',function(){
                var url = $(this).data("url");
                var rows = $table.bootstrapTable('getSelections');  //获取选中行
                if(rows.length < 1) {
                    layer.msg("请勾选一条数据");
                }else if(rows > 1) {
                    layer.msg("只能勾选一条数据");
                }else {
                    var bid = rows[0].bid;
                    var url = $(this).data("url");
                    location.href = url + "?bid=" + bid;
                }
            });

    });
</script>
</body>
</html>
