package com.yunhe.tianhe.entity.client;



import java.util.Date;
import java.util.List;

/* 客户信息实体*/

public class ClientUser {
    private Long id;//id
    private String mobile;//客户手机
    private String clientPwd;//客户密码
    private String name;//客户姓名
    private String idcard;//身份证号码
    private String driverNum;//驾驶证号码
    private Long companyId;//企业代码
    private Integer creditNum;//信用分值
    private String memLevel;//会员级别
    private Integer clientState;//状态：1.正常;2.锁定
    private Date addTime;//系统时间


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getClientPwd() {
        return clientPwd;
    }

    public void setClientPwd(String clientPwd) {
        this.clientPwd = clientPwd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getDriverNum() {
        return driverNum;
    }

    public void setDriverNum(String driverNum) {
        this.driverNum = driverNum;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Integer getCreditNum() {
        return creditNum;
    }

    public void setCreditNum(Integer creditNum) {
        this.creditNum = creditNum;
    }

    public String getMemLevel() {
        return memLevel;
    }

    public void setMemLevel(String memLevel) {
        this.memLevel = memLevel;
    }

    public Integer getClientState() {
        return clientState;
    }

    public void setClientState(Integer clientState) {
        this.clientState = clientState;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    @Override
    public String toString() {
       return "Classes [id=" + id + ", name=" + name + ", mobile=" + mobile+ "]";
    }
}
