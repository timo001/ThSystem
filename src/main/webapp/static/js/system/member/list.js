/**
 * Created by Administrator on 2017/11/23 0023.
 */
//JavaScript代码区域
layui.use(['element','layer'], function(){
    var layer = layui.layer;
    var element = layui.element;


    $('#tb_departments').bootstrapTable({
        url: '/system/level/page',         //请求后台的URL（*）
        method: 'get',                      //请求方式（*）
        toolbar: '#toolbar',                //工具按钮用哪个容器
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: true,                     //是否启用排序
        sortOrder: "asc",                   //排序方式
        // queryParams: {type:1},             //传递参数（*）
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        pageNumber:1,                       //初始化加载第一页，默认第一页
        pageSize: 20,                       //每页的记录行数（*）
        pageList: [20, 25, 50, 100],        //可供选择的每页的行数（*）
        // search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
        // strictSearch: true,
        // showColumns: true,                  //是否显示所有的列
        showRefresh: true,                  //是否显示刷新按钮
        // minimumCountColumns: 2,             //最少允许的列数
        clickToSelect: true,                //是否启用点击选中行
        height: 1000,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        uniqueId: "levelid",                     //每一行的唯一标识，一般为主键列
        // showToggle:true,                    //是否显示详细视图和列表视图的切换按钮
        // cardView: false,                    //是否显示详细视图
        detailView: false,                   //是否显示父子表
        columns: [{
            checkbox: true
        },{
            field: 'levelid',
            title: '编号',
            width:'25%',
            align: 'center'
        },{
            field: 'minscore',
            title: '信用值积分',
            width:'25%',
            align: 'center',
            formatter:function(val, row){
                return  row.minscore+"~"+row.maxscore;
            }
        },{
            field: 'levelname',
            title: '会员级别',
            width:'25%',
            align: 'center'
        },{
            field: 'discount',
            title: '租车折扣',
            width:'25%',
            align: 'center'
        }]
    });

    //删除
    $("#btn_deleted").click(function () {
        //1.要想获取主子表所有选中行，首先得拿到所有的表格对象（给所有的table 起相同的标识）
        var $table = $("table[id = tb_departments]");//找出以tb_departments开头的对象
        var table = $table.bootstrapTable("getSelections");//找出被选中的对象，并返回一个数组
        if(table.length>0){
            layer.confirm("请确定是否删除？",function () {
                var ids=[];
                $(table).each(function () {
                    ids.push(this.levelid)//把id放到一个数组中
                })
                $.ajax({
                    url:"/system/level/deleted",
                    type:"post",
                    dataType:"json",
                    data:{ids:ids.join()},
                    success:function () {
                        layer.msg("删除成功!")
                        $("#tb_departments").bootstrapTable("refresh");  //刷新表格数据
                    },
                    error:function () {
                        layer.msg("服务器忙，请稍后再试");
                    }
                })
            })
        }else {
            layer.msg("请至少选择一条数据");
        }
    })

//    添加

    $("#btn_add").click(function () {
        location.href="/system/level/add"
    })

//    修改
    $("#btn_edit").click(function () {
        var $table = $("table[id = tb_departments]");//找出以tb_departments开头的对象
        var table = $table.bootstrapTable("getSelections");//找出被选中的对象，并返回一个数组
        if (table.length==1){
            var id = table[0].levelid;
            location.href="/system/level/edit?levelid="+id;
        }else {
            layer.msg("请选择一条数据");
        }
    })



});