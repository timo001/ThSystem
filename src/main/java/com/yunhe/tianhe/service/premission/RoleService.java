package com.yunhe.tianhe.service.premission;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.dto.TreeNode;
import com.yunhe.tianhe.entity.premission.Role;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/29 0029 17:32
 * @Description: ${todo}(用一句话描述该文件做什么)
 */
public interface RoleService {

    void save(Role role ,String ids);

    void update(Role role,String ids);

    void deleted(String ids);

    Role find(Long roleid);

    List<Role> findlist(Role role);

    Pagedao<Role> findpage(int offset, int limit);

    List<TreeNode> findtree(Long roleid);


}

