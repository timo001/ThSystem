package com.yunhe.tianhe.service.client;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.client.Credit;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/28 0023
 * @Description: TODO
 */
public interface CreditService  {

    /**
     * 增加信用记录
     */
    void save(Credit credit);

    /**
     * 信用记录列表
     * @return
     */
    Pagedao<Credit> findPage(Integer offset, Integer limit,String idcard, String mobile);

    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**
     * 批量删除
     * @param ids
     */
    void deleteBatch(String ids);


}
