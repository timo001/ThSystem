package com.yunhe.tianhe.dao.client;

import com.yunhe.tianhe.entity.client.BacList;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/23 0023 上午 11:02
 * @Description: TODO
 */
public interface BacListDao {


    /**
     * 加入黑名单
     */
    void save(BacList bacList);

    /**
     * 查询所有黑名单信息,service层整合成pageable数据
     * @return
     */
    List<BacList> findList(@Param("offset")Integer offset,@Param("limit")Integer limit,@Param("name")String name,@Param("idcard")String idcard,@Param("mobile")String mobile);

    Integer findCount(@Param("name")String name,@Param("idcard")String idcard,@Param("mobile")String mobile);

    /**
     * 删除客户
     * @param id
     */
    void delete( Long id);

    /**
     * 根据ID查询详情
     * @param bid
     * @return
     */
    BacList findById(Long bid);

    /**
     * 修改黑名单信息
     * @param bacList
     */
    void update(BacList bacList);

}
