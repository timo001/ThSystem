package com.yunhe.tianhe.controller.premission;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.dto.TreeNode;
import com.yunhe.tianhe.entity.premission.Role;
import com.yunhe.tianhe.service.premission.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/29 0029 17:39
 * @Description: 角色控制
 */
@Controller
@RequestMapping("/permi/role")
public class RoleController {
    @Autowired
    private RoleService roleService;

    @RequestMapping("/list")
    public String list(){
        return "permission/role/list";
    }

    @RequestMapping("/add")
    public String add(){
        return "permission/role/add";
    }

    @RequestMapping("/edit")
    public ModelAndView edit(Long roleid){
        Role role = roleService.find ( roleid );

        return new ModelAndView ( "permission/role/edit","role",role );
    }

    @RequestMapping("/deleted")
    @ResponseBody
    public JSONResponse save(String ids){
        roleService.deleted ( ids );
        return new JSONResponse ();
    }

    @RequestMapping("/save")
    @ResponseBody
    public JSONResponse save(Role role,String ids){
        roleService.save ( role,ids );
        return new JSONResponse (  );
    }

    @RequestMapping("/update")
    @ResponseBody
    public JSONResponse update(Role role,String ids){
        roleService.update ( role,ids );
        return new JSONResponse (  );
    }

    @RequestMapping("/page")
    @ResponseBody
    public Pagedao page(int offset,int limit){
        Pagedao<Role> findpage = roleService.findpage ( offset, limit );
        return findpage;
    }

    @RequestMapping("/tree")
    @ResponseBody
    public  List<TreeNode> tree(Long roleid){
        List<TreeNode> findtree = roleService.findtree (roleid);
        return findtree;
    }




}
