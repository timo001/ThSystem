package com.yunhe.tianhe.controller.premission;



import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.premission.Account;
import com.yunhe.tianhe.entity.premission.Role;
import com.yunhe.tianhe.entity.system.Dept;
import com.yunhe.tianhe.service.premission.AccountService;
import com.yunhe.tianhe.service.premission.RoleService;
import com.yunhe.tianhe.service.system.DeptServive;
import com.yunhe.tianhe.service.system.SysdictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/23 0023 09:23
 * @Description: 个人信息设置
 */
@Controller
@RequestMapping("/permi/account")
public class AccountController {
    @Autowired
    private AccountService accountService;
    @Autowired
    private DeptServive deptServive;
    @Autowired
    private RoleService roleService;

    @RequestMapping("/list")
    public String list(HttpServletRequest request){
        Role role = new Role ();
        role.setIsabled ( 1 );
        List<Dept> findlist = deptServive.findlist ();
        List<Role> findlist1 = roleService.findlist ( role );
        HttpSession session = request.getSession ();
        session.setAttribute ( "list",findlist );
        session.setAttribute ( "rolelist",findlist1 );
        return "permission/account/list";
    }
    @RequestMapping("/add")
    public String add(){
        return "permission/account/add";
    }

    @RequestMapping("/edit")
    public ModelAndView edit(Long uid){
        Account account =  accountService.find ( uid );
        return new ModelAndView ("permission/account/edit","account",account  );
    }

    @RequestMapping("/save")
    @ResponseBody
    public JSONResponse save(Account account){
        accountService.save ( account );
        return new JSONResponse ( 1,"保存成功！" );
    }

    @RequestMapping("/update")
    @ResponseBody
    public JSONResponse update(Account account){
        accountService.update ( account );
        return new JSONResponse ( 1,"修改成功！" );
    }

    @RequestMapping("/deleted")
    @ResponseBody
    public JSONResponse deleted(String ids){
        accountService.deleted ( ids );
        return new JSONResponse ( 1,"删除成功！" );
    }
    @RequestMapping("/page")
    @ResponseBody
    public Pagedao page(Account account,int offset,int limit){
        Pagedao findpage = accountService.findpage ( account, offset, limit );
        return findpage;
    }
}
