package com.yunhe.tianhe.controller.cars;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.RentOrder;
import com.yunhe.tianhe.service.cars.RentOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/29 0029 上午 9:24
 * @Description TODO
 */
@Controller
@RequestMapping("/rentorder")
public class RentOrderController {
    @Autowired
    private RentOrderService rentOrderService;


    @RequestMapping("/rentorderlist")                  //到列表
    private String tolist(){
        return "cars/rentorder/list";
    }

    @RequestMapping("/page")  //分页
    @ResponseBody
    private Pagedao<RentOrder> list(Integer offset, Integer limit, String searchCode){
        Pagedao<RentOrder> list = rentOrderService.list(offset,limit, searchCode);
        return list;
    }

    /*@RequestMapping("/add")      //到添加页面
    public String add(){
        return "cars/rentorder/add";
    }

    @RequestMapping("/save")       //保存并重定向到列表
    public String save(RentOrder rentOrder){
        rentOrderService.save(rentOrder);
        return "redirect:/rentorder/rentorderlist";
    }*/



    @RequestMapping("/delete")      //删除
    @ResponseBody
    public JSONResponse delete(String ids){
        rentOrderService.delete(ids);
        return JSONResponse.success("删除成功");
    }







}
