<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/22 0022
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人信息设置</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
</head>
<body>
<div class="layui-tab layui-tab-card">
    <ul class="layui-tab-title">
        <li class="layui-this">用户资料</li>
        <li>密码信息</li>
    </ul>
    <div class="layui-tab-content" >
        <div class="layui-tab-item layui-show">
            <div class="layui-row">
                <div class="layui-col-md4 layui-col-md-offset3">
                    <div class="grid-demo grid-demo-bg1">
                        <form class="layui-form" action="" method="post">
                            <div class="layui-form-item">
                                <label class="layui-form-label">账户</label>
                                <div class="layui-input-block">
                                    <input type="text" name="title" lay-verify="title" autocomplete="off" placeholder="请输入账户" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">工号</label>
                                <div class="layui-input-block">
                                    <input type="text" name="title" lay-verify="title" autocomplete="off" placeholder="请输入工号" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">姓名</label>
                                <div class="layui-input-block">
                                    <input type="text" name="title" lay-verify="title" autocomplete="off" placeholder="请输入姓名" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-tab-item">
            <div class="layui-row">
                <div class="layui-col-md4 layui-col-md-offset3">
                    <div class="grid-demo grid-demo-bg1">
                        <div class="layui-form-item">
                            <label class="layui-form-label">原密码</label>
                            <div class="layui-input-block">
                                <input type="password" name="title" lay-verify="title" autocomplete="off" placeholder="请输入原密码" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">新密码</label>
                            <div class="layui-input-block">
                                <input type="password" name="title" lay-verify="title" autocomplete="off" placeholder="请输入新密码" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">确认密码</label>
                            <div class="layui-input-block">
                                <input type="password" name="title" lay-verify="title" autocomplete="off" placeholder="请再输一次" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <div class="layui-input-block">
                                <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/system/personal/list.js"></script>
</body>
</html>