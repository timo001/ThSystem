package com.yunhe.tianhe.entity.cars;

import java.util.Date;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/28 0028 上午 9:18
 * @Description: 车辆图片表
 */
public class CarPic {
    private Long picid;
    private String picname;
    private String picremark;
    private String picurl;
    private String carcode;
    private Date addtime;

    public Long getPicid() {
        return picid;
    }

    public void setPicid(Long picid) {
        this.picid = picid;
    }

    public String getPicname() {
        return picname;
    }

    public void setPicname(String picname) {
        this.picname = picname;
    }

    public String getPicremark() {
        return picremark;
    }

    public void setPicremark(String picremark) {
        this.picremark = picremark;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    public String getCarcode() {
        return carcode;
    }

    public void setCarcode(String carcode) {
        this.carcode = carcode;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}
