<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/22 0022
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人信息设置</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="box-padding-15">
     <span class="layui-breadcrumb" lay-separator=">">
        <a href="/" target="_top">首页</a>
        <a href="/permi/account/list">账户管理</a>
        <a><cite>编辑</cite></a>
    </span>
    <div class="layui-row">
        <div class="layui-col-md4 layui-col-md-offset3">
            <div class="grid-demo grid-demo-bg1">
                <form class="layui-form" method="post">
                    <div class="layui-form-item">
                        <label class="layui-form-label">账户</label>
                        <div class="layui-input-block">
                            <input type="hidden" name="uid" value="${account.uid}">
                            <input type="text" name="username" value="${account.username}" lay-verify="required"
                                   autocomplete="off" placeholder="请输入账户" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">昵称</label>
                        <div class="layui-input-block">
                            <input type="text" name="nickname" value="${account.nickname}" lay-verify="required"
                                   autocomplete="off" placeholder="请输入工号" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">姓名</label>
                        <div class="layui-input-block">
                            <input type="text" name="realname" value="${account.realname}" lay-verify="required"
                                   autocomplete="off" placeholder="请输入姓名" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">手机</label>
                        <div class="layui-input-block">
                            <input type="text" name="mobile" value="${account.mobile}" lay-verify="required|phone"
                                   autocomplete="off" placeholder="请输入手机" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">邮箱</label>
                        <div class="layui-input-block">
                            <input type="text" name="email" lay-verify="required|email"value="${account.email}" autocomplete="off" placeholder="请输入邮箱"
                                   class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">工号</label>
                        <div class="layui-input-block">
                            <input type="text" name="personid" value="${account.personid}" lay-verify="required"
                                   autocomplete="off" placeholder="请输入工号" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">租赁店</label>
                        <div class="layui-input-block">
                            <select name="deptcode" lay-verify="required">
                                <c:forEach items="${list}" var="dept">
                                    <option value="${dept.deptid}"  <c:if test="${dept.deptid} eq ${account.deptcode}">selected</c:if > >${dept.deptname}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">角色</label>
                        <div class="layui-input-block">
                            <select name="rolecode" lay-verify="required">
                                <c:forEach items="${rolelist}" var="role">
                                    <option value="${role.roleid}" <c:if test="${role.roleid} eq ${account.rolecode}">selected</c:if > >${role.rolename}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">初始密码</label>
                        <div class="layui-input-block">
                            <input type="text" name="userpwd" value="${account.userpwd}" lay-verify="required"
                                   autocomplete="off" placeholder="请输入初始密码" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">锁定</label>
                        <div class="layui-input-block">
                            <input type="radio" name="userstate" lay-filter="filter" value="0" <c:if test="${account.userstate eq 0}">checked</c:if> title="是" >
                            <input type="radio" name="userstate" lay-filter="filter" value="1" <c:if test="${account.userstate eq 1}">checked</c:if> title="否" >
                        </div>
                    </div>
                    <div class="layui-form-item <c:if test="${account.userstate eq 1}">layui-hide</c:if> ">
                        <label class="layui-form-label">锁定原因</label>
                        <div class="layui-input-block">
                            <input type="text" name="${account.remark}" value="remark" lay-verify="required" id="remark"  autocomplete="off" placeholder="请输入锁定原因"
                                   class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/premi/account/edit.js"></script>
</body>
</html>
