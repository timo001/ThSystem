package com.yunhe.tianhe.service.system.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.dto.TreeNode;
import com.yunhe.tianhe.dao.system.SysdictDao;
import com.yunhe.tianhe.entity.system.Sysdict;
import com.yunhe.tianhe.service.system.SysdictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 16:03
 * @Description: 实现SysdictService接口
 */
@Service
public class SysdictServiceImpl implements SysdictService{

    @Autowired
    private SysdictDao sysdictDao;
    @Override
    public void save(Sysdict sysdict) {
        sysdictDao.save ( sysdict );
    }

    @Override
    public void deleted(String ids) {
        String[] split = ids.split ( "," );
        for (String id:split){
            sysdictDao.deleted ( new Long(id) );
        }
    }

    @Override
    public void update(Sysdict sysdict) {
        sysdictDao.update ( sysdict );
    }

    @Override
    public Sysdict find(Long id) {
        return sysdictDao.find ( id );
    }

    @Override
    public List<Sysdict> findlist(Sysdict sysdict) {
        return sysdictDao.findlist (sysdict);
    }

    @Override
    public List<TreeNode> findtree(Sysdict sysdict) {
        List<Sysdict> findlist = this.findlist ( sysdict );
        List<TreeNode> treeNodes = new ArrayList<> ();
        if (findlist != null){
            for (Sysdict s: findlist){
                TreeNode treeNode = new TreeNode ( s.getId (), s.getFid (), s.getName () );
                treeNodes.add ( treeNode );
            }
        }
        return treeNodes;
    }

    @Override
    public Pagedao<Sysdict> findpage(Sysdict sysdict,int offset, int limit) {
        Pagedao<Sysdict> pagedao = new Pagedao<> ();
        Integer count = sysdictDao.count ();
        List<Sysdict> findpage=null;
        if (count>0){
             findpage = sysdictDao.findpage (sysdict, offset, limit );
             pagedao.setSize ( limit );
             pagedao.setPage ( offset/limit+1 );
        }
        pagedao.setTotal ( count );
        pagedao.setRows ( findpage );
        return pagedao;
    }
}
