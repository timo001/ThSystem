/**
 * Created by Administrator on 2017/11/23 0023.
 */
layui.use(['element','layer','form'], function() {
    var layer = layui.layer;
    var form = layui.form;
    var element = layui.element;


    var lng = $("#gpslon").val(); //获取经度
    var lat = $("#gpslat").val();//获取纬度

    //百度地图
    var map = new BMap.Map("container");        //在container容器中创建一个地图,参数container为div的id属性;
    var point = new BMap.Point(120.2,30.25);    //创建点坐标
    map.centerAndZoom(point, 14);                //初始化地图，设置中心点坐标和地图级别
    map.enableScrollWheelZoom();                //激活滚轮调整大小功能
    map.addControl(new BMap.NavigationControl());    //添加控件：缩放地图的控件，默认在左上角；
    map.addControl(new BMap.MapTypeControl());        //添加控件：地图类型控件，默认在右上方；
    map.addControl(new BMap.ScaleControl());        //添加控件：地图显示比例的控件，默认在左下方；
    map.addControl(new BMap.OverviewMapControl());  //添加控件：地图的缩略图的控件，默认在右下方； TrafficControl
    var search = new BMap.LocalSearch("中国", {
        onSearchComplete: function(result){
            if (search.getStatus() == BMAP_STATUS_SUCCESS){
                var res = result.getPoi(0);
                var point = res.point;
                map.centerAndZoom(point, 11);
            }

        },renderOptions: {  //结果呈现设置，
            map: map,
            autoViewport: true,
            selectFirstResult: true
        } ,onInfoHtmlSet:function(poi,html){
            $("#gpslon").val(poi.point.lng); //获取经度
            $("#gpslat").val(poi.point.lat);//获取纬度
        }
    });
    //根据做标显示位置
    if(lng != "" && lat != ""){
        map.clearOverlays();
        var new_point = new BMap.Point(lng,lat);
        var marker = new BMap.Marker(new_point);  // 创建标注
        map.addOverlay(marker);              // 将标注添加到地图中
        map.panTo(new_point);
    }

    //根据地址显示位置
    $("#incity,#address").on("input",function () {
        var newVar = $("#incity").val()+$("#address").val();
        console.log(newVar)
        //初始化显示的城市
        search.search(newVar);
    })




    //监听提交
    form.on('submit(formDemo)', function(data){
        $.ajax({
            url:"/system/dept/update",
            type:"post",
            dataType:"json",
            data:data.field,
            success:function (msg) {
                layer.msg("修改成功！",{time:1000},function () {
                    location.href="/system/dept/list";
                })
            }

        })
        return false;
    })
})