<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/27 0027
  Time: 下午 2:33
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix='fmt' uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改</title>
    <link rel="stylesheet" href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="/static/js/plugins/bootstrap-table/bootstrap-table.css">
    <link rel="stylesheet" href="/static/css/common.css">
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
</head>
<body>
<div class="layui-box box-padding-15">
<span class="layui-breadcrumb">
    <a href="/" target="_parent">首页</a>
                <a href="/maintain/maintainlist">车辆保养</a>
                <a><cite>修改</cite></a>
</span>
<hr>
<form class="layui-form" id="form" action="/maintain/update">
    <div class="layui-form-item">
        <label class="layui-form-label">车牌号</label>
        <div class="layui-input-block">
            <input name="carcode" lay-verify="required|carCode" value="${edit.carcode}" placeholder="请输入车牌号" autocomplete="off" class="layui-input">
            <input type="hidden" name="recordid" value="${edit.recordid}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">发车里程</label>
        <div class="layui-input-block">
            <input name="initmile" lay-verify="required" value="${edit.initmile}" placeholder="请输入发车里程" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">发车油量</label>
        <div class="layui-input-block">
            <input name="initoil" lay-verify="required" value="${edit.initoil}" placeholder="请输入发车油量" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">保养地点</label>
        <div class="layui-input-block">
            <input  name="address" lay-verify="required" value="${edit.address}" placeholder="请输入保养地点" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">保养人员</label>
        <div class="layui-input-block">
            <input name="headman" lay-verify="required" value="${edit.headman}" placeholder="请输入保养人员" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">经办手机</label>
        <div class="layui-input-block">
            <input name="headmobile" lay-verify="required" value="${edit.headmobile}" placeholder="请输入经办手机" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">出车时间</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input "
                       value="<fmt:formatDate value="${edit.usecartime}" pattern="yyyy-MM-dd"></fmt:formatDate>"
                       id="test-limit1" placeholder="请选择出车时间" name="usecartime">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">保养费用</label>
        <div class="layui-input-block">
            <input name="costmoney"  placeholder="请输入保养费用"  value="${edit.costmoney}" autocomplete="off" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">保养完毕</label>
        <div class="layui-input-block">
            <input type="checkbox" name="like[game]" title="更多" id="checkbox">该车辆已保养完毕，继续填写还车信息
        </div>
    </div>


    <div class="layui-hide" id="hide">


        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">还车时间</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" id="test-limit"
                           value="<fmt:formatDate value="${edit.returntime}" pattern="yyyy-MM-dd"></fmt:formatDate>"
                           placeholder="请选择还车时间" name="returntime">
                </div>
            </div>
        </div>



        <div class="layui-form-item">
            <label class="layui-form-label">还车里程</label>
            <div class="layui-input-block">
                <input  name="returnmile" placeholder="请输入还车里程" value="${edit.returnmile}" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">还车油量</label>
            <div class="layui-input-block">
                <input name="returnoil"  placeholder="请输入还车油量" value="${edit.returnoil}" autocomplete="off" class="layui-input">
            </div>
        </div>


        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">保养内容</label>
            <div class="layui-input-block">
                <textarea name="costcontent" placeholder="请输入保养内容" class="layui-textarea">${edit.costcontent}</textarea>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <textarea name="remark" placeholder="请输入备注" class="layui-textarea">${edit.remark}</textarea>
            </div>
        </div>


        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">下次保养时间</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input"
                           value="<fmt:formatDate value="${edit.nexttime}" pattern="yyyy-MM-dd"></fmt:formatDate>"
                           id="test-limit2" placeholder="请选择下次保养时间" name="nexttime">
                </div>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">下次保养里程</label>
            <div class="layui-input-block">
                <input name="nextmile"  value="${edit.nextmile}" placeholder="请输入下次保养里程" autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formSubmit" type="submit">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>

</form>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script>
    layui.use(['form','element','layer','laydate','upload'], function() {
        var form = layui.form;
        var laydate = layui.laydate;
        upload = layui.upload;


        form.on("checkbox",function () {
            $("#hide").toggleClass('layui-hide')
        })




        var ins22 = laydate.render({
            elem: '#test-limit1'
            , min: '2016-10-14'
            , max: '2080-10-14',
            ready: function(){

            }
        })

        var ins22 = laydate.render({
            elem: '#test-limit'
            , min: '2016-10-14'
            , max: '2080-10-14',
            ready: function(){

            }
        })

        var ins22 = laydate.render({
            elem: '#test-limit2'
            , min: '2016-10-14'
            , max: '2080-10-14',
            ready: function(){

            }
        })

        form.verify({
            carCode:function (value) {                      //carCode 不是name！
                var unique = false;
                $.ajax({
                    url:'/cars/verifyCarCode',
                    data:{carcode:value},
                    dataType:'json',
                    async:false,
                    success:function(msg){
                        if (msg.code == 1){             //如果为1，表示该车牌存在
                            unique = msg.data
                        }
                    },error:function () {
                        layer.msg("服务器错误")
                    }
                });
                if (!unique){
                    return "该车牌不存在";
                }
            }
        })
    })
</script>
</body>
</html>
