<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/25 0025
  Time: 上午 10:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>车辆自用记录编辑</title>
    <meta name="viewport" content="width=device-width"/>
    <link href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet"/>
    <link href="/static/js/plugins/bootstrap-table/bootstrap-table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="box-padding-15">
    <div class="layui-container box-form">
        <form class="layui-form " action="/selfuse/update" method="post">
            <input type="hidden" name="recordId" value="${selfuse.recordId}">
            <div class="layui-form-item">
                <label class="layui-form-label ">车牌号码</label>
                <div class="layui-input-block">
                    <input type="text" name="carCode" value="${selfuse.carCode}" lay-verify="required|carCode" placeholder="请输入车牌号"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label ">发车里程</label>
                <div class="layui-input-block">
                    <input type="text" name="initMile"  value="${selfuse.initMile}" placeholder="请输入使用前里程(km)"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label ">发车油量</label>
                <div class="layui-input-block">
                    <input type="text" name="initOil"  value="${selfuse.initOil}" placeholder="请输入使用前油量(L)"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">出车时间</label>
                <div class="layui-input-block">
                    <input type="text" name="useCarTime" id="useCarTime" value="<fmt:formatDate value="${selfuse.useCarTime}" pattern="yyyy-MM-dd"></fmt:formatDate>" placeholder="请选择出车时间"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">还车时间</label>
                <div class="layui-input-block">
                    <input type="text" name="returnTime" id="returnTime" value="<fmt:formatDate value="${selfuse.returnTime}" pattern="yyyy-MM-dd"></fmt:formatDate>" placeholder="请选择还车时间"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label ">还车剩余油量</label>
                <div class="layui-input-block">
                    <input type="text" name="returnOil"  value="${selfuse.returnOil}" placeholder="请输入还车剩余油量(L)"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label ">还车里程</label>
                <div class="layui-input-block">
                    <input type="text" name="returnMile" value="${selfuse.returnMile}"  placeholder="请输入使用后里程(km)"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label ">经办人员</label>
                <div class="layui-input-block">
                    <input type="text" name="headMan"  value="${selfuse.headMan}" placeholder="请输入经办人员"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label ">经办人手机</label>
                <div class="layui-input-block">
                    <input type="text" name="headMobile" value="${selfuse.headMobile}" lay-verify="required|phone" placeholder="请输入经办人手机"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">使用事由</label>
                <div class="layui-input-block">
                    <select name="noticeItem"value="${selfuse.noticeItem}" >
                        <option>请选择使用事由</option>
                        <option value="1" <c:if test="${selfuse.noticeItem eq 1}">selected</c:if>>维修</option>
                        <option value="2" <c:if test="${selfuse.noticeItem eq 2}">selected</c:if>>年检</option>
                        <option value="3" <c:if test="${selfuse.noticeItem eq 3}">selected</c:if>>私用</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label ">备注信息</label>
                <div class="layui-input-block">
                    <input type="text" name="remark"  value="${selfuse.remark}" placeholder="请输入备注信息"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">添加时间</label>
                <div class="layui-input-block">
                    <input type="text" name="addTime" id="addTime" value="<fmt:formatDate value="${selfuse.addTime}" pattern="yyyy-MM-dd"></fmt:formatDate>" lay-verify="required" placeholder="请选择添加时间"
                           autocomplete="off" class="layui-input">
                </div>
            </div>
            <div class="layui-form-item">
                <button class="layui-btn" lay-submit lay-filter="formSubmit">保存</button>
                <a href="/selfuse">
                    <button type="button" class="layui-btn layui-btn-primary">取消</button>
                </a>
            </div>
        </form>
    </div>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script>

    layui.use(['element', 'layer', 'form', 'laydate', 'upload'], function () {
        var form = layui.form,
            laydate = layui.laydate
            , upload = layui.upload
            , element = layui.element,
            layer = layui.layer;

        //日期时间范围
        laydate.render({
            elem: '#useCarTime',
        });

        laydate.render({
            elem: '#returnTime',
        });

        laydate.render({
            elem: '#addTime',
        });

        form.verify({
            carCode:function (value) {                      //carCode 不是name！
                var unique = false;
                $.ajax({
                    url:'/cars/verifyCarCode',
                    data:{carcode:value},
                    dataType:'json',
                    async:false,
                    success:function(msg){
                        if (msg.code == 1){             //如果为1，表示该车牌存在
                            unique = msg.data
                        }
                    },error:function () {
                        layer.msg("服务器错误")
                    }
                });
                if (!unique){
                    return "该车牌不存在";
                }
            }
        })
    });
</script>
</body>
</html>
