package com.yunhe.commons.util;

import org.apache.ibatis.session.SqlSession;

/**
 * @author tianxuan
 * @version V1.0
 * @Date 2017/11/10 0010 上午 11:18
 * @Description: TODO
 */

public class DaoFactory<T> {

    public  T getDao(Class<T> tClass){
        SqlSession session = MybatisUtils.getSession();
        T mapper = session.getMapper(tClass);
        return  mapper;
    }
}
