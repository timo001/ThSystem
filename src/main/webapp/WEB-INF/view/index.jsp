<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- Created by IntelliJ IDEA. --%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>主页</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/index.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">
            天禾汽车租赁系统
        </div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                    admin
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="">基本资料</a></dd>
                    <dd><a href="javescript:" id="uppwd">修改密码</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="javascript:" id="exit">退出</a></li>
        </ul>
    </div>
    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree"  lay-filter="test">
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:">我要租车</a>
                    <dl class="layui-nav-child">
                        <dd><a href="/register/list" target="ifram">租车登记</a></dd>
                        <dd><a href="/book" target="ifram">预定租车</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:">车辆管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="/insurance/insurancelist" target="ifram">车辆保险</a></dd>
                        <dd><a href="/maintain/maintainlist" target="ifram">车辆保养</a></dd>
                        <dd><a href="/repair" target="ifram">车辆维修</a></dd>
                        <dd><a href="/yearcheck/yearchecklist" target="ifram">车辆年检</a></dd>
                        <dd><a href="/cars" target="ifram">车辆查询</a></dd>
                        <dd><a href="/selfuse" target="ifram">车辆自用</a></dd>
                        <dd><a href="/rentorder/rentorderlist" target="ifram">租用历史</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:">分享爱车</a>
                    <dl class="layui-nav-child">
                        <dd><a href="javescript：" target="ifram">个人信息设置</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:">客户管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="/clientuser" target="ifram">客户信息</a></dd>
                        <dd><a href="/baclist" target="ifram">黑名单</a></dd>
                        <dd><a href="/credit" target="ifram">客户信用</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:">权限管理</a>
                    <dl class="layui-nav-child">
                        <dd><a href="/permi/role/list" target="ifram">角色管理</a></dd>
                        <dd><a href="/permi/account/list" target="ifram">账户管理</a></dd>
                        <dd><a href="/permi/resource/list" target="ifram">资源管理</a></dd>
                    </dl>
                </li>
                 <li class="layui-nav-item layui-nav-itemed">
                     <a class="" href="javascript:">系统设置</a>
                     <dl class="layui-nav-child">
                         <dd><a href="/system/personal/list" target="ifram">个人信息设置</a></dd>
                         <dd><a href="/system/level/list" target="ifram">系统会员级别</a></dd>
                         <dd><a href="/system/sysdict/list" target="ifram">系统字典管理</a></dd>
                         <dd><a href="/system/busi/list" target="ifram">系统操作日志</a></dd>
                         <dd><a href="/system/dept/list" target="ifram">门店信息管理</a></dd>
                     </dl>
                </li>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <iframe name="ifram"></iframe>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->
        © layui.com - 底部固定区域
    </div>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/index.js"></script>
</body>
</html>
