

package com.yunhe.tianhe.service.client;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.client.BacList;
import com.yunhe.tianhe.entity.client.ClientCompany;
import com.yunhe.tianhe.entity.client.ClientUser;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/22 0022 下午 8:02
 * @Description: TODO
 */
public interface ClientUserService {

    /**
     * 根据手机号码查询用户信息
     * @param mobile 手机号码
     * @return
     */
    ClientUser findByMobile(String mobile);
    /**
     * 首次用车保存新客户信息
     * @param clientUser
     */
    void save(ClientUser clientUser);

    void add(ClientUser clientUser, ClientCompany clientCompany);

    /**
     * 根据ID查询一个客户
     * @param id
     * @return
     */
    ClientUser findById(Long id);


    /**
     * 客户信息列表
     * @return
     */
    Pagedao<ClientUser> findPage(@Param("offset") Integer offset,@Param("limit") Integer limit,@Param("memLevel") Integer memLevel, @Param("addTime")Date addTime);




    /**
     * 删除
     * @param id
     */
    void delete(Long id);

    /**
     * 批量删除
     * @param ids
     */
    void deleteBatch(String ids);

    /**
     * 修改客户信息
     * @param clientUser
     */
    void update(ClientUser clientUser);

}