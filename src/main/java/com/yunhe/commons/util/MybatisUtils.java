package com.yunhe.commons.util;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.session.SqlSessionManager;

import java.io.InputStream;

/**
 * @author tianxuan
 * @version V1.0
 * @Date 2017/11/9 0009 上午 11:38
 * @Description: TODO
 */

public class MybatisUtils {
    //1. 根据mybatis-conf配置信息来构建session 工厂
    public static SqlSession getSession() {
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream( "mybatis-conf.xml" );
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(is);
        SqlSession session = SqlSessionManager.newInstance(sessionFactory);//自动创建session，提交事务，关闭session，不需要openSession
        return session;
    }

}
