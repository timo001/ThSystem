package com.yunhe.tianhe.entity.premission;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/29 0029 19:35
 * @Description: 权限实体
 */
public class Resource {
    private Long permisid;//资源编号
    private String permisname;//中文名称
    private String permiscode;//资源代码
    private Integer permistype;//资源类型
    private String relateurl;//相对路径
    private Long parentid;//上级编号
    private String parentids;//上级目录
    private Integer isabled=0; //是否启用
    @DateTimeFormat(pattern = "YYYY-MM-dd HH:mm:ss")
    private Date addtime; //添加时间

    public Long getPermisid() {
        return permisid;
    }

    public void setPermisid(Long permisid) {
        this.permisid = permisid;
    }

    public String getPermisname() {
        return permisname;
    }

    public void setPermisname(String permisname) {
        this.permisname = permisname;
    }

    public String getPermiscode() {
        return permiscode;
    }

    public void setPermiscode(String permiscode) {
        this.permiscode = permiscode;
    }

    public void setPermistype(Integer permistype) {
        this.permistype = permistype;
    }

    public Integer getPermistype() {
        return permistype;
    }

    public String getRelateurl() {
        return relateurl;
    }

    public void setRelateurl(String relateurl) {
        this.relateurl = relateurl;
    }

    public Long getParentid() {
        return parentid;
    }

    public void setParentid(Long parentid) {
        this.parentid = parentid;
    }

    public String getParentids() {
        return parentids;
    }

    public void setParentids(String parentids) {
        this.parentids = parentids;
    }

    public Integer getIsabled() {
        return isabled;
    }

    public void setIsabled(Integer isabled) {
        this.isabled = isabled;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}
