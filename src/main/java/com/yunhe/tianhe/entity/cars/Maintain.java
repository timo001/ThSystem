package com.yunhe.tianhe.entity.cars;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/25 0025 上午 11:49
 * @Description 车辆保养
 */
public class Maintain {
    private Long recordid;   //记录编号
    private String carcode;  //车辆编号
    private Double initmile;  //发车里程
    private Double initoil;   //发车油量
    private String address;   //保养地点
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date usecartime;  //出车时间
    private String headman;   //经办人员
    private String headmobile; //经办手机
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date returntime;   //还车时间
    private Double returnmile;   //还车里程
    private Double returnoil;   //还车油量
    private Double costmoney;   //保养费用
    private String costcontent;  //保养内容
    private String remark;    //备注信息
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date nexttime;     //下次保养时间
    private Double nextmile;   //下次保养里程
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date addtime;       //添加时间


    public Long getRecordid() {
        return recordid;
    }

    public void setRecordid(Long recordid) {
        this.recordid = recordid;
    }

    public String getCarcode() {
        return carcode;
    }

    public void setCarcode(String carcode) {
        this.carcode = carcode;
    }

    public Double getInitmile() {
        return initmile;
    }

    public void setInitmile(Double initmile) {
        this.initmile = initmile;
    }

    public Double getInitoil() {
        return initoil;
    }

    public void setInitoil(Double initoil) {
        this.initoil = initoil;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getUsecartime() {
        return usecartime;
    }

    public void setUsecartime(Date usecartime) {
        this.usecartime = usecartime;
    }

    public String getHeadman() {
        return headman;
    }

    public void setHeadman(String headman) {
        this.headman = headman;
    }

    public String getHeadmobile() {
        return headmobile;
    }

    public void setHeadmobile(String headmobile) {
        this.headmobile = headmobile;
    }

    public Date getReturntime() {
        return returntime;
    }

    public void setReturntime(Date returntime) {
        this.returntime = returntime;
    }

    public Double getReturnmile() {
        return returnmile;
    }

    public void setReturnmile(Double returnmile) {
        this.returnmile = returnmile;
    }

    public Double getReturnoil() {
        return returnoil;
    }

    public void setReturnoil(Double returnoil) {
        this.returnoil = returnoil;
    }

    public Double getCostmoney() {
        return costmoney;
    }

    public void setCostmoney(Double costmoney) {
        this.costmoney = costmoney;
    }

    public String getCostcontent() {
        return costcontent;
    }

    public void setCostcontent(String costcontent) {
        this.costcontent = costcontent;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getNexttime() {
        return nexttime;
    }

    public void setNexttime(Date nexttime) {
        this.nexttime = nexttime;
    }

    public Double getNextmile() {
        return nextmile;
    }

    public void setNextmile(Double nextmile) {
        this.nextmile = nextmile;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}
