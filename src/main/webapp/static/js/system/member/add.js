/**
 * Created by Administrator on 2017/11/23 0023.
 */
layui.use(['element','layer','form'], function() {
    var layer = layui.layer;
    var form = layui.form;
    var element = layui.element;

    $("#parentname").on("input",function () {
        var name = $(this).val();
        $.ajax({
            url:"/system/sysdict/parent",
            dataType:"json",
            data:{name:name},
            typt:"get",
            success:function (msg) {
                if (msg.code==1){
                    layer.msg("不存在该父级名称！")
                }
            },
            error:function () {
                console.log("服务器忙！")
            }
        })
    })

    //监听提交
    form.on('submit(formDemo)', function(data){
        $.ajax({
            url:"/system/level/save",
            type:"post",
            dataType:"json",
            data:data.field,
            success:function (msg) {
                layer.msg("添加成功！",{time:1000},function () {
                    location.href="/system/level/list";
                })
            },
            error:function () {
                layer.msg("服务器忙！请稍后再试！")
            }

        })
        return false;
    });
})