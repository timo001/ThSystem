package com.yunhe.tianhe.dao.cars;

import com.yunhe.tianhe.entity.cars.RentOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/27 0027 下午 2:32
 * @Description: TODO
 */
public interface RentOrderDao {
    void save(RentOrder rentOrder); //保存

    void delete(Long id);    //删除

    Integer findCount(@Param("searchCode")String searchCode);       //统计，（分页）

    List findAll(@Param("offset")Integer offset, @Param("limit") Integer limit, @Param("searchCode")String searchCode);   //分页

}
