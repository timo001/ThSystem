package com.yunhe.tianhe.service.system.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.dao.system.DeptDao;
import com.yunhe.tianhe.entity.system.Dept;
import com.yunhe.tianhe.service.system.DeptServive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/23 0023 12:06
 * @Description: 实现DeptServive类
 */
@Service
public class DeptServiceImpl implements DeptServive{
    @Autowired
    private DeptDao deptDao;

    @Override
    public void save(Dept dept) {
        deptDao.save ( dept );
    }

    @Override
    public void deleted(String ids) {
        String[] split = ids.split ( "," );
        for (String deptid:split){
            deptDao.deleted ( new Long (deptid  ) );
        }
    }

    @Override
    public void update(Dept dept) {
        deptDao.update ( dept );
    }

    @Override
    public Dept find(Long deptid) {
        return deptDao.find ( deptid );
    }

    @Override
    public List<Dept> findlist() {
        return deptDao.findlist ();
    }

    @Override
    public Pagedao findpage(int offset, int limit) {
        Pagedao<Dept> pagedao = new Pagedao<> ();
        List<Dept> findpage=null;
        int count = deptDao.count ();
        if (count>0){
             findpage = deptDao.findpage ( offset, limit );
            pagedao.setPage ( offset/limit+1 );
            pagedao.setSize ( limit );
        }
        pagedao.setTotal ( count );
        pagedao.setRows ( findpage );
        return pagedao;
    }
}
