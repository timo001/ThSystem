package com.yunhe.tianhe.service.client.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.util.StringUtils;
import com.yunhe.tianhe.dao.client.BacListDao;
import com.yunhe.tianhe.entity.client.BacList;
import com.yunhe.tianhe.entity.client.ClientUser;
import com.yunhe.tianhe.service.client.BacListService;
import com.yunhe.tianhe.service.client.ClientUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/22 0022 下午 8:03
 * @Description: TODO
 */
@Service
public class BacListServiceImpl implements BacListService {

    @Autowired
    private BacListDao bacListDao;
    @Override
    public void save(BacList bacList) {
        bacListDao.save(bacList);
    }

    @Override
    public Pagedao<BacList> findPage(Integer offset, Integer limit,String name,String idcard,String mobile) {
        Pagedao<BacList> pagedao = new Pagedao<>();
        List<BacList> data = new ArrayList<>();
        Integer count = 0;
        count = bacListDao.findCount( name, idcard, mobile);
/*        if(!StringUtils.notEmpty(name)) name = null;
        if(!StringUtils.notEmpty(idcard)) idcard = null;
        if(!StringUtils.notEmpty(mobile)) mobile = null;*/
        data = bacListDao.findList( offset, limit, name, idcard, mobile);

        pagedao.setTotal(count );
        pagedao.setRows(data);
        return pagedao;
    }

    @Override
    public void delete(Long id) {
        bacListDao.delete(id);
    }

    @Override
    public void deleteBatch(String ids) {
        if(StringUtils.notEmpty(ids)){
            String[] idArr = ids.split(",");
            for(String id : idArr) {
                this.delete(Long.valueOf(id));
            }
        }
    }

    @Override
    public BacList findById(Long bid) {
        return bacListDao.findById(bid);
    }

    @Override
    public void update(BacList bacList) {
        bacListDao.update(bacList);
    }
}
