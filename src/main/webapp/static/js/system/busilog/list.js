/**
 * Created by Administrator on 2017/11/23 0023.
 */
//JavaScript代码区域
layui.use(['element','layer','laydate'], function(){
    var layer = layui.layer;
    var element = layui.element;
    var laydate = layui.laydate;
    laydate.render({
        elem: '#date1',
        type:"datetime"//指定元素
    });
    laydate.render({
        elem: '#date2',
        type:"datetime"//指定元素
    });

    window.operateEvents = {
        'click .RoleOfedit': function (e, value, row, index) {
            alert("asdfaas");
        }
    }
    $('#tb_departments').bootstrapTable({
        url: '/system/busi/page',         //请求后台的URL（*）
        method: 'get',                      //请求方式（*）
        // toolbar: '#toolbar',                //工具按钮用哪个容器
        striped: true,                      //是否显示行间隔色
        cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
        pagination: true,                   //是否显示分页（*）
        sortable: true,                     //是否启用排序
        sortOrder: "asc",                   //排序方式
        // queryParams: {type:1},             //传递参数（*）
        sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
        pageNumber:1,                       //初始化加载第一页，默认第一页
        pageSize: 10,             //每页的记录行数（*）
        pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
        // search: true,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
        // strictSearch: true,
        // showColumns: true,                  //是否显示所有的列
        showRefresh: false,                  //是否显示刷新按钮
        // minimumCountColumns: 2,             //最少允许的列数
        clickToSelect: false,                //是否启用点击选中行
        height: 1000,                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
        uniqueId: "logid",                     //每一行的唯一标识，一般为主键列
        // showToggle:true,                    //是否显示详细视图和列表视图的切换按钮
        // cardView: false,                    //是否显示详细视图
        detailView: false,                   //是否显示父子表
        columns: [{
            field: 'logid',
            title: '编号',
            width:'16%',
            align: 'center'
        },{
            field: 'businame',
            title: '操作业务',
            width:'16%',
            align: 'center'
        },{
            field: 'busitype',
            title: '操作类型',
            width:'17%',
            align: 'center'
        },{
            field: 'addtime',
            title: '操作时间',
            width:'16%',
            align: 'center',
            formatter:function (value, row, index) {
                if(isNaN(value)){
                    return "--";
                } else {
                    var time = new Date(value);
                    return time.toLocaleString().replace("/","-").replace("/","-");
                }
            }
        },{
            field: 'busiconent',
            title: '操作内容',
            width:'19%',
            align: 'center'
        },{
            field: 'logid',
            title: '操作',
            width:'16%',
            align: 'center',
            events:operateEvents,
            formatter:function (value, row, index) {
                return ["<button type='button' class='RoleOfedit' >查看日志</button>"].join("");
            }
        }]
    });


    // '/system/busi/work?logid="+value+"

//查找
    $("#btn_find").on("click",function () {
        var businame = $("input[name=businame]");
        var busitype = $("input[name=busitype]");
        var begintime = $("input[name=begintime]");
        var endtime = $("input[name=endtime]");
        $('#tb_departments').bootstrapTable("refresh",{
            query:{
                businame:businame.val(),
                busitype:busitype.val(),
                begintime:begintime.val(),
                endtime:endtime.val()
            }
        })
        businame.val("");
        busitype.val("");
        begintime.val("");
        endtime.val("");
    })

   /* $(".work").on("click",function (type, selector, data, fn, one) {
        debugger
    })*/




});