package com.yunhe.tianhe.service.cars.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.util.StringUtils;
import com.yunhe.tianhe.dao.cars.MaintainDao;
import com.yunhe.tianhe.entity.cars.Insurance;
import com.yunhe.tianhe.entity.cars.Maintain;
import com.yunhe.tianhe.service.cars.MaintainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/25 0025 下午 1:40
 * @Description TODO
 */
@Service
public class MaintainServiceImpl implements MaintainService {
    @Autowired
    private MaintainDao maintainDao;

    @Override
    public Pagedao <Maintain> list(Integer limit, Integer offset,String carcode) {
        Pagedao <Maintain> pagedao = new Pagedao <>();
        List <Maintain> maintains = null;
        int count = maintainDao.pagedao(carcode);
        if (count > 0) {
            pagedao.setPage(offset);
            pagedao.setSize(limit);
            maintains = maintainDao.list(offset,limit,carcode);
        }
        pagedao.setTotal(count);
        pagedao.setRows(maintains);
        return pagedao;
    }

    @Override
    public void delete(String ids) {
        if (StringUtils.notEmpty(ids)){
            String[] split = ids.split(",");
            for (String p:split){
                maintainDao.delete(Integer.valueOf(p));
            }
        }
    }

    @Override
    public void save(Maintain maintain) {
        maintainDao.save(maintain);
    }

    @Override
    public Maintain edit(Long recordid) {
        Maintain edit = maintainDao.edit(recordid);
        return edit;
    }

    @Override
    public void update(Maintain maintain) {
        maintainDao.update(maintain);
    }
}
