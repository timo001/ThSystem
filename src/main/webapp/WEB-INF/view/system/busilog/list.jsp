<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/22 0022
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>系统日志管理</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet"/>
    <link href="/static/js/plugins/bootstrap-table/bootstrap-table.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="box-padding-15">
    <span class="layui-breadcrumb " lay-separator=">">
        <a href="/index" target="_top">首页</a>
        <a href="/system/busi/list"> 日志管理</a>
        <a><cite>列表</cite></a>
    </span>
    <div class="layui-row">
        <div class="layui-col-md2">
            <div class="grid-demo grid-demo-bg1">
                <div class="layui-form-item">
                    <label class="layui-form-label box-width">操作业务：</label>
                    <div class="layui-input-block">
                        <input type="text" name="businame" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-col-md2">
            <div class="grid-demo">
                <div class="layui-form-item">
                    <label class="layui-form-label box-width">操作类型：</label>
                    <div class="layui-input-block">
                        <input type="text" name="busitype" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-col-md2">
            <div class="grid-demo grid-demo-bg1">
                <div class="layui-form-item">
                    <label class="layui-form-label box-width">开始时间：</label>
                    <div class="layui-input-block">
                        <input type="text"id="date1" name="begintime"  lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-col-md2">
            <div class="grid-demo grid-demo-bg1">
                <div class="layui-form-item">
                    <label class="layui-form-label box-width">结束时间：</label>
                    <div class="layui-input-block">
                        <input type="text" name="endtime" id="date2" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-col-md2">
            <div class="grid-demo grid-demo-bg1">
                <button class="layui-btn" id="btn_find">查找</button>
            </div>
        </div>
    </div>
    <table id="tb_departments"></table>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/system/busilog/list.js"></script>
</body>
</html>
