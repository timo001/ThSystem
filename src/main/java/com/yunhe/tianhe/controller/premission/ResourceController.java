package com.yunhe.tianhe.controller.premission;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.premission.Resource;
import com.yunhe.tianhe.entity.premission.Role;
import com.yunhe.tianhe.service.premission.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/29 0029 20:00
 * @Description: 权限控制
 */
@Controller
@RequestMapping("/permi/resource")
public class ResourceController {
    @Autowired
    private ResourceService resourceService;
    @RequestMapping("/list")
    public String list(){
        return "permission/resource/list";
    }
    @RequestMapping("/add")
    public String add(){
        return "permission/resource/add";
    }

    @RequestMapping("/edit")
    public ModelAndView edit(Long permisid){
        ModelAndView modelAndView = new ModelAndView ( "permission/resource/edit" );
        Resource newresource = new Resource ();
        Resource resource = resourceService.find ( permisid );
        if (resource.getPermistype ()>1) {
            newresource.setPermistype ( resource.getPermistype () == 2 ? 1 : 2 );
            List<Resource> findlist = resourceService.findlist ( newresource );
            modelAndView.addObject ( "resourcelist", findlist );
        }
        modelAndView.addObject ( "resource",resource );

        return modelAndView;
    }

    @RequestMapping("/deleted")
    @ResponseBody
    public JSONResponse save(String ids){
        resourceService.deleted ( ids );
        return new JSONResponse ();
    }

    @RequestMapping("/save")
    @ResponseBody
    public JSONResponse save(Resource resource){
        resourceService.save ( resource );
        return new JSONResponse (  );
    }

    @RequestMapping("/update")
    @ResponseBody
    public JSONResponse update(Resource resource){
        resourceService.update ( resource );
        return new JSONResponse ( );
    }

    @RequestMapping("/page")
    @ResponseBody
    public Pagedao page(Resource resource,int offset, int limit){
        Pagedao<Resource> findpage = resourceService.findpage (resource, offset, limit );
        return findpage;
    }


    @RequestMapping("/parent")
    @ResponseBody
    public  List<Resource> page(Resource resource){
        List<Resource> findlist = resourceService.findlist ( resource );
        return findlist;
    }

    @RequestMapping("/type")
    @ResponseBody
    public JSONResponse type(Resource resource){
        List<Resource> findlist = resourceService.findlist (resource);
        return new JSONResponse ( 1,"成功！",findlist );
    }
}
