package com.yunhe.tianhe.service.cars;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.Car;
import org.apache.ibatis.annotations.Param;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/23 0023 上午 11:11
 * @Description: 车辆基本信息业务处理接口
 */
public interface CarService {
    /*车辆信息保存*/
    Long save(Car car);

    /*车辆信息修改*/
    void update(Car car);

    /*车辆删除*/
    void delete(Long carId);

    /*车辆 批量删除*/
    void deleteBatch(String ids);

    /*车辆信息查询*/
    Car find(Long carId);

    /*分页*/
    Pagedao<Car> findPage(@Param("offset") Integer offset, @Param("limit") Integer limit,@Param("searchCode")String searchCode,@Param("btrentdept") String btrentdept);

    int carcode(String carcode);  //根据车牌查询（是否有此车）
}
