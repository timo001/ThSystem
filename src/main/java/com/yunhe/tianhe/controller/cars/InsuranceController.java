package com.yunhe.tianhe.controller.cars;
import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.Insurance;
import com.yunhe.tianhe.service.cars.InsuranceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/23 0023 上午 9:18
 * @Description 车辆保险Controller
 */
@Controller
@RequestMapping("/insurance")
public class InsuranceController {
    @Autowired
    private InsuranceService insuranceService;      //调用车辆保险servlet


    @RequestMapping("/insurancelist")                //车辆保险列表
    public String list(){
        return "cars/insurance/list";
    }

    @RequestMapping("/page")                        //车辆保险列表分页
    @ResponseBody
    public Pagedao findlist(Integer limit, Integer offset){
        Pagedao findlist = insuranceService.findlist(limit, offset);
        return findlist;
    }

    @RequestMapping("/addlist")                         // 跳转到添加页面
    public String add(){
        return "cars/insurance/add";
    }


    @RequestMapping("/save")                               //车辆保险新增保存方法，完成后重定向到列表
    public String save(Insurance insurance){
        insuranceService.save(insurance);
        return "redirect:/insurance/insurancelist";
    }


    @RequestMapping("/delete")                             //删除
    @ResponseBody
    public JSONResponse delet(String ids){
        insuranceService.delete(ids);
        return new JSONResponse(1,"删除成功！");
    }

    @RequestMapping("/toupdate")                            //跳转到修改页面并带参数
    public ModelAndView toupdate(Long recordid){
        Insurance insurance = insuranceService.toupdate(recordid);
        return new ModelAndView("cars/insurance/edit","insurance",insurance);
    }


    @RequestMapping("/update")                                  //修改
    public String update(Insurance insurance){
        insuranceService.update(insurance);
        return "redirect:/insurance/insurancelist";
    }

    @RequestMapping("/findcarcode")                                        //查找
    @ResponseBody
    public Pagedao findcarcode(String car,Integer limit,Integer offset){
        Pagedao findcarcode = insuranceService.findcarcode(car,limit,offset);
        return findcarcode;
    }



}
