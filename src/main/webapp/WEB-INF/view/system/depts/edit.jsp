<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/22 0022
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>门店列表</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">
    <!-- 地图显示的样式 -->
    <style type="text/css">
        html{height:100%}
        body{height:100%;margin:0px;padding:0px}
        #container{height:100%}
    </style>
</head>
<body>
<div class="box-padding-15 ">
     <span class="layui-breadcrumb" lay-separator=">">
        <a href="/" target="_top">首页</a>
        <a href="/system/dept/list">门店管理</a>
        <a><cite>编辑</cite></a>
    </span>
    <div class="layui-row">
        <div class="layui-col-md4 layui-col-md-offset3">
            <div class="grid-demo grid-demo-bg1">
                <form class="layui-form" method="post">
                    <div class="layui-form-item">
                        <label class="layui-form-label">门店名称</label>
                        <div class="layui-input-block">
                            <input type="hidden" name="deptid" value="${dept.deptid}">
                            <input type="text" name="deptname" value="${dept.deptname}"  lay-verify="required" placeholder="请输入门店名称" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">门店区域</label>
                        <div class="layui-input-block">
                            <input type="text" name="incity" value="${dept.incity}" id="incity" lay-verify="required" placeholder="请输入门店区域" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">门店地址</label>
                        <div class="layui-input-block">
                            <input type="text" name="address" value="${dept.address}" id="address" lay-verify="required" placeholder="请输入门店地址" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">门店固话</label>
                        <div class="layui-input-block">
                            <input type="text" name="deptphone" value="${dept.deptphone}"  lay-verify="required" placeholder="请输入门店固话" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">店长姓名</label>
                        <div class="layui-input-block">
                            <input type="text" name="ownername" value="${dept.ownername}"  lay-verify="required" placeholder="请输入店长姓名" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">店长手机</label>
                        <div class="layui-input-block">
                            <input type="text" name="ownermobile" value="${dept.ownermobile}"  lay-verify="required" placeholder="请输入手机" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">地图标注</label>
                        <div class="layui-input-block">
                            <div id="container"  style="width:400px;height:400px;"></div>
                            <input type="hidden" name="gpslon" id="gpslon" value="${dept.gpslon}"><%--经度--%>
                            <input type="hidden" name="gpslat" id="gpslat" value="${dept.gpslat}" ><%--纬度--%>
                        </div>
                    </div>
                    <div class="layui-form-item layui-form-text">
                        <label class="layui-form-label">备注信息</label>
                        <div class="layui-input-block">
                            <textarea name="remark" value="${dept.remark}" placeholder="请输入备注" class="layui-textarea"></textarea>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                            <a href="/system/dept/list"><button type="button" class="layui-btn layui-btn-primary">取消</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>


<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="http://api.map.baidu.com/api?v=1.4" type="text/javascript"></script>
<script src="/static/js/system/dept/edit.js"></script>
</body>
</html>
