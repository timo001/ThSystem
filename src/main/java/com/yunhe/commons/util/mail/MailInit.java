package com.yunhe.commons.util.mail;

import com.sun.mail.util.MailSSLSocketFactory;
import org.apache.ibatis.annotations.Param;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * 发送邮件
 */
public class MailInit {

//    测试
    public static void main(String[] args) throws GeneralSecurityException, IOException, MessagingException {
        AttachBean bean = new AttachBean ( new File ("E:\\001.png" ), "001.png", "001" );
        Mail mail = new Mail ( "yunhe5438@163.com","yunhe5438@163.com","你好","你好！<img src=\"cid:"+bean.getCid ()+"\">" );
        mail.addAttachbean ( bean );
        mail(mail);
    }

   public static void mail(@Param ( "mail" ) Mail mail) throws GeneralSecurityException, IOException, MessagingException {
       Session session = CreatSession ( "smtp.qq.com", "zhaofeiyang@qq.com", "lmrknmdiesrvdhaj" );
       send ( session,mail );
   }
    /**
     * 发送邮件
     * @param session
     * @param mail
     * @throws MessagingException
     * @throws IOException
     */
    public static void send(Session session, final Mail mail) throws MessagingException, IOException {
        //3.创建session 对象，与邮件服务器建立连接
        session.setDebug(true);

        //4. 创建邮件对象
        MimeMessage msg = new MimeMessage (session);
        //5. 设置 发件人、收件人、标题、内容
        msg.setFrom(new InternetAddress (mail.getFrom ()));  //发件人必须和 当前发送账号保持一致
        msg.addRecipients( Message.RecipientType.TO, mail.getToAddress ());// 设置收件人
        msg.setSubject(mail.getSubjet ());  //邮件标题
        String ccAddress = mail.getCcAddress ();//获取抄送人
        msg.setSentDate ( new Date (  ) );//发送时间
        if (!ccAddress.isEmpty ()){
            msg.addRecipients( Message.RecipientType.CC, ccAddress);// 设置抄送人
        }
        String bccAddress = mail.getBccAddress ();//获取密送人
        if (!bccAddress.isEmpty ()){
            msg.addRecipients ( Message.RecipientType.BCC,bccAddress );//设置密送人
        }
//        添加文本内容
        MimeMultipart parts = new MimeMultipart ();//创建附件集对象
        MimeBodyPart part = new MimeBodyPart ();//创建一个存放文本的内容
        part.setContent ( mail.getContent (),"text/html;charset=utf-8" );
        parts.addBodyPart ( part );//把附件添加到附件集
//        添加附件
        List<AttachBean> list = mail.getList ();//获取所有的附件信息
        if (list!=null){
            for (AttachBean bean:list){
                MimeBodyPart Attachpart = new MimeBodyPart ();
                Attachpart.attachFile ( bean.getFile ());//设置附件文件
                Attachpart.setFileName ( bean.getFileName ());//设置附件文件名
                String cid = bean.getCid ();
                if (cid!=null){
                    Attachpart.setContentID ( cid);//设置cid
                }
                parts.addBodyPart ( Attachpart );
            }
        }
        msg.setContent ( parts );//把附件集添加到邮件对象中
        Transport.send(msg);//发送邮件

    }




    /**
     * 获取session
     * @param host  发送邮件类型
     * @param userName //邮箱账号
     * @param passord   //授权码
     * @return
     * @throws GeneralSecurityException
     */
    public static Session CreatSession(String host, final String userName, final String passord) throws GeneralSecurityException {
        // 1.创建参数配置
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", host);     // 指定主机
        props.setProperty("mail.smtp.auth", "true"); // 指定验证为true
        //SSL
        MailSSLSocketFactory sf = new MailSSLSocketFactory ();
        sf.setTrustAllHosts(true);
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.ssl.socketFactory", sf);

        //2.创建验证器
        Authenticator auth = new Authenticator () {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication (userName, passord);  //传入邮箱账号、密码
            }
        };//匿名函数，一个语句后加分号
        return Session.getDefaultInstance(props, auth);
    }
}
