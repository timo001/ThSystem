package com.yunhe.tianhe.service.premission.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.dao.permission.ResourceDao;
import com.yunhe.tianhe.entity.premission.Resource;
import com.yunhe.tianhe.service.premission.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/29 0029 19:50
 * @Description: ${todo}(用一句话描述该文件做什么)
 */
@Service
public class ResourceServiceImpl implements ResourceService {

    @Autowired
    private ResourceDao resourceDao;
    @Override
    public void save(Resource resource) {
        resourceDao.save ( resource );
    }

    @Override
    public void update(Resource resource) {
        resourceDao.update ( resource );
    }

    @Override
    public void deleted(String ids) {
        String[] split = ids.split ( "," );
        for (String premisid:split){
            resourceDao.deleted ( new Long(premisid) );
        }

    }

    @Override
    public Resource find(Long permisid) {
        Resource resource = resourceDao.find ( permisid );
        return resource;
    }

    @Override
    public List<Resource> findlist(Resource resource) {
        return resourceDao.findlist (resource);
    }

    @Override
    public Pagedao<Resource> findpage(Resource resource,int offset, int limit) {
        Pagedao<Resource> pagedao = new Pagedao<> ();
        List<Resource> findpage = resourceDao.findpage ( resource,offset, limit );
        pagedao.setRows ( findpage );
        return pagedao;
    }
}
