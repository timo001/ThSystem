<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/22 0022
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">

</head>
<body>
<div class="box-padding-15 ">
     <span class="layui-breadcrumb" lay-separator=">">
        <a href="/index" target="_top">首页</a>
        <a href="/system/level/list">会员级别列表</a>
        <a><cite>编辑</cite></a>
    </span>
    <div class="layui-row">
        <div class="layui-col-md4 layui-col-md-offset3">
            <div class="grid-demo grid-demo-bg1">
                <form class="layui-form"  method="post">
                    <div class="layui-form-item">
                        <label class="layui-form-label">级别名称</label>
                        <div class="layui-input-block">
                            <input type="hidden" name="levelid" value="${member.levelid}">
                            <input type="text" name="levelname" value="${member.levelname}"  lay-verify="required" placeholder="请输入门店级别名称" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">最小信用</label>
                        <div class="layui-input-block">
                            <input type="text" name="minscore"id="incity" value="${member.minscore}"  lay-verify="required" placeholder="请输入最小信用" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">最大信用</label>
                        <div class="layui-input-block">
                            <input type="text" name="maxscore"id="address" value="${member.maxscore}"  lay-verify="required" placeholder="请输入最大信用" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">租车折扣</label>
                        <div class="layui-input-block">
                            <input type="text" name="discount" value="${member.discount}"  lay-verify="required" placeholder="请输入租车折扣" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                            <a href="/system/level/list"><button type="button" class="layui-btn layui-btn-primary">取消</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/system/member/edit.js"></script>
</body>
</html>
