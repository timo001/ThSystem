/**
 * Created by Administrator on 2017/9/8.
 */

//JavaScript代码区域
layui.use(['layer','element'], function(){
    var element = layui.element;
    var layer = layui.layer;
    //修改密码
    $("#uppwd").on("click",function () {
        layer.open({
            type: 2,
            title: false,
            closeBtn: 1, //显示关闭按钮
            shade: [0.5],//透明度
            area: ['800px', '800px'],
            anim: 0,//弹出方式
            content: ['/user/changepwd', 'no'], //iframe的url，no代表不显示滚动条
        })
    })

//    退出
    $("#exit").click(function () {
        layer.confirm("请确认是否退出?",function () {
            location.href="/exit"
        })
    })
});

