package com.yunhe.tianhe.service.rent.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.dao.rent.BookDao;
import com.yunhe.tianhe.entity.cars.RentOrder;
import com.yunhe.tianhe.service.rent.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/12/1 0001 上午 11:14
 * @Description: TODO
 */
@Service
public class BookServiceImpl implements BookService{
    @Autowired
    private BookDao bookDao;

    @Override
    public Pagedao<RentOrder> findPage(Integer offset, Integer limit, String searchCode) {
        Pagedao<RentOrder> pagedao = new Pagedao<>();
        Integer count = bookDao.findCount(searchCode);
        List<RentOrder> data = null;
        if(count > 0){
            pagedao.setPage(offset);
            pagedao.setSize(limit);
            data = bookDao.findAll(offset,limit,searchCode);
        }
        pagedao.setTotal(count);
        pagedao.setRows(data);
        return pagedao;
    }
}
