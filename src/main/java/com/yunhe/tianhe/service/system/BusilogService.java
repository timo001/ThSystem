package com.yunhe.tianhe.service.system;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.system.Busilog;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 10:53
 * @Description:
 */
public interface BusilogService {
    /**
     * 添加
     * @param busilog
     */
    void save(Busilog busilog);

    /**
     * 单个查找
     * @param logid
     * @return
     */
    Busilog find(Long logid);

    /**
     * 查找所有
     * @return
     */
    List<Busilog> findlist();

    /**
     * 查找所有
     * @return
     */
    Pagedao<Busilog> findpage( Busilog busilog,int offset, int limit);
}
