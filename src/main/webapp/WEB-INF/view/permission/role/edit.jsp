<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/22 0022
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>个人信息设置</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/js/plugins/zTree_v3/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="box-padding-15 ">
    <span class="layui-breadcrumb" lay-separator=">">
        <a href="/" target="_top">首页</a>
        <a href="/permi/role/list">角色管理</a>
        <a><cite>编辑</cite></a>
    </span>
    <div class="layui-row">
        <div class="layui-col-md4 layui-col-md-offset3">
            <div class="grid-demo grid-demo-bg1">
                <form class="layui-form"  method="post">
                    <div class="layui-col-md6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">角色名称</label>
                            <div class="layui-input-block">
                                <input type="hidden" name="roleid" value="${role.roleid}">
                                <input type="text" name="rolename" value="${role.rolename}" lay-verify="required" autocomplete="off" placeholder="请输入角色名称"
                                       class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">状态</label>
                            <div class="layui-input-block">
                                <input type="checkbox" value="1" name="isabled" <c:if test="${role.isabled eq 1}">checked</c:if> title="启用">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label class="layui-form-label">角色描述</label>
                            <div class="layui-input-block">
                                <input type="text" name="decrib"value="${role.decrib}" lay-verify="required" autocomplete="off" placeholder="请输入角色描述"
                                       class="layui-input">
                            </div>
                        </div>
                    </div>
                    <div class="layui-col-md6">
                        <div class="layui-form-item">
                            <label class="layui-form-label">权限</label>
                            <div class="layui-input-block">
                                <input type="hidden" name="permisid" id="permisid">
                                <ul id="treeRole" class="ztree"></ul>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script type="text/javascript" src="/static/js/plugins/zTree_v3/js/jquery.ztree.core.js"></script>
<script type="text/javascript" src="/static/js/plugins/zTree_v3/js/jquery.ztree.excheck.js"></script>
<script src="/static/js/premi/role/edit.js"></script>
</body>
</html>
