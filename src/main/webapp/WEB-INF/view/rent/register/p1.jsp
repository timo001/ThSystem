<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/30 0030
  Time: 上午 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登记列表</title>
    <link rel="stylesheet" href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="/static/js/plugins/bootstrap-table/bootstrap-table.css">
    <link rel="stylesheet" href="/static/css/common.css">
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
</head>
<body>
<div class="layui-box box-padding-15">
<span class="layui-breadcrumb">
    <a href="/" target="_parent">首页</a>
                <a href="/insurance/insurancelist">租车登记</a>
</span>
    <hr>
    <form class="layui-form" id="form" action="/register/add">
        <div class="layui-form-item">
            <label class="layui-form-label">客户手机</label>
            <div class="layui-input-inline">
                <input name="mobile"   placeholder="请输入手机号" id="input" autocomplete="off" class="layui-input input-zdy">
            </div>
                <input name="msg" value="" disabled id="msg" class="layui-input layui-input-inline layui-hide">
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">客户姓名</label>
            <div class="layui-input-inline">
                <input name="name"  placeholder="请输入姓名"  autocomplete="off" class="layui-input input-zdy">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">身份证号</label>
            <div class="layui-input-inline">
                <input name="idcard"  placeholder="请输入身份证号"  autocomplete="off" class="layui-input input-zdy">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">驾驶证号</label>
            <div class="layui-input-inline">
                <input name="driverNum"  placeholder="请输入驾驶证号"  autocomplete="off" class="layui-input input-zdy">
            </div>
        </div>
    <div class="layui-form-item">
        <label class="layui-form-label">我是企业</label>
        <div class="layui-input-inline">
            <input type="checkbox"  title="企业" id="checkbox">
        </div>
    </div>
    <div class="layui-hide" id="hide">
        <div class="layui-upload" id="div">
            <label class="layui-form-label">营业执照</label>
            <button type="button" class="layui-btn" id="test1">点击上传</button>
            <div class="layui-upload-list">
                <img class="layui-upload-img" id="demo1" style="width: 300px">
                <input type="hidden" name="busiLicense" id="busiLicense">
                <p id="demoText"></p>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">机构代码</label>
            <div class="layui-input-inline">
                <input name="orgCode"  placeholder="请输入机构代码"  autocomplete="off" class="layui-input input-zdy">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">税务证号</label>
            <div class="layui-input-inline">
                <input name="taxCertNo"  placeholder="请输入税务证号"  autocomplete="off" class="layui-input input-zdy">
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">企业备注</label>
            <div class="layui-input-inline">
                <textarea name="remark" placeholder="请输入企业备注" class="layui-textarea"></textarea>
            </div>
        </div>

    </div>
        <div class="layui-form-item">
            <button class="layui-btn" id="button" lay-submit lay-filter="formSubmit">下一步</button>
                <button type="button" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </form>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script>
    layui.use(['element', 'layer', 'form', 'laydate', 'upload'], function () {
        var form = layui.form,
            laydate = layui.laydate
            , upload = layui.upload
            , element = layui.element,
            layer = layui.layer;

        var uploadInst = upload.render({
            elem: '#test1'
            , url: '/upload'
            , before: function (obj) {
                //预读本地文件示例，不支持ie8
                obj.preview(function (index, file, result) {
                    $('#demo1').attr('src', result); //图片链接（base64）
                });
            }
            , done: function (res) {
                $("#busiLicense").val(res.data)    //将返回回来的时间戳组成的名字存入隐藏域中
                //如果上传失败
                if (res.code != 1) {
                    return layer.msg('上传失败');
                }
                //上传成功
            }
            , error: function () {
                //演示失败状态，并实现重传
                var demoText = $('#demoText');
                demoText.html('<span style="color: #FF5722;">上传失败</span> <a class="layui-btn layui-btn-mini demo-reload">重试</a>');
                demoText.find('.demo-reload').on('click', function () {
                    uploadInst.upload();
                });
            }
        });

       form.on("checkbox",function () {
            $("#hide").toggleClass("layui-hide")
        })

        $("#input").on("blur",function (e,a) {
            var phone = $("#input").val();
            $.ajax({
                url:'/register/phone',
                type:'get',
                data:{phone:phone},
                dataType:"json",
                sussecc:function () {
                    debugger
                },error:function () {
                    debugger
                }
            })
        })



    });
</script>
</body>
</html>
