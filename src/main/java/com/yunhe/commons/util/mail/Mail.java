package com.yunhe.commons.util.mail;

import java.util.ArrayList;
import java.util.List;

/**
 *邮件类，你需要设置：账户名和密码、收件人、抄送(可选)、暗送(可选)、主题、内容，以及附件(可选)
 *
 * 在创建了Mail对象之后
 * 可以调用它的setSubject()、setContent()，设置主题和正文
 * 也可以调用setFrom()和　addToAddress()，设置发件人，和添加收件人。
 * 也可以调用addAttch()添加附件
 * 创建AttachBean：new AttachBean(new File("..."), "fileName");
 */
public class Mail {
    private String from;//发件人
    private StringBuilder toAddress = new StringBuilder (  );//收件人，多个，可扩充
    private StringBuilder ccAddress = new StringBuilder (  );//抄送，多个，可扩充
    private StringBuilder bccAddress = new StringBuilder (  );//暗送，多个，可扩充

    private  String subjet; //主题
    private  String content; //正文

//    创建附件列表
       private List<AttachBean> list=new ArrayList<> ();
    public Mail() {}

    public Mail(String from){
        this(from,null,null,null);
    }

    public Mail(String from,String to){
        this(from,to,null,null);
    }
    /**
     * 只进行发送
     * @param from
     */
    public Mail(String from, String toAddress, String subjet, String content) {
        this.from = from;
        this.toAddress.append ( toAddress ) ;
        this.subjet = subjet;
        this.content = content;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getToAddress() {
        return toAddress.toString ();
    }
    public String getCcAddress() {
        return ccAddress.toString ();
    }
    public String getBccAddress() {
        return bccAddress.toString ();
    }

    public String getSubjet() {
        return subjet;
    }

    public void setSubjet(String subjet) {
        this.subjet = subjet;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 添加收件人
     * @param to
     */
    public void addToAddress(String to){
        if (this.toAddress.length ()>0){
            this.toAddress.append ( "," );
        }
        this.toAddress.append ( to );
    }

    /**
     * 添加抄送人
     * @param cc
     */
    public void addCcAddress(String cc){
        if (this.ccAddress.length ()>0){
            this.ccAddress.append ( "," );
        }
        this.ccAddress.append ( cc );
    }

    /**
     * 添加密送人
     * @param bcc
     */
    public void addBccAddress(String bcc){
        if (this.bccAddress.length ()>0){
            this.bccAddress.append ( "," );
        }
        this.bccAddress.append ( bcc );
    }

    /**
     * 添加附件
     * @param attachBean
     */
    public void addAttachbean(AttachBean attachBean){
        this.list.add ( attachBean );
    }
    public List<AttachBean> getList() {
        return list;
    }
}
