<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/24 0024
  Time: 下午 3:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>车辆自用记录列表</title>
    <meta name="viewport" content="width=device-width" />
    <link href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/static/js/plugins/bootstrap-table/bootstrap-table.css" rel="stylesheet" />
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="layui-box box-padding-15">
        <span class="layui-breadcrumb">
          <a href="#">首页</a>
          <a href="javascript:;">车辆自用记录</a>
          <a><cite>列表</cite></a>
        </span>
    <hr>
    <div class="layui-row layui-col-space30">
        <div class="layui-col-md3">
            <div class="layui-btn-group">
                <button class="layui-btn" id="addBtn">增加</button>
                <button class="layui-btn" id="editBtn">编辑</button>
                <button class="layui-btn" id="delBtn">删除</button>
                <%--<c:forEach items="${buttons}" var="btn">
            <button id="${btn.btnId}" data-url="${btn.uri}" class="layui-btn layui-btn-primary btn-add">${btn.name}</button>
        </c:forEach>--%>
            </div>
        </div>
        <div class="layui-col-md4">
            <input type="text" id="searchCode"  placeholder="请输入要查询的车牌号" autocomplete="off"  class="layui-input">
        </div>
        <div class="layui-col-md2">
            <button class="layui-btn" id="queryBtn">查询</button>
        </div>
        <div class="layui-col-md3">

        </div>
    </div>
    <div class=" box-table-h7">
    <table id="tb_page"></table>
    </div>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>

<script>
    layui.use(['element','layer'], function(){
        var layer = layui.layer;
        /*var btnEvent = layui.btnEvent;*/
        //定义表格
        var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
        $('#tb_page').bootstrapTable({
            url: '/selfuse/listData',         //请求后台的URL（*）
            method: 'get',                      //请求方式（*）
            toolbar: '#toolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: true,                   //是否显示分页（*）
            sortable: false,                     //是否启用排序
            sortOrder: "asc",                   //排序方式
            sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber:1,                       //初始化加载第一页，默认第一页
            pageSize: 10,                       //每页的记录行数（*）
            pageList: [15, 30, 50],        //可供选择的每页的行数（*）
            search: false,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            strictSearch: true,
            showColumns: false,                  //是否显示所有的列
            showRefresh: true,                  //是否显示刷新按钮
            minimumCountColumns: 2,             //最少允许的列数
            clickToSelect: true,                //是否启用点击选中行
            height: '700',                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: "recordId",                     //每一行的唯一标识，一般为主键列
            showToggle:false,                    //是否显示详细视图和列表视图的切换按钮
            cardView: false,                    //是否显示详细视图
            detailView: false,                   //是否显示父子表
            columns: [{
                checkbox: true
            }, {
                field: 'carCode',
                title: '车牌号',
            }, {
                field: 'useCarTime',
                title: '出车时间',
                formatter:function (value, row, index) {
                    if(isNaN(value)){
                        return "--";
                    } else {
                        var time = new Date(value);
                        return time.toLocaleDateString().replace("/","-").replace("/","-");
                    }
                }
            }, {
                    field: 'returnTime',
                    title: '还车时间',
                    formatter:function (value, row, index) {
                        if(isNaN(value)){
                            return "--";
                        } else {
                            var time = new Date(value);
                            return time.toLocaleDateString().replace("/","-").replace("/","-");
                        }
                    }
                },{
                field: 'noticeItem',
                title: '使用事由',
                formatter: function (value, row, index) {
                    var rtVal = "";
                    switch (value) {
                        case "1":
                            rtVal = "维修";
                            break;
                        case "2":
                            rtVal = "年检";
                            break;
                        case "3":
                            rtVal = "私用";
                            break;
                    }
                    return rtVal;
                }
            }, {
                field: 'headMan',
                title: '经办人员',
            }, {
                    field: 'headMobile',
                    title: '经办人手机',
                },]

        });

        //绑定添加事件
        $("#addBtn").on('click',function (e) {
            location.href="/selfuse/add";
        })

        //绑定编辑事件
        $("#editBtn").on('click',function(e){
            //1.要想获取主子表所有选中行，首先得拿到所有的表格对象（给所有的table 起相同的标识）
            var $tables = $('table[id ^= tb_page]');  //传入方法名调用该方法
            var rows = [];    //表示所有选中行的记录
            //2.遍历所有的表格对象，将其选中行，合并到一个数组中去
            $tables.each(function(){
                var selections = $(this).bootstrapTable('getSelections');
                if(selections.length > 0) {
                    $.merge(rows,selections);  //把 selections 数组合并到  rows中
                }
            });

            if(rows.length < 1) {
                layer.msg("请勾选一条数据");
            }else if(rows.length > 1) {
                layer.msg("只能勾选一条数据");
            }else {
                var id = rows[0].recordId;
                location.href = "/selfuse/edit?recordId=" + id;
            }
        })

        $("#delBtn").on('click',function(){

            //1.要想获取主子表所有选中行，首先得拿到所有的表格对象（给所有的table 起相同的标识）
            var $tables = $('table[id ^= tb_page]');  //传入方法名调用该方法
            var rows = [];    //表示所有选中行的记录
            //2.遍历所有的表格对象，将其选中行，合并到一个数组中去
            $tables.each(function(){
                var selections = $(this).bootstrapTable('getSelections');
                if(selections.length > 0) {
                    $.merge(rows,selections);  //把 selections 数组合并到  rows中
                }
            });

            var rowIds = [];
            $(rows).each(function(){
                var id = this.recordId;
                rowIds.push(id);
            });
            //3.异步请求后台删除，所选行记录
            if(rows.length < 1) {
                layer.msg("请勾选至少一条数据");
            }else {
                layer.confirm("确定删除所选数据吗？",function (index) {
                    //执行删除操作
                    $.ajax({
                        url: "/selfuse/delete",
                        type: "post",
                        dataType: 'json',
                        data: {ids : rowIds.join(",")},
                        success:function(msg){
                            layer.msg(msg.message);
                            if(msg.code == 1) {
                                $("#tb_page").bootstrapTable("refresh");  //刷新表格数据
                            }
                        },
                        error:function(e){
                            layer.msg("服务器忙，请稍后再试");
                        }
                    });
                });
            }
        });
        /* btnEvent.bound($('#tb_page'),"addBtn","editBtn","delBtn");   //绑定通用事件*/
        $("#queryBtn").on('click',function () {
            var searchCode = $("#searchCode").val();
            $('#tb_page').bootstrapTable("refresh",{
                url: '/selfuse/listData',
                query:{
                    searchCode:searchCode
                }
            });
            $("#searchCode").val("");
        })
    })
</script>
</body>
</html>
