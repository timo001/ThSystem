package com.yunhe.tianhe.entity.cars;

import com.yunhe.tianhe.entity.client.ClientUser;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/25 0025 下午 12:34
 * @Description: TODO
 */

public class RentOrder {

    private Long orderId;  //订单编号

    private String carCode;  //车牌号

    private String mobile;   //客户手机

    private Long clientId;  //客户编号

    private String client;   //客户

    private Double shouldRent;  //租赁费用

    private Double shouldBasic;  //基本保险

    private Double shouldDeposit;  //车辆押金

    private Double shouldIllegal;  //违章押金

    private Double realTotal;  //实际总额

    private Double returnTimeout;  //扣除超时

    private Double returnDamage;  //扣除车损

    private Double returnIllegal;  //扣除违章

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date readyBtTime;  //预计取车时间
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date readyEtTime;  //预计还车时间

    private String btRentDept;  //取车门店

    private String etRentDept;  //还车门店
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date realBtTime;  //实际取车时间
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date realEtTime;  //实际还车时间

    private Double realBtOil;  //取车油量

    private Double realEtOil;  //还车油量

    private Double realBtMile;  //取车里程

    private Double realEtMile;  //还车里程

    private Integer payWay;  //支付方式
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date payTime;  //支付时间
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date addTime;  //添加时间

    private Integer orderState;  //订单状态


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getCarCode() {
        return carCode;
    }

    public void setCarCode(String carCode) {
        this.carCode = carCode;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Double getShouldRent() {
        return shouldRent;
    }

    public void setShouldRent(Double shouldRent) {
        this.shouldRent = shouldRent;
    }

    public Double getShouldBasic() {
        return shouldBasic;
    }

    public void setShouldBasic(Double shouldBasic) {
        this.shouldBasic = shouldBasic;
    }

    public Double getShouldDeposit() {
        return shouldDeposit;
    }

    public void setShouldDeposit(Double shouldDeposit) {
        this.shouldDeposit = shouldDeposit;
    }

    public Double getShouldIllegal() {
        return shouldIllegal;
    }

    public void setShouldIllegal(Double shouldIllegal) {
        this.shouldIllegal = shouldIllegal;
    }

    public Double getRealTotal() {
        return realTotal;
    }

    public void setRealTotal(Double realTotal) {
        this.realTotal = realTotal;
    }

    public Double getReturnTimeout() {
        return returnTimeout;
    }

    public void setReturnTimeout(Double returnTimeout) {
        this.returnTimeout = returnTimeout;
    }

    public Double getReturnDamage() {
        return returnDamage;
    }

    public void setReturnDamage(Double returnDamage) {
        this.returnDamage = returnDamage;
    }

    public Double getReturnIllegal() {
        return returnIllegal;
    }

    public void setReturnIllegal(Double returnIllegal) {
        this.returnIllegal = returnIllegal;
    }

    public Date getReadyBtTime() {
        return readyBtTime;
    }

    public void setReadyBtTime(Date readyBtTime) {
        this.readyBtTime = readyBtTime;
    }

    public Date getReadyEtTime() {
        return readyEtTime;
    }

    public void setReadyEtTime(Date readyEtTime) {
        this.readyEtTime = readyEtTime;
    }

    public String getBtRentDept() {
        return btRentDept;
    }

    public void setBtRentDept(String btRentDept) {
        this.btRentDept = btRentDept;
    }

    public String getEtRentDept() {
        return etRentDept;
    }

    public void setEtRentDept(String etRentDept) {
        this.etRentDept = etRentDept;
    }

    public Date getRealBtTime() {
        return realBtTime;
    }

    public void setRealBtTime(Date realBtTime) {
        this.realBtTime = realBtTime;
    }

    public Date getRealEtTime() {
        return realEtTime;
    }

    public void setRealEtTime(Date realEtTime) {
        this.realEtTime = realEtTime;
    }

    public Double getRealBtOil() {
        return realBtOil;
    }

    public void setRealBtOil(Double realBtOil) {
        this.realBtOil = realBtOil;
    }

    public Double getRealEtOil() {
        return realEtOil;
    }

    public void setRealEtOil(Double realEtOil) {
        this.realEtOil = realEtOil;
    }

    public Double getRealBtMile() {
        return realBtMile;
    }

    public void setRealBtMile(Double realBtMile) {
        this.realBtMile = realBtMile;
    }

    public Double getRealEtMile() {
        return realEtMile;
    }

    public void setRealEtMile(Double realEtMile) {
        this.realEtMile = realEtMile;
    }

    public Integer getPayWay() {
        return payWay;
    }

    public void setPayWay(Integer payWay) {
        this.payWay = payWay;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Integer getOrderState() {
        return orderState;
    }

    public void setOrderState(Integer orderState) {
        this.orderState = orderState;
    }

    @Override
    public String toString() {
        return "RentOrder{" +
                "orderId=" + orderId +
                ", carCode='" + carCode + '\'' +
                ", clientId=" + clientId +
                ", client=" + client +
                ", shouldRent=" + shouldRent +
                ", shouldBasic=" + shouldBasic +
                ", shouldDeposit=" + shouldDeposit +
                ", shouldIllegal=" + shouldIllegal +
                ", realTotal=" + realTotal +
                ", returnTimeout=" + returnTimeout +
                ", returnDamage=" + returnDamage +
                ", returnIllegal=" + returnIllegal +
                ", readyBtTime=" + readyBtTime +
                ", readyEtTime=" + readyEtTime +
                ", btRentDept='" + btRentDept + '\'' +
                ", etRentDept='" + etRentDept + '\'' +
                ", realBtTime=" + realBtTime +
                ", realEtTime=" + realEtTime +
                ", realBtOil=" + realBtOil +
                ", realEtOil=" + realEtOil +
                ", realBtMile=" + realBtMile +
                ", realEtMile=" + realEtMile +
                ", payWay=" + payWay +
                ", payTime=" + payTime +
                ", addTime=" + addTime +
                ", orderState=" + orderState +
                '}';
    }
}
