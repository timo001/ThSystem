package com.yunhe.tianhe.service.cars;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.RentOrder;
import org.apache.ibatis.annotations.Param;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/29 0029 上午 9:27
 * @Description TODO
 */
public interface RentOrderService<E> {
    Pagedao<RentOrder> list(@Param("offset") Integer offset, @Param("limit") Integer limit, @Param("searchCode")String searchCode);      //分页
    void delete(String ids);        //删除
    void save(RentOrder rentOrder);   //保存
}
