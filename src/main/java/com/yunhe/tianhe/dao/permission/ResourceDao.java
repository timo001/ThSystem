package com.yunhe.tianhe.dao.permission;

import com.yunhe.tianhe.entity.premission.Resource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/29 0029 17:32
 * @Description: 权限操作
 */
public interface ResourceDao {

    void save(Resource resource);

    void update(Resource resource);

    void deleted(Long premisid);

    Resource find(Long permisid);

    List<Resource> findlist(Resource resource);

    List<Resource> findpage(Resource resource, @Param ( "offset" ) int offset, @Param ( "limit" ) int limit);
}

