package com.yunhe.tianhe.dao.system;

import com.yunhe.tianhe.entity.system.Memberlevel;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 13:36
 * @Description: 会员级别
 */
public interface MemberlevelDao {


    /**
     * 添加
     * @param memberlevel
     */
    void save(Memberlevel memberlevel);

    /**
     * 删除
     * @param levelid
     */
    void deleted(Long levelid);

    /**
     * 修改
     * @param memberlevel
     */
    void update(Memberlevel memberlevel);

    /**
     * 单个查找
     * @param levelid
     * @return
     */
    Memberlevel find(Long levelid);

    /**
     * 查找
     * @return
     */
    List<Memberlevel> findlist();

    /**
     * 分页
     * @return
     */
    Integer count();

    /**
     * 分页
     * @param offset
     * @param limit
     * @return
     */
    List<Memberlevel> findpage(@Param ( "offset" ) int offset,@Param ( "limit" ) int limit);

}
