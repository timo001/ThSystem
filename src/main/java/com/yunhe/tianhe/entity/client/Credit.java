package com.yunhe.tianhe.entity.client;

import java.util.Date;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/23 0023 上午 10:52
 * @Description: 客户信用积分的实体类
 */
public class Credit {

    private Long cId;//用户id
    private String creditChange;//积分变化情况
    private String changeReason;//变化原因
    private Date addTime;//系统时间
    private ClientUser clientUser;//客户实体

    public Long getcId() {
        return cId;
    }

    public void setcId(Long cId) {
        this.cId = cId;
    }

    public String getCreditChange() {
        return creditChange;
    }

    public void setCreditChange(String creditChange) {
        this.creditChange = creditChange;
    }

    public String getChangeReason() {
        return changeReason;
    }

    public void setChangeReason(String changeReason) {
        this.changeReason = changeReason;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public ClientUser getClientUser() {
        return clientUser;
    }

    public void setClientUser(ClientUser clientUser) {
        this.clientUser = clientUser;
    }
}
