package com.yunhe.tianhe.service.client.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.util.StringUtils;


import com.yunhe.tianhe.dao.client.CreditDao;
import com.yunhe.tianhe.entity.client.Credit;
import com.yunhe.tianhe.service.client.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServlet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/22 0022 下午 8:03
 * @Description: TODO
 */
@Service
public class CreditServiceImpl implements CreditService {



    @Autowired
    private CreditDao creditDao;
    @Override
    public void save(Credit credit) {
        creditDao.save(credit);
    }

    @Override
    public Pagedao<Credit> findPage(Integer offset, Integer limit,String idcard,String mobile) {
        Pagedao<Credit> pagedao = new Pagedao<>();
        List<Credit> data = new ArrayList<>();
        Integer count = 0;
        count = creditDao.findCount( idcard, mobile);
/*        if(!StringUtils.notEmpty(name)) name = null;
        if(!StringUtils.notEmpty(idcard)) idcard = null;
        if(!StringUtils.notEmpty(mobile)) mobile = null;*/
        data = creditDao.findList( offset, limit,idcard, mobile);

        pagedao.setTotal(count );
        pagedao.setRows(data);
        return pagedao;
    }

    @Override
    public void delete(Long id) {
        creditDao.delete(id);
    }

    @Override
    public void deleteBatch(String ids) {
        if(StringUtils.notEmpty(ids)){
            String[] idArr = ids.split(",");
            for(String id : idArr) {
                this.delete(Long.valueOf(id));
            }
        }
    }


}
