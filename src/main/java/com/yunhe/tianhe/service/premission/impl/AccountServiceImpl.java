package com.yunhe.tianhe.service.premission.impl;

import com.yunhe.commons.dto.Pagedao;

import com.yunhe.tianhe.dao.permission.AccountDao;

import com.yunhe.tianhe.entity.premission.Account;
import com.yunhe.tianhe.service.premission.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/27 0027 20:27
 * @Description: 实现AccountService接口
 */
@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    private AccountDao accountDao;
    @Override
    public void save(Account account) {
        accountDao.save ( account );
    }

    @Override
    public void update(Account account) {
        accountDao.update ( account );
    }

    @Override
    public void deleted(String ids) {
        String[] split = ids.split ( "," );
        for (String uid:split){
            accountDao.deleted ( new Long(uid) );
        }
    }

    @Override
    public Account find(Long uid) {
        return accountDao.find ( uid );
    }

    @Override
    public List<Account> findlist() {
        return accountDao.findlist ();
    }

    @Override
    public Pagedao findpage(Account account,int offset,int limit) {
        Pagedao<Account> pagedao = new Pagedao<> ();
        int count = accountDao.count ();
        List<Account> findpage=null;
        if (count>0){
             findpage = accountDao.findpage ( account, offset, limit );
             pagedao.setSize ( limit );
             pagedao.setPage ( offset/limit+1 );
        }
        pagedao.setRows ( findpage );
        pagedao.setTotal ( count );
        return pagedao;
    }
}
