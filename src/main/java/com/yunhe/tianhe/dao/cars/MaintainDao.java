package com.yunhe.tianhe.dao.cars;

import com.yunhe.tianhe.entity.cars.Maintain;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/25 0025 下午 1:41
 * @Description TODO
 */
public interface MaintainDao {
    //分页
    List<Maintain> list(@Param("limit") Integer limit,@Param("offset") Integer offset,@Param("carcode")String carcode);
    //统计（数量）
    int pagedao(String carcode);
    //删除
    void delete(Integer id);
    //添加
    void save(Maintain maintain);
    //查询为修改做准备
    Maintain edit(Long recordid);
    //修改
    void update(Maintain maintain);
}
