package com.yunhe.tianhe.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/22 0022 19:52
 * @Description: 主页控制语句
 */
@Controller
@RequestMapping("/")
public class LoginController {

    @RequestMapping
    public String index(){
        return "index";
    }
}
