package com.yunhe.commons.util;





import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;



/**
 * 获取配置文件里的信息，添加属性到配置文件里
 * 添加日志信息
 */
public class Propertiesinit {
    private final static Logger logger = LoggerFactory.getLogger(Properties.class); //日志

    /**
     * 根据文件名获取Properties对象
     * @param fileNmae
     * @return
     */
      public static Properties read(String fileNmae){
          Reader reader;
          Properties properties = new Properties ();
          reader = new InputStreamReader ( Propertiesinit.class.getClassLoader ().getResourceAsStream ( fileNmae ) );
          try {
              properties.load ( reader );
              return properties;
          } catch (IOException e) {
              e.printStackTrace ();
          }finally {
              if (reader!=null){
                  try {
                      reader.close ();
                  } catch (IOException e) {
                      e.printStackTrace ();
                  }
              }
          }
        return null;
      }

    /**
     * 根据文件名和key值获取去val值
     * @param fileName
     * @param key
     * @return
     */
      public static String readVal(String fileName,String key){
          Properties read = read ( fileName );
          String property = read.getProperty ( key );
          if (read!=null){
              return property;
          }
          return null;
      }

    /**
     * 根据key值获取value
     * @param properties
     * @param key
     * @return
     */
      public static String readVal(Properties properties,String key){
          if (properties!=null){
              return properties.getProperty ( key );
          }
          return null;
      }

    /**
     * 添加单个属性
     * @param fileName
     * @param key
     * @param value
     */
    public static void write(String fileName,String key,String value){
        Map<String, String> map = new HashMap<> ();
        map.put ( key,value );
        writeValues(fileName,map);
    }

    private static void writeValues(String fileName, Map<String, String> map) {
        InputStream input=null;
        OutputStream output=null;
        try {
            input = Propertiesinit.class.getClassLoader ().getResourceAsStream ( fileName );
            Properties properties = new Properties ();
            properties.load ( input );
            String resource = Propertiesinit.class.getResource ( "/" + fileName ).getPath ();
            output = new FileOutputStream ( resource );
            if (map!=null){
                Set<String> set = map.keySet ();
                for (String key:set){
                    properties.setProperty ( key,map.get(key));
                    logger.info ( "更新"+fileName+"的键("+key+")值为"+map.get ( key ) );
                }
            }
        } catch (IOException e) {
            e.printStackTrace ();
        }finally {
            try {
                if (input != null) {
                    input.close ();
                }
                if (output != null) {
                    output.flush ();
                    output.close ();
                }
            } catch (IOException e) {
                e.printStackTrace ();
            }
        }
    }
}

