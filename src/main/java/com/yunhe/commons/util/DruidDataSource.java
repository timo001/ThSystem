package com.yunhe.commons.util;

import com.alibaba.druid.pool.DruidDataSourceFactory;
import org.apache.ibatis.datasource.DataSourceFactory;

import javax.sql.DataSource;
import java.util.Properties;


/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/9 0009 15:32
 * @Description:  mybatis 中获取数据源DataSource
 */
public class DruidDataSource implements DataSourceFactory {
    private  Properties properties;
    @Override
    public void setProperties(Properties properties) {
        this.properties=properties;
    }

    @Override
    public DataSource getDataSource() {
        Properties properties = Propertiesinit.read ( "druid.properties" );
        this.properties=properties;
        DataSource dataSource=null;
        try {
            dataSource = DruidDataSourceFactory.createDataSource ( properties );
        } catch (Exception e) {
            e.printStackTrace ();
        }
        return dataSource;
    }
}
