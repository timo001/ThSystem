<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/23 0023
  Time: 下午 2:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加</title>
    <link rel="stylesheet" href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css">
    <link rel="stylesheet" href="/static/js/plugins/bootstrap-table/bootstrap-table.css">
    <link rel="stylesheet" href="/static/css/common.css">
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
</head>
<body>
<div class="layui-box box-padding-15">
<span class="layui-breadcrumb">
    <a href="/" target="_parent">首页</a>
                <a href="/insurance/insurancelist">车辆保险</a>
                <a><cite>添加</cite></a>
</span>
<hr>
<form class="layui-form" action="/insurance/save">
    <div class="layui-form-item">
        <label class="layui-form-label">车牌号</label>
        <div class="layui-input-block">
            <input name="carCode" lay-verify="required|carCode" placeholder="请输入车牌号" id="carcode" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">保险公司</label>
        <div class="layui-input-block">
            <input name="company" lay-verify="required" placeholder="请输入保险公司" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">出险电话</label>
        <div class="layui-input-block">
            <input name="companyPhone" lay-verify="required" placeholder="请输入出险电话" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">保险编号</label>
        <div class="layui-input-block">
            <input name="code" lay-verify="required" placeholder="请输入保险编号" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">保险内容</label>
        <div class="layui-input-block">
            <input  name="content" lay-verify="required" placeholder="请输入保险内容" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">保险费用</label>
        <div class="layui-input-block">
            <input name="money" lay-verify="required" placeholder="请输入保险费用" autocomplete="off" class="layui-input">
        </div>
    </div>

        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label">开始时间</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input "  id="test-limit1" placeholder="请选择开始时间" name="validdate">
                </div>
            </div>
        </div>
    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">失效时间</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" id="test-limit" placeholder="请选择失效时间" name="invaliddate">
            </div>
        </div>
    </div>



    <div class="layui-form-item">
        <label class="layui-form-label">经办人</label>
        <div class="layui-input-block">
            <input  name="contactman" lay-verify="required" placeholder="请输入经办人" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">经办电话</label>
        <div class="layui-input-block">
            <input name="contactmobile" lay-verify="required" placeholder="请输入经办电话" autocomplete="off" class="layui-input">
        </div>
    </div>

    <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">备注</label>
        <div class="layui-input-block">
            <textarea name="remark" placeholder="请输入备注" class="layui-textarea"></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="formSubmit" type="submit">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script>
    layui.use(['form','element','layer','laydate','upload'], function() {
        var form = layui.form;
        var laydate = layui.laydate;
        upload = layui.upload;


        var ins22 = laydate.render({
            elem: '#test-limit1'
            , min: '2016-10-14'
            , max: '2080-10-14',
            ready: function(){

            }
        })

        var ins22 = laydate.render({
            elem: '#test-limit'
            , min: '2016-10-14'
            , max: '2080-10-14',
            ready: function(){

            }
        })




        form.verify({
            carCode:function (value) {                      //carCode 不是name！
            var unique = false;
            $.ajax({
                url:'/cars/verifyCarCode',
                data:{carcode:value},
                dataType:'json',
                async:false,
                success:function(msg){
                   if (msg.code == 1){             //如果为1，表示该车牌存在
                       unique = msg.data
                   }
                },error:function () {
                    layer.msg("服务器错误")
                }
            });
             if (!unique){
                 return "该车牌不存在";
              }
            }
        })

    })
</script>
</body>
</html>
