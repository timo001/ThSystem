package com.yunhe.tianhe.controller.client;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.client.BacList;
import com.yunhe.tianhe.service.client.BacListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/23 0023 下午 4:36
 * @Description: TODO
 */
@Controller
@RequestMapping("/baclist")
public class BacListController {

    @Autowired
    private BacListService bacListService;

    @RequestMapping()
    public String list(){
        return "client/baclist/baclist";
    }


    @RequestMapping("/add")
    public  String add(HttpServletRequest request){
        return "/client/baclist/add";
    }

    @RequestMapping("/save")
    @ResponseBody
    public  JSONResponse save(BacList bacList){
        bacListService.save(bacList);
        return JSONResponse.success("保存成功");
    }


    /**
     * 黑名单页面
     * 黑名单列表,分页数据
     * @return
     */
    @RequestMapping("/listData")
    @ResponseBody
    public Pagedao<BacList> findPage(Integer offset, Integer limit,String name,String idcard,String mobile){
        return  bacListService.findPage(offset,limit,name,idcard,mobile);
    }

    /**
     * 删除一个黑名单用户
     * @return  跳转到列表页
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JSONResponse delete(String ids){
        bacListService.deleteBatch(ids);
        return JSONResponse.success("删除成功");
    }

    /**
     * 编辑页面
     * @return
     */
    @RequestMapping("/edit")
    public String edit(Long bid, HttpServletRequest request){
        BacList bacList = bacListService.findById(bid);
        request.setAttribute("bacList", bacList);
        return "/client/baclist/edit";
    }

    /**
     *
     * 修改黑名单
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public  JSONResponse update(HttpServletRequest request,BacList bacList){
        bacListService.update(bacList);
        return JSONResponse.success("修改成功");
    }
}
