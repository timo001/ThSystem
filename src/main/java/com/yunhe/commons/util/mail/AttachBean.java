package com.yunhe.commons.util.mail;

import java.io.File;

/**
 *附件类
 */
public class AttachBean {
    private File file;//附件路径
    private String fileName;//附件文件名
    private String cid;//相当于图片id
    public AttachBean(){}
    public AttachBean(File file,String fileName){
        this(file,fileName,null);
    }
    public AttachBean(File file, String fileName, String cid) {
        this.file = file;
        this.fileName = fileName;
        this.cid = cid;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }
}
