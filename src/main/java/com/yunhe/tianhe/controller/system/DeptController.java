package com.yunhe.tianhe.controller.system;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.system.Dept;
import com.yunhe.tianhe.service.system.DeptServive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/23 0023 09:23
 * @Description: 门店信息设置
 */
@Controller
@RequestMapping("/system/dept")
public class DeptController {
    @Autowired
    private DeptServive deptServive;

    @RequestMapping("/list")
    public String list(){
        return "system/depts/list";
    }
    @RequestMapping("/add")
    public String add(){
        return "system/depts/add";
    }

    @RequestMapping("/edit")
    public ModelAndView edit(Long deptid){
        Dept dept = deptServive.find ( deptid );
        return new ModelAndView ("system/depts/edit","dept",dept  );
    }

    @RequestMapping("/save")
    @ResponseBody
    public JSONResponse save(Dept dept){
        deptServive.save ( dept );
        return new JSONResponse (1,"保存成功！");
    }

    @RequestMapping("/update")
    @ResponseBody
    public JSONResponse update(Dept dept){
        deptServive.update ( dept );
        return new JSONResponse (1,"保存成功！");
    }

    @RequestMapping("/deleted")
    @ResponseBody
    public JSONResponse deleted(String ids){
        deptServive.deleted ( ids );
        return new JSONResponse(1,"删除成功！");
    }


    @RequestMapping("/page")
    @ResponseBody
    public Pagedao page(int offset,int limit){
        Pagedao findpage = deptServive.findpage ( offset, limit );
        return findpage;
    }
}
