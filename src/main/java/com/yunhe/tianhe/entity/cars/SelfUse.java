package com.yunhe.tianhe.entity.cars;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/24 0024 下午 3:40
 * @Description: 车辆自用
 */
public class SelfUse {
    private Long recordId;//记录编号
    private String carCode;//车牌号
    private Double initMile;//发车里程
    private Double initOil;//发车油量
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date useCarTime;//出车时间
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date returnTime;//还车时间
    private Double returnMile;//还车里程
    private Double returnOil;//还车剩余油量
    private String headMan;//经办人员
    private String headMobile;//经办人手机
    private String noticeItem;//使用事由
    private String remark;//备注信息
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date addTime;//添加时间

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getCarCode() {
        return carCode;
    }

    public void setCarCode(String carCode) {
        this.carCode = carCode;
    }

    public Double getInitMile() {
        return initMile;
    }

    public void setInitMile(Double initMile) {
        this.initMile = initMile;
    }

    public Double getInitOil() {
        return initOil;
    }

    public void setInitOil(Double initOil) {
        this.initOil = initOil;
    }

    public String getNoticeItem() {
        return noticeItem;
    }

    public void setNoticeItem(String noticeItem) {
        this.noticeItem = noticeItem;
    }

    public Date getUseCarTime() {
        return useCarTime;
    }

    public void setUseCarTime(Date useCarTime) {
        this.useCarTime = useCarTime;
    }

    public String getHeadMan() {
        return headMan;
    }

    public void setHeadMan(String headMan) {
        this.headMan = headMan;
    }

    public String getHeadMobile() {
        return headMobile;
    }

    public void setHeadMobile(String headMobile) {
        this.headMobile = headMobile;
    }

    public Date getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(Date returnTime) {
        this.returnTime = returnTime;
    }

    public Double getReturnMile() {
        return returnMile;
    }

    public void setReturnMile(Double returnMile) {
        this.returnMile = returnMile;
    }

    public Double getReturnOil() {
        return returnOil;
    }

    public void setReturnOil(Double returnOil) {
        this.returnOil = returnOil;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

}
