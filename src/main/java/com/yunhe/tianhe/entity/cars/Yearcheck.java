package com.yunhe.tianhe.entity.cars;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/27 0027 下午 4:47
 * @Description TODO
 */
public class Yearcheck {
    private Long recordid;
    private String carcode;  //
    private Double initmile;//
    private Double initoil;//
    private String address;//
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date usecartime;//
    private String headman;//
    private String headmobile;//
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date returntime;//
    private Double returnmile;//
    private Double returnoil;//
    private Double costmoney;//
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date nexttime;//
    private String remark;//
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date addtime;


    public Long getRecordid() {
        return recordid;
    }

    public void setRecordid(Long recordid) {
        this.recordid = recordid;
    }

    public String getCarcode() {
        return carcode;
    }

    public void setCarcode(String carcode) {
        this.carcode = carcode;
    }

    public Double getInitmile() {
        return initmile;
    }

    public void setInitmile(Double initmile) {
        this.initmile = initmile;
    }

    public Double getInitoil() {
        return initoil;
    }

    public void setInitoil(Double initoil) {
        this.initoil = initoil;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getUsecartime() {
        return usecartime;
    }

    public void setUsecartime(Date usecartime) {
        this.usecartime = usecartime;
    }

    public String getHeadman() {
        return headman;
    }

    public void setHeadman(String headman) {
        this.headman = headman;
    }

    public String getHeadmobile() {
        return headmobile;
    }

    public void setHeadmobile(String headmobile) {
        this.headmobile = headmobile;
    }

    public Date getReturntime() {
        return returntime;
    }

    public void setReturntime(Date returntime) {
        this.returntime = returntime;
    }

    public Double getReturnmile() {
        return returnmile;
    }

    public void setReturnmile(Double returnmile) {
        this.returnmile = returnmile;
    }

    public Double getReturnoil() {
        return returnoil;
    }

    public void setReturnoil(Double returnoil) {
        this.returnoil = returnoil;
    }

    public Double getCostmoney() {
        return costmoney;
    }

    public void setCostmoney(Double costmoney) {
        this.costmoney = costmoney;
    }

    public Date getNexttime() {
        return nexttime;
    }

    public void setNexttime(Date nexttime) {
        this.nexttime = nexttime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}
