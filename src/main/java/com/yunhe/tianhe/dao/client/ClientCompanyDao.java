package com.yunhe.tianhe.dao.client;

import com.yunhe.tianhe.entity.client.ClientCompany;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/12/1 0001 上午 10:32
 * @Description: TODO
 */
public interface ClientCompanyDao {

    /**
     * 增加客户企业信息记录
     */
    Long save(ClientCompany clientCompany);

    /**
     * 查询一个企业
     * @param cid
     * @return
     */
    ClientCompany findById(Long cid);
}
