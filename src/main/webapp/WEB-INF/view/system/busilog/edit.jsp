<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/22 0022
  Time: 20:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>门店列表</title>
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="box-padding-15 ">
    <div class="layui-row">
        <div class="layui-col-md4 layui-col-md-offset3">
            <div class="grid-demo grid-demo-bg1">
                    <div class="layui-form-item">
                        <label class="layui-form-label">编号</label>
                        <div class="layui-input-block">
                            <input type="text" name="logid"disabled="disabled" value="${busilog.logid}"  lay-verify="required"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">操作业务</label>
                        <div class="layui-input-block">
                            <input type="text" name="businame" disabled="disabled" value="${busilog.businame}"  lay-verify="required"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">操作类型</label>
                        <div class="layui-input-block">
                            <input type="text" name="busitype" disabled="disabled" value="${busilog.busitype}"  lay-verify="required"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">操作时间</label>
                        <div class="layui-input-block">
                            <input type="text" name="addtime"disabled="disabled"  value="${busilog.addtime}"  lay-verify="required"  autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">操作内容</label>
                        <div class="layui-input-block">
                            <input type="text" name="busiconent" disabled="disabled" value="${busilog.busiconent}"  lay-verify="required" autocomplete="off" class="layui-input">
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/system/busilog/edit.js"></script>
</body>
</html>
