package com.yunhe.tianhe.controller.client;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.util.TimeUtils;
import com.yunhe.tianhe.entity.client.ClientCompany;
import com.yunhe.tianhe.entity.client.ClientUser;
import com.yunhe.tianhe.service.client.ClientUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/11/22 0022 下午 7:16
 * @Description: TODO
 */
@Controller
@RequestMapping("/clientuser")
public class ClientUserController {

    @Autowired
    private ClientUserService clientUserService;

    @RequestMapping()
    public String list(){
        return "client/clientuser/clientuser";
    }

    /**
     * 租车页面
     * 根据手机号码获得用户信息,判断用户是否为老客户
     * @return
     */
    @RequestMapping("/find")
    public String findByMobile(HttpServletRequest req,String mobile){
        ClientUser clientUser= clientUserService.findByMobile(mobile);
        req.setAttribute("clientUser",clientUser);
        return "/";
    }

    /**
     * 租车页面
     * 如果是新客户,首次用车保存客户信息
     * @return
     */
    @RequestMapping("/save")
    public  String save(HttpServletRequest request, ClientUser clientUser){
        clientUserService.save(clientUser);
        return "redirect:client/clientuser/clientuser";
    }


    /**
     * 客户管理页面
     * 客户详细信息列表,分页数据
     * @return
     */
    @RequestMapping("/listData")
    @ResponseBody
    public Pagedao<ClientUser> findPage(Integer offset, Integer limit,Integer memLevel,String endTime){
        Pagedao<ClientUser> page = new Pagedao<>();
        if(null != endTime){
            Date date = TimeUtils.StrToDateLong(endTime, TimeUtils.FORMAT_YYYY_MM_DD_HH_MM_SS);
            page = clientUserService.findPage(offset, limit, memLevel, date);
        }else{
            page = clientUserService.findPage(offset, limit, memLevel, null);
        }
       return page;
    }

    /**
     * 删除一个客户
     * @return  跳转到列表页
     */
    @RequestMapping("/delete")
    @ResponseBody
    public JSONResponse delete(String ids){
        clientUserService.deleteBatch(ids);
        return JSONResponse.success("删除成功");
    }

    /**
     * 编辑页面
     * @return
     */
    @RequestMapping("/edit")
    public String edit(Long id, HttpServletRequest request){
        ClientUser clientUser = clientUserService.findById(id);
        request.setAttribute("clientUser", clientUser);
        return "/client/clientuser/edit";
    }

    /**
     * 租车页面
     * 如果是新客户,首次用车保存客户信息
     * @return
     */
    @RequestMapping("/update")
    @ResponseBody
    public  JSONResponse update(HttpServletRequest request,ClientUser clientUser){
        clientUserService.update(clientUser);
        return JSONResponse.success("修改成功");
    }

}
