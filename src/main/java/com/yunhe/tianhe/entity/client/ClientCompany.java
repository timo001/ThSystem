package com.yunhe.tianhe.entity.client;

import java.util.Date;

/**
 * @author 2ez4u
 * @version V1.0
 * @Date 2017/12/1 0001 上午 10:16
 * @Description: 客户企业实体类
 */
public class ClientCompany {

    private Long cId; //企业编号
    private String busiLicense;//营业执照
    private String orgCode;//机构代码
    private String taxCertNo;//税务证号
    private String remark;//企业备注
    private Date addTime;//添加时间

    public Long getcId() {
        return cId;
    }

    public void setcId(Long cId) {
        this.cId = cId;
    }

    public String getBusiLicense() {
        return busiLicense;
    }

    public void setBusiLicense(String busiLicense) {
        this.busiLicense = busiLicense;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getTaxCertNo() {
        return taxCertNo;
    }

    public void setTaxCertNo(String taxCertNo) {
        this.taxCertNo = taxCertNo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }
}
