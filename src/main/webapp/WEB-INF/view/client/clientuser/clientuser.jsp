<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/11/23 0023
  Time: 下午 3:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>列表</title>
    <meta name="viewport" content="width=device-width" />
    <link href="/static/js/plugins/bootstrap-3.3.7-dist/css/bootstrap.css" rel="stylesheet" />
    <link href="/static/js/plugins/bootstrap-table/bootstrap-table.css" rel="stylesheet" />
    <link rel="stylesheet" href="/static/js/plugins/layui/css/layui.css">
    <link rel="stylesheet" href="/static/css/common.css">
</head>
<body>
<div class="layui-box box-padding-15">
        <span class="layui-breadcrumb">
          <a href="#">首页</a>
          <a href="javascript:;">客户管理</a>
          <a><cite>客户列表</cite></a>
        </span>


    <div class="layui-form-item">
        <label class="layui-form-label">会员级别：</label>
        <div class="layui-input-block">
            <select name="memLevel" id="memLevel">
                <option value="">所有会员</option>
                <option value="1">普通会员</option>
                <option value="2">白银会员</option>
                <option value="3">黄金会员</option>
                <option value="4">钻石会员</option>
                <%--<c:forEach items="${roles}" var="role">
                    <option value="${role.id}">${role.name}</option>
                </c:forEach>--%>
            </select>

            <div class="layui-inline">
                <label class="layui-form-label">客户注册时间：</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" id="startTime" placeholder="请选择注册时间">
                </div>
            </div>

            <div class="layui-inline">
                <label class="layui-form-label">截止时间：</label>
                <div class="layui-input-inline">
                    <input type="text" class="layui-input" id="endTime" placeholder="请选择截止时间">
                </div>
            </div>


            <button id="selBtn" data-url="/clientuser/select" class="layui-btn layui-btn-primary btn-sel" >查询</button>
        </div>


    </div>
    <div id="toolbar" class="layui-btn-group">
    <%--    <c:forEach items="${buttons}" var="btn">
            <button id="${btn.btnId}" data-url="${btn.uri}" class="layui-btn layui-btn-primary btn-add">${btn.name}</button>
        </c:forEach>--%>

         <button id="editBtn" data-url="/clientuser/edit" class="layui-btn layui-btn-primary btn-edit" >编辑</button>
         <button id="delBtn" data-url="/clientuser/delete" class="layui-btn layui-btn-primary btn-del">删除</button>
    </div>
    <%--<div class=" box-table-h7">--%>
    <table id="tb_page"></table>
    <%--</div>--%>
</div>

<script src="/static/js/plugins/jquery.js"></script>
<script src="/static/js/plugins/bootstrap-3.3.7-dist/js/bootstrap.js"></script>
<script src="/static/js/plugins/bootstrap-table/bootstrap-table.js"></script>
<script src="/static/js/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.js"></script>
<script src="/static/js/plugins/layui/layui.js"></script>
<script src="/static/js/common.js"></script>
<script>

    layui.use(['element','layer','laydate','btnEvent'], function(){
        var layer = layui.layer;
        laydate = layui.laydate;
        var btnEvent = layui.btnEvent;
        //定义表格
        var element = layui.element; //导航的hover效果、二级菜单等功能，需要依赖element模块
        $('#tb_page').bootstrapTable({
            url: '/clientuser/listData',         //请求后台的URL（*）
            method: 'get',                      //请求方式（*）
            toolbar: '#toolbar',                //工具按钮用哪个容器
            striped: true,                      //是否显示行间隔色
            cache: false,                       //是否使用缓存，默认为true，所以一般情况下需要设置一下这个属性（*）
            pagination: true,                   //是否显示分页（*）
            sortable: false,                     //是否启用排序
            sortOrder: "asc",                   //排序方式
            sidePagination: "server",           //分页方式：client客户端分页，server服务端分页（*）
            pageNumber:1,                       //初始化加载第一页，默认第一页
            pageSize: 15,                       //每页的记录行数（*）
            pageList: [15, 30, 50],        //可供选择的每页的行数（*）
            search: false,                       //是否显示表格搜索，此搜索是客户端搜索，不会进服务端，所以，个人感觉意义不大
            strictSearch: true,
            showColumns: false,                  //是否显示所有的列
            showRefresh: true,                  //是否显示刷新按钮
            minimumCountColumns: 2,             //最少允许的列数
            clickToSelect: true,                //是否启用点击选中行
            height: '700',                        //行高，如果没有设置height属性，表格自动根据记录条数觉得表格高度
            uniqueId: "id",                     //每一行的唯一标识，一般为主键列
            showToggle:false,                    //是否显示详细视图和列表视图的切换按钮
            cardView: false,                    //是否显示详细视图
            detailView: false,                   //是否显示父子表
            columns: [{
                checkbox: true
            }, {
                field: 'name',
                title: '客户名称',
                width: '20%'
            },  {
                field: 'mobile',
                title: '手机',
                width: '20%'
            },{
                field: 'creditNum',
                title: '信用值',
                width: '20%'
            },{
                field: 'memLevel',
                title: '会员级别',
                width: '20%'
            },{
                field: 'count',
                title: '租车次数',
                width: '15%',
            }]

        });

        //日期时间范围
        laydate.render({
            elem: '#startTime'
            ,type: 'datetime'

        });
        //日期时间范围
        laydate.render({
            elem: '#endTime'
            ,type: 'datetime'

        });

        btnEvent.bound($('#tb_page'),"addBtn","editBtn","delBtn");   //绑定通用事件


        $("#selBtn").click(function (){
            var memLevel = $('#memLevel').val();
            var endTime = $('#endTime').val();
            var opt = {
                url: "/clientuser/listData",
                silent: true,
                query:{
                    memLevel:memLevel,
                    endTime:endTime
                }
            };
            $("#tb_page").bootstrapTable('refresh', opt);
        });
    });
</script>
</body>
</html>
