package com.yunhe.tianhe.service.cars.impl;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.commons.util.StringUtils;
import com.yunhe.tianhe.dao.cars.RepairDao;
import com.yunhe.tianhe.entity.cars.Repair;
import com.yunhe.tianhe.service.cars.RepairService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/27 0027 下午 3:50
 * @Description: TODO
 */
@Service
public class RepairServiceImpl implements RepairService{

    @Autowired
    private RepairDao repairDao;
    @Override
    public Long save(Repair repair) {
        repairDao.save(repair);
        Long recordId = repair.getRecordId();
        return recordId;
    }

    @Override
    public void update(Repair repair) {
        repairDao.update(repair);
    }

    @Override
    public void delete(Long recordId) {
        repairDao.delete(recordId);
    }

    @Override
    public void deleteBatch(String ids) {
        if(StringUtils.notEmpty(ids)){
            String[] idArr = ids.split(",");
            for(String id : idArr){
                this.delete(Long.valueOf(id));
            }
        }
    }

    @Override
    public Repair find(Long recordId) {
        return repairDao.find(recordId);
    }

    @Override
    public Pagedao<Repair> findPage(Integer offset, Integer limit, String searchCode) {
        Pagedao<Repair> pagedao = new Pagedao<>();
        Integer count = repairDao.findCount(searchCode);
        List<Repair> data = null;
        if(count>0){
            pagedao.setPage(offset);
            pagedao.setSize(limit);
            data = repairDao.findAll(offset, limit, searchCode);
        }
        pagedao.setTotal(count);
        pagedao.setRows(data);
        return pagedao;
    }

    @Override
    public List<Repair> findByCarCode(String carCode) {
        return repairDao.findByCarCode(carCode);
    }
}
