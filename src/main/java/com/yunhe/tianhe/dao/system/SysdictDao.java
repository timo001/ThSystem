package com.yunhe.tianhe.dao.system;

import com.yunhe.tianhe.entity.system.Memberlevel;
import com.yunhe.tianhe.entity.system.Sysdict;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 15:46
 * @Description:
 */

public interface SysdictDao {

    /**
     * 添加
     * @param sysdict
     */
    void save(Sysdict sysdict);

    /**
     * 删除
     * @param id
     */
    void deleted(Long id);

    /**
     * 修改
     * @param sysdict
     */
    void update(Sysdict sysdict);

    /**
     * 单个查找
     * @param id
     * @return
     */
    Sysdict find(Long id);

    /**
     * 查找
     * @return
     */
    List<Sysdict> findlist(Sysdict sysdict);

    /**
     * 分页
     * @return
     */
    Integer count();

    /**
     * 分页
     * @param offset
     * @param limit
     * @return
     */
    List<Sysdict> findpage(Sysdict sysdict,@Param( "offset" ) int offset, @Param ( "limit" ) int limit);

}
