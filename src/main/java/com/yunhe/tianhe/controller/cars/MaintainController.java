package com.yunhe.tianhe.controller.cars;

import com.yunhe.commons.dto.JSONResponse;
import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.Maintain;
import com.yunhe.tianhe.service.cars.MaintainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/25 0025 下午 1:11
 * @Description TODO
 */
@Controller
@RequestMapping("/maintain")
public class MaintainController {
    @Autowired
    private MaintainService maintainService;

    @RequestMapping("/maintainlist")        //到保养列表
    public String maintainlist(){
        return "cars/maintain/list";
    }

    @RequestMapping("/page")          //分页
    @ResponseBody
    public Pagedao list(Integer limit,Integer offset,String carcode){
        Pagedao list = maintainService.list(limit, offset,carcode);
        return list;
    }

    @RequestMapping("/addview")
    public String addview(){
        return "cars/maintain/add";
    }

    @RequestMapping("/save")
    public String save(Maintain maintain){
        maintainService.save(maintain);
        return "redirect:/maintain/maintainlist";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public JSONResponse delete(String ids){
        maintainService.delete(ids);
        return JSONResponse.success("删除成功！");
    }

    @RequestMapping("/edit")
    public ModelAndView edit(Long recordid){
        Maintain edit = maintainService.edit(recordid);
        return new ModelAndView("/cars/maintain/edit","edit",edit);
    }

    @RequestMapping("/update")
    public String update(Maintain maintain){
        maintainService.update(maintain);
        return "redirect:/maintain/maintainlist";
    }

}
