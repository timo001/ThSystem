/**
 * Created by Administrator on 2017/11/23 0023.
 */
layui.use(['element','layer','form'], function() {
    var layer = layui.layer;
    var form = layui.form;
    var element = layui.element;

    var setting = {
        check: {
            enable: true
        },
        data: {
            simpleData: {
                enable: true
            }
        }
    };
    var roleid = $("input[name=roleid]").val();
    $.ajax({
        url:"/permi/role/tree",
        type:"get",
        dataType:"json",
        data:{roleid:roleid},
        success:function (msg) {
            var zNodes =msg.data;//获取数据
            $.fn.zTree.init($("#treeRole"), setting, msg); //初始化节点树
            console.log("节点数加载成功！")
        },
        error:function () {
            console.log("节点数加载失败！")
        }
    })

    //监听提交
    form.on('submit(formDemo)', function(data){
        var treeObj = $.fn.zTree.getZTreeObj("treeRole");  //获取树对象
        var nodes = treeObj.getCheckedNodes(true);  //获取树的选中节点
        var ids=[];
        $.each(nodes,function (i,item) {
            ids.push(item.id);
        })
        var reqData = data.field;
        reqData.ids = ids.join();

        $.ajax({
            url:"/permi/role/update",
            type:"post",
            dataType:"json",
            data:data.field,
            success:function (msg) {
                layer.msg("修改成功！",{time:1000},function () {
                    location.href="/permi/role/list";
                })
            }

        })
        return false;
    })
})