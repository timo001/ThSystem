package com.yunhe.tianhe.entity.premission;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/29 0029 15:39
 * @Description: 角色实体
 */
public class Role {
    private Long roleid; //角色编号
    private String rolename; //角色名称
    private String decrib; //角色描述
    private Integer isabled=0; //是否启用
    @DateTimeFormat(pattern = "YYYY-MM-dd HH:mm:ss")
    private Date addtime; //添加时间

    public Long getRoleid() {
        return roleid;
    }

    public void setRoleid(Long roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getDecrib() {
        return decrib;
    }

    public void setDecrib(String decrib) {
        this.decrib = decrib;
    }

    public Integer getIsabled() {
        return isabled;
    }

    public void setIsabled(Integer isabled) {
        this.isabled = isabled;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }
}
