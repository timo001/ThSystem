/**
 * Created by Administrator on 2017/11/23 0023.
 */
//JavaScript代码区域
layui.use(['element','layer','form'], function(){
    var layer = layui.layer;
    var element = layui.element;
    var form = layui.form;

    //监听保存提交
    form.on('submit(formDemo)', function(data){
        $.ajax({
            url:"/system/sysdict/save",
            type:"post",
            dataType:"json",
            data:data.field,
            success:function (msg) {
                layer.msg("添加成功！",{time:1000},function () {
                    location.href="/system/sysdict/list";
                })
            },
            error:function () {
                layer.msg("服务器忙！请稍后再试！")
            }
        })
        return false;
    });

//    修改
    var id;
    function zTreeOnClick(event, treeId, treeNode) {
        id=treeNode.id;
        $("#parent").val(treeNode.name)//添加的父类
        $.ajax({
            url:"/system/sysdict/edit",
            type:"get",
            dataType:"json",
            data:{id:id},
            success:function (msg) {
                var data = msg.data;
                var data1 = msg.data1;
                if (data1==null){
                    layer.msg("该项不能修改！")
                }else{
                    $("#parentname").val(data1.name)
                    $("#name").val(data.name)
                    $("#code").val(data.code)
                }
            },
            error:function () {
                layer.msg("服务器忙！请稍后再试！")
            }
        })
    };
    $("#btn_edit").click(function () {
        if (null == id){
            layer.msg("请选择一条数据")
        }
    })

    //监听保修改提交
    form.on('submit(formedit)', function(data){
        $.ajax({
            url:"/system/sysdict/update",
            type:"post",
            dataType:"json",
            data:data.field,
            success:function (msg) {
                layer.msg("修改成功！",{time:1000},function () {
                    location.href="/system/sysdict/list";
                })
            },
            error:function () {
                layer.msg("服务器忙！请稍后再试！")
            }

        })
        return false;
    });


});