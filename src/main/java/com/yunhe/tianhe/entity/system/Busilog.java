package com.yunhe.tianhe.entity.system;


import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/24 0024 10:37
 * @Description: 系统日志实体
 */
public class Busilog {

    private Long logid;  //日志编号
    private String businame; //操作业务
    private String busitype; //操作类型
    private String busiconent; //操作内容
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addtime;//添加时间
    private String begintime;//开始时间
    private String endtime;//结束时间



    public Long getLogid() {
        return logid;
    }

    public void setLogid(Long logid) {
        this.logid = logid;
    }

    public String getBusiname() {
        return businame;
    }

    public void setBusiname(String businame) {
        this.businame = businame;
    }

    public String getBusitype() {
        return busitype;
    }

    public void setBusitype(String busitype) {
        this.busitype = busitype;
    }

    public String getBusiconent() {
        return busiconent;
    }

    public void setBusiconent(String busiconent) {
        this.busiconent = busiconent;
    }

    public Date getAddtime() {
        return addtime;
    }

    public void setAddtime(Date addtime) {
        this.addtime = addtime;
    }

    public String getBegintime() {
        return begintime;
    }

    public void setBegintime(String begintime) {
        this.begintime = begintime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }
}
