package com.yunhe.tianhe.dao.permission;

import com.yunhe.tianhe.entity.premission.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/29 0029 17:17
 * @Description: 角色管理
 */
public interface RoleDao {

    void save(Role role);

    void update(Role role);

    void deleted(Long roleid);

    Role find(Long roleid);

    List<Role> findlist(Role role);

    List<Role> findpage(@Param ( "offset" ) int offset,@Param ( "limit" ) int limit);

    int count();

    void saveRoleResource(Long roleid,Long resourceid);

    void deletedRoleResource(Long roleid);

    List<Long> findPermi(Long roleid);

}
