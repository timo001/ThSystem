package com.yunhe.tianhe.dao.cars;

import com.yunhe.tianhe.entity.cars.Yearcheck;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/27 0027 下午 5:16
 * @Description TODO
 */
public interface YearcheckDao {
    int pagedao(String carcode);  //分页（统计总数）
    List<Yearcheck> queryPage(@Param("page") int page, @Param("size") int size, @Param("carcode") String carcode);//分页

    void save(Yearcheck yearcheck);   //增加

    void delete(Integer id);     //删除
    //查询(为修改做准备)
    Yearcheck edit(Long recordid);
    //修改
    void update(Yearcheck yearcheck);
}
