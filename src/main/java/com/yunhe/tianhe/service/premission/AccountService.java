package com.yunhe.tianhe.service.premission;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.premission.Account;


import java.util.List;

/**
 * @author LeiPeifeng
 * @version V1.0
 * @date 2017/11/27 0027 20:13
 * @Description: 账户管理
 */
public interface AccountService {

   //保存
    void save(Account account);
   //修改
    void update(Account account);
   //删除
    void deleted(String ids);
    // 单个查找
     Account find(Long uid);

    // 查找所有
     List<Account> findlist();

    /**
     * 分页查找
     * @param account
     * @return
     */
     Pagedao findpage(Account account,int offset,int limit);

}
