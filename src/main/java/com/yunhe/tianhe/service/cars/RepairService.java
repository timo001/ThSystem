package com.yunhe.tianhe.service.cars;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.Repair;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author XU
 * @version V1.0
 * @Date 2017/11/27 0027 下午 3:46
 * @Description: TODO
 */
public interface RepairService {
    /*车辆维修记录保存*/
    Long save(Repair repair);

    /*车辆维修记录修改*/
    void update(Repair repair);

    /*车辆维修记录删除*/
    void delete(Long recordId);

    /*车辆维修记录批量删除*/
    void deleteBatch(String ids);

    /*车辆维修记录查找*/
    Repair find(Long recordId);

    /*分页*/
    Pagedao<Repair> findPage(@Param("offset") Integer offset, @Param("limit") Integer limit, @Param("searchCode")String searchCode);

    /*根据车牌号查询*/
    List<Repair> findByCarCode(String carCode);
}
