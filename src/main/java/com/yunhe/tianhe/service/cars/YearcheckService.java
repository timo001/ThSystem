package com.yunhe.tianhe.service.cars;

import com.yunhe.commons.dto.Pagedao;
import com.yunhe.tianhe.entity.cars.Yearcheck;
import org.apache.ibatis.annotations.Param;

/**
 * @author zhaofeiyang
 * @version V1.0
 * @Date 2017/11/27 0027 下午 5:10
 * @Description TODO
 */
public interface YearcheckService<E> {
    //分页
    Pagedao<Yearcheck> list(@Param("limit")Integer limit, @Param("offset") Integer offset,@Param("carcode") String carcode);
    //新增
    void save(Yearcheck yearcheck);
    //删除
    void delete(String ids);
    //查询(为修改做准备)
    Yearcheck edit(Long recordid);
    //修改
    void update(Yearcheck yearcheck);
}
